# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.
# v_ansible_hosts_jdb_demo='7.4.1'

# - View the host list like this:
#   $ ansible-inventory --graph
#   $ ansible-inventory --list | jq -S '.all'
#   $ ansible-inventory --list | grep ansible_host
#   $ ansible-inventory --list | grep '"ansible_'


# - Reference:
#   https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#intro-inventory
#   https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#inventory-setup-examples
#   https://docs.ansible.com/ansible/latest/reference_appendices/faq.html
#   https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#inventory-basics-formats-hosts-and-groups


# - FYI:
#   -> Buggy behavior on remote systems
#   https://stackoverflow.com/questions/18388106/ansible-pausing-indefinitely

# - FYI:
#   -> Pauses in SSH loops are typically SSH "automation failures" with interactive questions.

# - FYI:
#   SSH connections use config DEFAULT_TIMEOUT which defaults to 10 seconds.

# - Tests:
#   mac$ ansible all -m ping
#   mac$ ansible all -m ping -u root

# - FYI: YAML
#   https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html#yaml-syntax

# - Tips & Tricks:
#   https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html        # - Search: Tags, Use dynamic inventory with clouds
#   https://docs.ansible.com/ansible/latest/user_guide/intro_dynamic_inventory.html#intro-dynamic-inventory


# - FYI:
#   The hosts listed here are hardened against Internet login attacks.
#   Eg: SSH login only allows public-key authentication; password access is disabled.


# ---


# all:
#     hosts:
#
#     children:
#
#         # - The Rasberry PIs are defined in terms of reachability from John's Mac on home network.
#         #   This may be an Internet connection.
#         #
#         # - To override the connection FQDN, try this:
#         #
#         #   $ apehh apple-pi.umi.ch
#         #   $ apers
#         #   $ ape 00_b apple-pi
#
#         pi:
#             hosts:
#                 apple-pi:
#                     ansible_host: apple-pi.home.umi.ch    ## - apple-pi.umi.ch
#                     ansible_port: 2223
#                     ansible_user: jdb                     ## - ubuntu, tester
#                     ansible_ssh_private_key_file: ~/.ssh-jdb/id_ed25519
#
#                 cherry-pi:
#                     ansible_host: cherry-pi.home.umi.ch   ## - cherry-pi.umi.ch
#                     ansible_port: 2223
#                     ansible_user: jdb
#                     ansible_ssh_private_key_file: ~/.ssh-jdb/id_ed25519
#
#                 cow-pi:
#                     ansible_host: cow-pi.home.umi.ch
#                     ansible_port: 2223
#                     ansible_user: jdb                     ## - ubuntu, tester
#                     ansible_ssh_private_key_file: ~/.ssh-jdb/id_ed25519
#
#         test:
#             hosts:
#                 cow-pi:
#
#         test_host:
#             hosts:
#                 cow-pi:
#
#
#         awses:      # - AWS EC2 servers
#             hosts:
#                 aws:
#                     ansible_host: aws.umi.ch
#                     ansible_port: 2223
#                     ansible_user: jdb
#                     ansible_ssh_private_key_file:   ~/.ssh-jdb/id_ed25519
#
#                 awx:
#                     ansible_host: awx.umi.ch
#                     ansible_port: 2223
#                     ansible_user: jdb
#                     ansible_ssh_private_key_file:   ~/.ssh-jdb/id_ed25519
#
#                 awx2:
#                     ansible_host: awx2.umi.ch
#                     ansible_port: 2223
#                     ansible_user: jdb
#                     ansible_ssh_private_key_file:   ~/.ssh-jdb/id_ed25519
#
#
#         does:       # - Digital Oceans
#             hosts:
#                 ocean:
#                     ansible_host: ocean.umi.ch
#                     ansible_port: 2223
#                     ansible_user: jdb
#                     ansible_ssh_private_key_file:   ~/.ssh-jdb/id_ed25519
#
#
#         cloud:
#             hosts:
#                 aws:
#                 awx:
#                 awx2:
#                 ocean:

