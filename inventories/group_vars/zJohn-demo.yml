# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.
# v_ansible='8.1.0'


    ##########
    # Basics #
    ##########

# - Identity, for multitenant use:
my_tenant:
    "tenant_default"


# - Ansible Vault encryption tests, in Playbook 00_e.show_vaulted.yml

my_demoKey_1:
    demoKey

my_demoKey_1_enc:
    "{{ demoKey }}"

my_demoKey_2:
    demoKey2

my_demoKey_2_enc:
    "{{ demoKey2 }}"


# https://docs.ansible.com/ansible/latest/user_guide/become.html#become-connection-variables
#
ansible_common_remote_group:
    users

allow_world_readable_tmpfiles:
    True

# - Added users can always be part of these groups. (string list)
my_user_groups:
    "users"


    ##########################
    # John's SSH public keys #
    ##########################

# - ToDo: Deploy extra, non-inbound SSH keys. Eg:
#   mac$ scp -p -r -P 2223 ~/.sshs/ssh-bozo  jdb@awx.umi.ch:.ssh-bozo
#
# - Value is encrypted in Ansible Vault.

inbound_authorized_keys: |+
    {{ ansv_inbound_authorized_keys_default }}

done_1:
  done


    #################
    # Web selectors #
    #################

# - Selector '00' is always loaded, and gives the "Original Unix" teaser page.
#   Selector "01' is a stub NOOP, but useful here.

my_web_selectors:
    - "01"



    ####################################
    # LetsEncrypt SSL/TLS certificates #
    ####################################

# - Initial certificates are acquired manually on an offline system (eg: Mac-Mini),
#   then transfered to the target system with Ansible.
#
# - Scripts to acquire certs are here:
#   bashrc_101_b / i_Lets_Encrypt_2
#   $ le_certbot()
#   $ le_renew_fix()
#
# - Show repository locations:
#   $ ape 01_d.env_test all

my_letsencrypt_repo:
    ~/zGit/ansible/playbooks/files/letsencrypt/

my_letsencrypt_tgz:             # - Override per system
    le.stub.tgz

my_etc_letsencrypt_disabled:    # - Directories to remove. Comment-out to disable.
    letsencrypt/

# - FYI: Demo stub certs in the  bozo/  directory.
#
my_etc_letsencrypt_files:
    - options-ssl-nginx.conf
    - ssl-dhparams.pem
    - bozo



    #########################
    # sshd-jdb installation #
    #########################

# - The build file can also be used, with CLI overrides. Eg:
#
#   mac$ apee 'my_app_arch=arm64'
#   mac$ apee 'my_build_host=cow-pi'
#   mac$ apee 'my_app_date=2022-11-30'
#   mac$ ape  04_q cow-pi 1

my_app_sshd_jdb:                    # -> my_app
    sshd-jdb

my_app_dir_sshd_jdb:                # -> my_app_dir
   /usr/local/{{ my_app_sshd_jdb }}

my_app_pkg_dir_sshd_jdb:            # -> my_app_pkg_dir
    "~/zGit/ansible/playbooks/files/{{ my_app_sshd_jdb }}.zPkgs/"

my_app_deb_sshd_jdb:                # -> my_app_deb, system specific.
    "undefined_my_app_deb_sshd_jdb"

my_app_pkg_sshd_jdb:                # -> my_app_pkg
    "{{ my_app_pkg_dir_sshd_jdb }}/{{ my_app_deb_sshd_jdb }}"



    ##########################
    # Miniconda / Mambaforge #
    ##########################

# - Upload this manually.
#   The files are architecture specific.
#   As of April 2022, Miniconda is not yet ready for Raspberry-PI.
#
# - FYI: The "uname_arch" is from  $(uname -p)  which is not necessarily
#   the same as the "Debian packaging archicture" naming conventions in
#   the build scripts.
#
# - FYI: The files here are typically symlinks.
#   See README.md in the files directory for more info.

my_miniconda_x86_64:     # - Standard amd64 / i386 / x86 / Intel Pentium
    ## Miniconda3-py39_4.10.3-Linux-x86_64.sh
    ## Miniconda3-py397_4.12.0-Linux-x86_64.sh
    Miniconda3-Linux-x86_64.sh

my_miniconda_aarch64:    # - Raspberry PI 4
    Mambaforge-Linux-aarch64.sh

my_miniconda_repo_dir:
    "~/zGit/ansible/playbooks/files/conda.zPkgs/"


    ############
    # Fail2Ban #
    ############

# - Other parameters are host-specific.

## my_f2b_ignoreip:
##    "ignoreip = 127.0.0.1/8  ::1  192.168.1.0/24"

my_f2b_ignoreip:
    - 127.0.0.1/8
    - ::1
    - 192.168.1.0/24
    #
    # - John's stuff here:
    #

my_f2b_local_repo_dir:
    "~/zGit/ansible/playbooks/files/fail2ban"

# - List of jails to set 'enabled = true', if the jail appears in the file but is set false.
#   $ guts  ~/.zrc/zEtc/fail2ban/jail.local | egrep -A1 '^\['
#   $ guts /etc/fail2ban/jail.local | egrep '^\['
#
my_f2b_jails_to_enable:
    - sshd
    - apache
    - apache-noscript
    - apache-overflows
    - apache-nohome
    - nginx-rc-john-400
    - nginx-rc-john-402
    - nginx-rc-john-404
    - nginx-rc-john-420
    - nginx-rc-john-444
    - nginx-req-limit-john
    - nginx-ssl-mismatch-john


# - List of jails to set 'enabled = false', if the jail appears in the file but is set true.
my_f2b_jails_to_disable:
    []      # - empty list


    #######
    # UFW #
    #######

# - FYI:  https://www.speedguide.net/port.php?port=5201

# - Logging is verbose and not very useful. Enable it in special cases.
my_ufw_log:     'off'
my_ufw_logging: 'off'

# - Allow all access from RFC1918 networks to this host.
#   -> Consider skipping this (with an empty list) on hosts accessed by Internet NAT.
my_ufw_networks:
    - 10.0.0.0/8
    - 172.16.0.0/12
    - 192.168.0.0/16


my_ufw_ports_tcp:
    # - SSH: standard port, and John's alternatves
    - 22
    - 2223

    # - Web:
    - 80        # - http only, usually redirects to https
    - 443       # - https
    - 446       # - John's client-cert tests
    - 5080      # - do_web2: cherrypy web server & http_pinger

    # - Puppet
    - 8140      # - Puppet

my_ufw_ports_udp:
    ##  - 500       # - IPsec
    ##  - 4500      # - IPsec

my_ufw_ip_esp:
    ##  # - IPsec

my_ufw_ip_ah:
    ##  # - IPsec

my_ufw_ports_all:
    - 3389      # - RDP
    - 5001      # - iPerf2
    - 5201      # - iPerf3

done_2:
    done



    ################
    # Repositories #
    ################

# - Playbook: Git clone - a list of repositories, eg:
#   git@gitlab.com:umi-ch/bashrc.git
#
# - Suffix is implicit and hardwired in the playbook: '.git'

my_repo_prefix:
    "git@gitlab.com:umi-ch"

my_repos:
    - "bashrc"

# - Container directory of the repositories.
#   -> FYI: multitenant override.
#
my_git_repos_dir:
    "zGit"

# - The repo linked from .zrc:
my_repo_zrc:
    "bashrc"


# - Playboook: Bashrc deploy, without Git clone.
#   Bundle items support glob wildcards, eg: bashrc_1*

my_bashrc_repo:
    "git@gitlab.com:umi-ch/bashrc.git"

my_bashrc_deploy_dir:
    "zGit.deploy"

my_bashrc_bundle_static:
    - bash_profile
    - profile
    - ruler
    - vimrc
    - zStamp
    - zrc.env
    - binp/
    - bashrc_001

my_bashrc_bundle:
    - bashrc_1*

done_3:
  done



    ###############
    # Other repos #
    ###############

# - Raspberry PI repos

my_pi_argon_case_repo_dir:
    "~/zGit/ansible/playbooks/files/pi-argon/"

my_pi_heat_logger_dir:
    "~/zGit/ansible/playbooks/files/pi-heat-logger/"


# - Ubuntu repos

my_speedtest_dir:
    "~/zGit/ansible/playbooks/files/speedtest/"

my_zfs_logger_dir:
    "~/zGit/ansible/playbooks/files/zfs-logger/"

my_minecraft_dir:
    "~/zGit/ansible/playbooks/files/minecraft/"



    ##############
    # nmap ports #
    ##############

# - Here are "interesting ports" to scan for being open.
#   Eg, potential SSH ports when installing the custom OpenSSH package below.

my_nmap_ports:
    - 22-9999     # - This works, but large port scans are slow.

