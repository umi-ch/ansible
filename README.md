# John's Agile Ansible

This repository illustrates an exploration of "**Agile Ansible**" -- scalable in several ways.


## Host groups and variables

My hosts are more loosely defined in groups, rather than using the traditional role layout. <br>
The intention is that I can run **any playbook**, at **any time**, on **any host** -- when useful. <br>
**Macroscopic automation** (ie, a series of playbooks) is handled in another manner. <br>


## Platform: Ubuntu Linux servers

- Raspberry-PI 4 : **power setup** : 8 Gb RAM, large USB SSD disk(s)
- **AWS cloud** EC2 instances
- **Digital Ocean cloud** Droplet instances
- **VMware** instances
- Deprecated: Solaris instances, on real hardware
- Some MacOS support, but this is not a "server target"

Most these **Ansible playbooks** will work on other Linux distributions too (eg: **RedHat**, **SuSE**)<br>
but those are not my target platforms at this time.


## Ansible controller

- Apple Macintosh, eg, MacBook Pro Laptop -- my preference
- Any Linux instance


## Substantial application functionality, a la carte

- Extensive Bashrc convenience library (loaded from another Repository)
- Programming languages installed, including from Miniconda / Mambaforge:
  Python, Java, Go, Perl, GCC
- User accounts, with SSH key creation
- Web servers: NGINX, Apache
- Sample web sites
- LetEncrypt SSL/TLS certificates, with wildcard support via DNS API automation
- Internet security hardening, with UFW firewall and Ubuntu App Armor
- Internet Honeypot jails with Fail2Ban
- Internet Honeypot with rebuilt OpenSSH daemon, to log malware password attacks
- ZFS file systems on explicit hardware (eg: Raspberry PI)
- Performance / statistic utilities
- Ubuntu software updates and reboot control
- Backups
- Multi-tenant Git clients
- SSH agent forwarding, for secure remote connections - eg, to GitLab

<br/>

## Installation

This is aligned with **Johns Agile Bashrc** conventions.

```
$ mkdir -p ~/zGit/

$ git clone  git@gitlab.com:umi-ch/ansible.git  ~/zGit/ansible/
    ...

$ export ANSIBLE_CONFIG=~/zGit/ansible/etc/ansible.cfg

# - Ansible Vault: These are working demos. Replace with your own credentials.
#
$ export ANSV_PASSWORD_FILE=~/.vaults/password-1.yml
$ export ANSV_VAULT_FILE=~/.vaults/vault-1.yml
$ export ANSV_VAULT_ID=demoVaultId
#
$ export ANSIBLE_VAULT_IDENTITY="${ANSV_VAULT_ID}@${ANSV_PASSWORD_FILE}"
$ export ANSIBLE_VAULT_IDENTITY_LIST="${ANSV_VAULT_ID}"
```

<br/>

## Optimized Ansible output

- **f_ape()** Bash wrapper, in **Johns Agile Bashrc**
- verbose modes


## Agile Ansible CLI

- The playbooks take parameters from Bash environment variables
- Easy overrides
- Convenient tuning and conditional execution


## Conditional playbook task execution

- Simple user-defined enumeration


## Consistent error checking

- automatic error output
- optional **quit or continue** hooks


## Lessons Learned

There is much work to be done with Ansible, to add a high degree of **robustness** and **agility**.<br>
But many Ansible use-cases typically need only a **subset** of these extra efforts.

<br/>

## Ansible Vault

Ansible offers very basic **"Vault methods"** to work with sensitive values (like passwords),<br/>
by storing **encrypted editions** of files and variable, which automatically decrypt in **Playbooks**.<br/>
This requires appropriate setup in advance.

For more serious enterprise use, I recommend **Hashicorp Vault** instead.<br/>
Nevertheless, **Ansible Vault** is adequate to secure your data in basic use-cases.

- https://docs.ansible.com/ansible/latest/user_guide/vault.html
- https://developer.hashicorp.com/vault

My **Ansible Vault demo setup** is described in the text file above:<br/>
 &nbsp; &nbsp; `./zDocs/2022-11-25.Config 53.Ansible-vault-public.txt`

- https://gitlab.com/umi-ch/ansible/-/blob/main/zDocs/2022-11-25.Config%2053.%20Ansible-vault-public.txt

<br/>

**Summary**

```
# - Set up the demo Vault password file like this:

$ mkdir -p  ~/.vaults/
$ printf  "demoMasterPassword\n"  >  ~/.vaults/password-1.txt


# - Here are demo Ansible Vault artifacts in this repository:

$ cd ~/zGit/ansible/

$ ls ./inventories/group_vars/all/ | cat
    stubs.yml
    vault-1.yml

$ grep '!vault' ./inventories/group_vars/all/vault-1.yml
    demoKey: !vault |
    demoKey2: !vault |
    ansv_inbound_authorized_keys_default: !vault |
    ansv_ubuntu_password: !vault |

$ grep  inbound_authorized_keys  ./inventories/group_vars/zJohn.yml
    inbound_authorized_keys: |+
        "{{ ansv_inbound_authorized_keys_default }}"

$ grep  APE_var_2  ./playbooks/30_j.default-users.yml
    APE_var_2:  "{{ ansv_ubuntu_password }}"
    APE_var_2:  "{{ ansv_ubuntu_password }}"


# - Sample Ansible Vault extraction and decryption:

$ ansible localhost  -m ansible.builtin.debug  -a var="demoKey"  \
                     -e @~/zGit/ansible/inventories/group_vars/all/vault-1.yml  \
                     --vault-id demoVaultId@~/.vaults/password-1.txt

  localhost | SUCCESS => {
       "demoKey": "demoPassword"
  }


# - Here's a real example. Adapt this repository to use your own servers.
#
# - This uses the f_ape() wrappers in John's Agile Bashrc.
#   https://gitlab.com/umi-ch/bashrc
#
# - Ansible Vault variables are described here:
#   https://docs.ansible.com/ansible/latest/reference_appendices/config.html#envvar-ANSIBLE_VAULT_IDENTITY_LIST

$ grep -A4 "name: '20\."  ~/zGit/ansible/playbooks/00_e.show-vaulted.yml
    - name: '20. Variables: encrypted'
      debug:
        msg:
            - "my_demoKey_1_enc : {{ my_demoKey_1 }} -> {{ my_demoKey_1_enc }}"
            - "my_demoKey_2_enc : {{ my_demoKey_2 }} -> {{ my_demoKey_2_enc }}"

$ grep demoKey  ~/zGit/ansible/inventories/group_vars/zJohn.yml
    my_demoKey_1:
        demoKey
    my_demoKey_1_enc:
        "{{ demoKey }}"
    my_demoKey_2:
        demoKey2
    my_demoKey_2_enc:
        "{{ demoKey2 }}"

$ grep -A1 demoKey ~/zGit/ansible/inventories/group_vars/all/vault-1.yml
    demoKey: !vault |
              $ANSIBLE_VAULT;1.2;AES256;demoVaultId
    --
    demoKey2: !vault |
              $ANSIBLE_VAULT;1.2;AES256;demoVaultId


$ ape 00_e ocean
    -# 00_e.show-vaulted.yml
    -$ export APE_HOSTS='ocean'         # - Playbook host list.
    ...
    -# vault_id=''
    -# ANSIBLE_VAULT_IDENTITY_LIST='demoVaultId@~/.vaults/password-1.txt'
    -# ANSIBLE_VAULT_ID_MATCH='True'
    -# ANSIBLE_VAULT_PASSWORD_FILE='/Users/jdb/.vaults/password-1.txt'

    -$ ansible-playbook  '/Users/jdb/zGit/ansible/playbooks/00_e.show-vaulted.yml'
    ...

    TASK [20. Variables: encrypted] ************************************************
    ok: [ocean] => {
        "msg": [
            my_demoKey_1_enc : demoKey -> demoPassword
            my_demoKey_2_enc : demoKey2 -> demoPassword2
    ...
```

<br/>

## Examples

**Agile robustness** is not Ansible's strength. <br>
Eg: &nbsp; *Things work well when they work, but failure conditions are messy
and not nicely handled.*

This extra code shown here is an example how I work-around this limitation. <br>
My next steps are to refactor this extra code into convenient modules and macros.

```
- hosts: "{{ lookup ('env', 'APE_HOSTS') }}"
  gather_facts: no

  vars:
    APE_v:      "{{ lookup ('env', 'APE_verbose') or 0 }}"                  # - verbose mode
    LOOP_v_tag: "{{ (APE_v | int > 0) | ternary ('always', 'never') }}"     # - verbose-only LOOP
    steps:      "{{ lookup ('env', 'APE_STEPS') | default ([], true) }}"    # - list of sections (optional)

  tasks:

    - name: '10. Remote timestamp'
      shell: |
        echo '$ date; whoami; hostname -f; uptime'
        date
        whoami
        hostname -f
        uptime
      args:
        executable: /bin/bash
      register: tout
      when:
        (('10' in steps) or (steps | length == 0))

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('10' in steps) or (steps | length == 0))  and
        (APE_v | int >= 0)
```

<br>

And here are my sample solutions for nicer error handling:


```
    # - FYI:
    #   '$ certbot certificates' implicitly makes an HTTP connection,
    #     as mentioned in the log file, and as explained here:
    #   https://letsencrypt.org/docs/lencr.org/
    #
    - name: "11. Show certbot certificates #1"
      ignore_errors: true
      become: true
      shell:  |
        echo "$ sudo certbot certificates"
        certbot certificates  2>&1
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "- Error, rc: $rc"
            exit $rc
        fi
        true
      args:
        executable: /bin/bash
      register: tout
      when:
        (('11' in steps) or (steps | length == 0))

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('11' in steps) or (steps | length == 0))  and
        ( (APE_v | int >= 0) or
          (("rc" in tout) and (tout.rc != 0)) )

    - name: "11. Fail?"
      fail: msg='Certbot failed.'
      when:
        (('11' in steps) or (steps | length == 0))  and
        (("rc" in tout) and (tout.rc != 0))


    - name: "29. Fix permissions: chmod -R 700 /etc/letsencrypt/{...}"
      become: true
      ignore_errors: true
      ansible.builtin.file:
        path:       "/etc/letsencrypt/{{ item }}"
        mode:       '700'
        state:      directory
        recurse:    true
      register: toutb
      loop:
        - accounts
        - archive
        - keys
        - live
      when:
        ('29' in steps) or (steps | length == 0)

    - debug:
        var: toutb
      when:
        (('29' in steps) or (steps | length == 0))  and
        ( (APE_v | int >= 1)  or
          ("failed" in toutb) and (toutb.failed == true) )

    - name: "29. Fail?"
      fail:
        msg: "chmod failed."
      when:
        (('29' in steps) or (steps | length == 0))  and
        (('failed' in toutb) and (toutb.failed == true))
```

<br>

Output is "niced" with my **f_ape()** wrapper, reducing Ansible's cluttered output.


```
$ ape 00_a ocean
    -# 00_b.NOOP+remote.yml
    -$ export APE_HOSTS='ocean'         # - Playbook host list.
    -$ export APE_STEPS=''
    -# vault_id=''
    -# ANSIBLE_VAULT_IDENTITY_LIST='demoVaultId@~/.vaults/password-1.txt'
    -# ANSIBLE_VAULT_ID_MATCH='True'
    -# ANSIBLE_VAULT_PASSWORD_FILE='/Users/jdb/.vaults/password-1.txt'

    -$ ansible-playbook   '/Users/jdb/zGit/ansible/playbooks/00_b.NOOP+remote.yml'

    PLAY [localhost] ***************************************************************
    TASK [Gathering Facts] *********************************************************
    TASK [01. Timestamp] ***********************************************************
    ok: [localhost] => {
        msg": "2022-10-17 13:46:33 CEST

    TASK [02. Ansible version] *****************************************************
    ok: [localhost] => {
        msg": "Ansible 2.13.3, Python 3.10.6, /usr/local/Cellar/ansible/6.3.0/libexec/bin/python3.10

    PLAY [cloud,pi] ****************************************************************
    TASK [10. Remote timestamp] ****************************************************
    TASK [debug] *******************************************************************
    ...

    ok: [ocean] => {
            "stdout_lines": [
             $ date; whoami; hostname -f; uptime
                Mon Oct 17 13:46:35 CEST 2022
                jdb
                ocean.umi.ch
                 13:46:35 up 5 days,  1:59,  1 user,  load average: 0.26, 0.08, 0.02

    PLAY RECAP ************************************************************************************************************************************************
    ocean   : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

<br>

Versus this:

```
mac$ export APE_HOSTS='ocean';  ansible-playbook 00_b.NOOP+remote.yml
    PLAY [localhost] ******************************************************************************************************************************************

    TASK [Gathering Facts] ************************************************************************************************************************************
    ok: [localhost]

    TASK [01. Timestamp] **************************************************************************************************************************************
    ok: [localhost] => {
        "msg": "2022-10-17 13:48:00 CEST"
    }

    TASK [02. Ansible version] ********************************************************************************************************************************
    ok: [localhost] => {
        "msg": "Ansible 2.13.3, Python 3.10.6, /usr/local/Cellar/ansible/6.3.0/libexec/bin/python3.10 "
    }

    PLAY [ocean] *******************************************************************************************************************************************

    TASK [10. Remote timestamp] *******************************************************************************************************************************
    changed: [ocean]

    TASK [debug] **********************************************************************************************************************************************
    ok: [ocean] => {
        "tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict": {
            "stderr_lines": [],
            "stdout_lines": [
                "$ date; whoami; hostname -f; uptime",
                "Mon Oct 17 13:48:03 CEST 2022",
                "jdb",
                "ocean.umi.ch",
                " 13:48:03 up 5 days,  2:00,  1 user,  load average: 0.06, 0.06, 0.01"
            ]
        }
    }

    PLAY RECAP ************************************************************************************************************************************************
ocean      : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```
<br>


## Logging

Beware the side-effect of security logging with Ansible via SSH.<br>
Ongoing actions are needed to reduce the logs, and keep them manageable.<br/>
These can go elsewhere - eg, stream to Elasticsearch log service.

```
cow_$ date; whoami; uname -n
    Wed Oct 19 19:56:18 CEST 2022
    jdb
    cow-pi

cow_$ sudo journalctl | head
    Jun 07 12:49:55 cow-pi kernel: Booting Linux on physical CPU 0x0000000000 [0x410fd083]
    Jun 07 12:49:55 cow-pi kernel: Linux version 5.15.0-1011-raspi (buildd@bos02-arm64-014) (gcc (Ubuntu 11.2.0-19ubuntu1) 11.2.0, GNU ld (GNU Binutils for Ubuntu) 2.38) #13-Ubuntu SMP PREEMPT Thu Jun 2 11:44:34 UTC 2022 (Ubuntu 5.15.0-1011.13-raspi 5.15.35)
    Jun 07 12:49:55 cow-pi kernel: random: fast init done
    Jun 07 12:49:55 cow-pi kernel: Machine model: Raspberry Pi 4 Model B Rev 1.4

cow_$ sudo journalctl | tail
    Oct 19 19:57:20 cow-pi sudo[3913]:   jdb : TTY=pts/0 ; PWD=/home/jdb ; USER=root ; COMMAND=/usr/bin/journalctl
    Oct 19 19:57:20 cow-pi sudo[3913]: pam_unix(sudo:session): session opened for user root(uid=0) by jdb(uid=1001)
    Oct 19 19:57:25 cow-pi sudo[3913]: pam_unix(sudo:session): session closed for user root

cow_$ sudo journalctl | wc -l
    216'098


# - Many log lines are related to Ansible work.

cow_$ sudo journalctl | grep -i ansible| wc -l
    12'633

cow_$ alias cutt='cut -c1-132'

# - Lot's of Ansible invocations, this is my standard management tool
cow_$ sudo journalctl | grep -i ansible | tail | cutt
    Oct 19 17:20:42 cow-pi python3[2046]: ansible-ansible.builtin.apt Invoked with update_cache=True state=present update_cache_retries=
    Oct 19 17:22:04 cow-pi sudo[2370]:      jdb : TTY=pts/0 ; PWD=/home/jdb ; USER=root ; COMMAND=/bin/sh -c echo BECOME-SUCCESS-vwhlrcf
    Oct 19 17:22:05 cow-pi python3[2372]: ansible-ansible.builtin.apt Invoked with autoclean=True autoremove=True name=* state=latest up
    Oct 19 19:49:05 cow-pi python3[3414]: ansible-ansible.legacy.command Invoked with _raw_params=echo "$ sudo systemctl daemon-reload"
    Oct 19 19:49:44 cow-pi python3[3521]: ansible-ansible.legacy.setup Invoked with gather_subset=['all'] gather_timeout=10 filter=[] fa
    Oct 19 19:49:47 cow-pi python3[3627]: ansible-ansible.legacy.command Invoked with executable=/bin/bash _raw_params=echo "$ sudo syst

# - Each Ansible invocation has much logging.
cow_$ sudo journalctl | grep 'Oct 19 19:49:47' | cutt
    Oct 19 19:49:47 cow-pi python3[3627]: ansible-ansible.legacy.command Invoked with executable=/bin/bash _raw_params=echo "$ sudo syst
    Oct 19 19:49:47 cow-pi sudo[3629]:      jdb : TTY=pts/0 ; PWD=/home/jdb ; USER=root ; COMMAND=/usr/bin/systemctl daemon-reload
    Oct 19 19:49:47 cow-pi sudo[3629]: pam_unix(sudo:session): session opened for user root(uid=0) by jdb(uid=1001)
    Oct 19 19:49:47 cow-pi systemd[1]: Reloading.
```
<br>


## Agile theme
This repository has useful working examples, quickly and iteratively adaptable to new environments.
<br/>


## License
Copyright &copy; 2019, Mountain Informatik GmbH<br>
All rights reserved.<br>
Permission to share is granted under the terms of the **Server Side Public License**<br>
https://www.mongodb.com/licensing/server-side-public-license
<br>


