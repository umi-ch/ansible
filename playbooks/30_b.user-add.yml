# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.
# v_ansible='7.3.2'

# - Add a user account. Eg:
#   ape 30_b jdb 'John' NP 'adm,docker,plugdev,sudo'

# - FYI:
#   https://docs.ansible.com/ansible/latest/collections/ansible/builtin/user_module.html
#   https://www.edureka.co/community/83388/warning-appears-hashed-argument-encrypted-module-properly
#   https://docs.ansible.com/ansible/latest/collections/ansible/builtin/include_vars_module.html
#   https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#defining-variables-in-included-files-and-roles
#   https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html
#   https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html#ansible-collections-ansible-builtin-template-module

# - FYI:
#   Default umask is set in /etc/login.defs

# - ToDo:
#   Allow explicitly setting uid & gid


---

- hosts: localhost
  become: no
  gather_facts: yes

  vars:
    APE_v:      "{{ lookup ('env', 'APE_verbose') or 0 }}"                  # - verbose mode
    LOOP_v_tag: "{{ ((APE_v | int >= 1)) | ternary ('always', 'never') }}"  # - verbose-only LOOP
    steps:      "{{ lookup ('env', 'APE_STEPS') | default ([], true) }}"    # - list of sections (optional)

    APE_var_1: "{{ lookup ('env', 'APE_var_1') }}"
    APE_var_2: "{{ lookup ('env', 'APE_var_2') }}"
    APE_var_3: "{{ lookup ('env', 'APE_var_3') | default('NP',true) }}"
    APE_var_4: "{{ lookup ('env', 'APE_var_4') | default('',  true) }}"     # - string. Eg: '' or 'sudo' or 'sudo,adm'

  tasks:

    - name: "00. Timestamp"
      debug:
        msg: "{{ ansible_date_time.date }} {{ ansible_date_time.time }} {{ ansible_date_time.tz }}"

    - debug:
        msg: "APE verbose mode: APE_v = {{ APE_v }}, LOOP_v_tag = {{ LOOP_v_tag }}"
      when:
        (APE_v | int >= 1)

    - name: "00. Sanity test: Is APE_var_1 (User account) defined?"
      fail: msg="User account is not defined, in APE_var_1. Abort."
      when: APE_var_1 is not defined or APE_var_1 == ""

    - name: "00. Sanity test: Is APE_var_2 (User name) defined?"
      fail: msg="User name (gecos comment) is not defined, in APE_var_2. Abort."
      when: APE_var_2 is not defined or APE_var_2 == ""

    - name: "00. FYI"
      debug:
        msg:
            - "FYI:"
            - "APE_var_1: {{ APE_var_1 }}"
            - "APE_var_2: {{ APE_var_2 }}"
            - "APE_var_3: {{ APE_var_3 }}"
            - "APE_var_4: {{ APE_var_4 }}"
      when:
        (APE_v | int >= 1)


- hosts: "{{ lookup ('env', 'APE_HOSTS') }}"
  gather_facts: yes

  vars:
    APE_v:      "{{ lookup ('env', 'APE_verbose') or 0 }}"                  # - verbose mode
    LOOP_v_tag: "{{ ((APE_v | int >= 1)) | ternary ('always', 'never') }}"     # - verbose-only LOOP
    steps:      "{{ lookup ('env', 'APE_STEPS') | default ([], true) }}"    # - list of sections (optional)

    APE_var_1: "{{ lookup ('env', 'APE_var_1') }}"
    APE_var_2: "{{ lookup ('env', 'APE_var_2') }}"
    APE_var_3: "{{ lookup ('env', 'APE_var_3') | default('NP',true) }}"
    APE_var_4: "{{ lookup ('env', 'APE_var_4') | default('',  true) }}"     # - string. Eg: '' or 'sudo' or 'sudo,adm'

    x_my_user_groups:   "{{ my_user_groups          | default ('', true) }}"
    xx_keys:            "{{ inbound_authorized_keys | default ('', true) }}"


  tasks:

    - name: '10. Timestamp'
      shell: 'echo "$ date; whoami; hostname -f"; date; whoami; hostname -f'
      register: tout

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('10' in steps) or (steps | length == 0))


    - name: '11. Sanity check: inbound_authorized_keys defined'
      fail:  msg="Variable inbound_authorized_keys is not defined"
      when:
        (('11' in steps) or (steps | length == 0))  and
        (not xx_keys)

    - name: "11. FYI"
      debug:
        msg:
            - "FYI:"
            - "x_my_user_groups: '{{ x_my_user_groups }}'"
            - "xx_keys: '{{ xx_keys }}'"
      when:
        (APE_v | int >= 1)


    - name: "12. Sanity test: sudo access"
      become: true
      shell: |
        echo '$ sudo -l'
        sudo -l
      register: tout
      when:
        (('12' in steps) or (steps | length == 0))

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('12' in steps) or (steps | length == 0))  and
        (APE_v | int >= 1)


    - name: '20. Add user account "{{ APE_var_1 }}", as a "no-password" account'
      become: true

      ansible.builtin.user:
        comment:            "{{ APE_var_2 }}"
        generate_ssh_key:   "yes"
        groups:             "{{ APE_var_4 }},{{ x_my_user_groups }}"        # - "sudo"
        name:               "{{ APE_var_1 }}"
        ## password_lock:   "yes"
        password:           "*"     # - Account has no password-access, but ssh-keys work
        remove:             "no"
        shell:              "/bin/bash"
        ssh_key_comment:    "{{ APE_var_1 }}_SSH_on_{{ inventory_hostname }}"
        ssh_key_file:       ".ssh/id_ed25519"
        ssh_key_type:       "ssh-ed25519"
        state:              present
        umask:              022         # - Default: owner write, group read, others none
        ## uid:             2002        # - ToDo: Parameterize this "hack patch".
      register: tout
      when:
        (('20' in steps) or (steps | length == 0))  and
        (APE_var_3 == "NP")

    - debug:
        var: tout
      when:
        (('20' in steps) or (steps | length == 0))  and
        (APE_v | int >= 1)


    - name: '21. Add user account "{{ APE_var_1 }}", with a specified password.'
      become: true
      vars:
        my_password: "{{ APE_var_3 }}"      ## - Eg: 'stub-Change-Me'

      ansible.builtin.user:
        comment: "{{ APE_var_2 }}"
        generate_ssh_key: "yes"
        groups: "{{ APE_var_4 }},{{ x_my_user_groups }}"        # - "sudo"
        name: "{{ APE_var_1 }}"
        password: "{{ my_password | password_hash('sha512') }}"
        password_lock: no
        remove: no
        shell: "/bin/bash"
        ssh_key_comment: "{{ APE_var_1 }}_SSH_on_{{ inventory_hostname }}"
        ssh_key_file: ".ssh/id_ed25519"
        ssh_key_type: "ssh-ed25519"
        state: present
        ## umask: 022                   # - default: owner write, group read

      register: tout
      when:
        (('21' in steps) or (steps | length == 0))  and
        (APE_var_3 != "NP")

    - debug:
        var: tout
      when:
        (('21' in steps) or (steps | length == 0))  and
        (APE_v | int >= 1)


    # - SSH has special requirements for password-less logins.
    #   - Default Ubuntu SSHD allows logins to locked accounts, if PAM is enabled.
    #   - John's OpenSSHD does not use PAM and does not allow this, so a fix is needed.
    #
    # https://unix.stackexchange.com/questions/193066/how-to-unlock-account-for-public-key-ssh-authorization-but-not-for-password-aut
    #
    # - Tricky point:
    #   This fine distinction is not visible with 'passwd -S',
    #   but only with the questionable practice of grepping /etc/shadow !  :-(
    #
    # - Eg:
    #   -> inbound ssh as tester works with Ubuntu SSHD & PAM, but not Johns OpenSSH no-PAM
    #      $ ssh -p 2223  tester@cow-pi.home.umi.ch whoami  # - fails
    #
    #   $ sudo passwd -S tester
    #   tester L 11/11/2022 0 99999 7 -1
    #
    #   $ sudo grep tester /etc/shadow    # - Never do this... except now!
    #   tester:!:19130:0:99999:7:::
    #
    # - Then:
    #   -> Inbound ssh as tester works with both Ubuntu SSHD & PAM, and Johns OpenSSH no-PAM
    #      $ ssh -p 2223  tester@cow-pi.home.umi.ch whoami  # - works
    #
    #   $ sudo usermod -p '*' tester  # - Inbound ssh works, to a locked account.
    #
    #   $ sudo passwd -S tester
    #   tester L 11/11/2022 0 99999 7 -1
    #
    #   $ sudo grep tester /etc/shadow    # - Never do this... except now!
    #   tester:*:19130:0:99999:7:::

    - name: "22. Fixups for user account: {{ APE_var_1 }}"
      become: true
      shell: |
        echo "$ sudo chmod  g-w,o-w  ~{{ APE_var_1 }}   # - Needed, else inbound ssh fails."
        chmod  g-w,o-w  ~{{ APE_var_1 }}
        echo
        echo "$ sudo passwd -S {{ APE_var_1 }}"
        passwd -S {{ APE_var_1 }}  2>&1
        echo
        echo "$ sudo grep {{ APE_var_1 }} /etc/shadow   # - Never do this... except now!"
        sudo grep {{ APE_var_1 }} /etc/shadow
        echo
        echo "$ sudo usermod -p '*' {{ APE_var_1 }}     # - Inbound ssh works, to a locked account."
        usermod -p '*' {{ APE_var_1 }}
        echo
        echo "$ sudo grep {{ APE_var_1 }} /etc/shadow   # - Never do this... except now!"
        sudo grep {{ APE_var_1 }} /etc/shadow
      register: tout
      when:
        (('22' in steps) or (steps | length == 0))

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('22' in steps) or (steps | length == 0))  and
        (APE_v | int >= 1)


    - name: "23. Show user account"
      become: true
      shell: |
        echo "$ getent passwd {{ APE_var_1 }}"
        getent passwd {{ APE_var_1 }}  2>&1
        echo
        echo "$ ls -ladhF ~{{ APE_var_1 }}"
        ls -ladhF ~{{ APE_var_1 }}  2>&1
        echo
        echo "$ sudo passwd -S {{ APE_var_1 }}"
        passwd -S {{ APE_var_1 }}  2>&1
        echo
        echo "$ sudo grep {{ APE_var_1 }} /etc/shadow    # - Never do this... except now!"
        sudo grep {{ APE_var_1 }} /etc/shadow
        echo
        echo "$ sudo groups {{ APE_var_1 }}"
        groups {{ APE_var_1 }}  2>&1
      register: tout
      when:
        (('23' in steps) or (steps | length == 0))

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('23' in steps) or (steps | length == 0))


    - name: '27. Update file  ~{{ APE_var_1 }}/.ssh/authorized_keys  from template'
      become: yes
      ansible.builtin.copy:
        content: "{{ xx_keys }}"
        dest: "~{{ APE_var_1 }}/.ssh/authorized_keys"
      register: tout
      when:
        (('27' in steps) or (steps | length == 0))

    - debug:
        var: tout
      when:
        (('27' in steps) or (steps | length == 0))  and
        (APE_v | int >= 1)


    - name: '28. chown  ...  ~{{ APE_var_1 }}/.ssh/authorized_keys'
      become: true
      ansible.builtin.file:
        path: "~{{ APE_var_1 }}/.ssh/authorized_keys"
        owner: "{{ APE_var_1}}"
      register: tout
      when:
        (('28' in steps) or (steps | length == 0))

    - debug:
        var: tout
      when:
        (('28' in steps) or (steps | length == 0))  and
        (APE_v | int >= 1)


    - name: '29. Show ~{{ APE_var_1 }}/.ssh/authorized_keys'
      become: yes
      shell: |
        echo "$ ls -lahF ~{{ APE_var_1 }}/.ssh/"
        ls -lahF ~{{ APE_var_1 }}/.ssh/
        echo
        echo "$ cat ~{{ APE_var_1 }}/.ssh/authorized_keys"
        cat ~{{ APE_var_1 }}/.ssh/authorized_keys
        echo
        echo "$ cat ~{{ APE_var_1 }}/.ssh/id*.pub"
        cat ~{{ APE_var_1 }}/.ssh/id*.pub
        true
      register: tout
      when:
        (('29' in steps) or (steps | length == 0))

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('29' in steps) or (steps | length == 0))  and
        (APE_v | int >= 1)

