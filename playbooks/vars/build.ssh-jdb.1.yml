# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.
# v_ansible='7.4.1'

# - Parameters to build OpenSSD with JDB's password tracker, for Honeypots.

# - FYI:
#   $ i_ansible_sshd_jdb_build


    ##################
    # App parameters #
    ##################
    #
    # - Align these with the build- and installation- parameters.

my_app:                             # - Used in multiple places.
    sshd-jdb

my_app_dir:                         # - Installation location.
   /usr/local/{{ my_app }}          # - Earlier:  /usr/local/ssh-jdb/


my_app_release:
    9.0p1

# - Debian package conventions support these terms:
#   - amd64
#   - arm64
#
my_app_arch:
    amd64

# - Eg:
#   $ my_app_tag="$(egrep '^NAME' /etc/os-release | cut -f2 -d=)_$(egrep '^VERSION_ID' /etc/os-release | cut -f2 -d=)"
#   $ my_app_tag=$(echo "$my_app_tag" | sed -e 's/"//g')
#
my_app_tag:
    Ubuntu_22.04

# - Eg:
#   $ my_app_date=$(date '+%Y-%m-%d')
#
my_app_date:
    2022-11-30

# - This build-hostname is just for convenience and introspection.
my_build_host:
    awx

# - Eg:
#   sshd-jdb.9.0p1.amd64.Ubuntu_22.04_awx.2022-11-30.deb
#    sshd-jdb.logs.amd64.Ubuntu_22.04_awx.2022-11-30.tgz
#
my_app_deb:
    "{{ my_app }}.{{ my_app_release }}.{{ my_app_arch }}.{{ my_app_tag }}_{{ my_build_host }}.{{ my_app_date }}.deb"

my_app_logs_tgz:
    "{{ my_app }}.logs.{{ my_app_arch }}.{{ my_app_tag }}_{{ my_build_host }}.{{ my_app_date }}.tgz"



    ####################
    # Build parameters #
    ####################
    #
    # - Variants: the build server may, or may not, have direct Git repo access.

my_build_src_name:                  # - Eg: openssh-9.0p1
    "openssh-{{ my_app_release }}"


# - Source code URL.
#   Set this to a "highly available" location, which is relatively nearby.
#
my_build_src_url:
    "https://mirror.ungleich.ch/pub/OpenBSD/OpenSSH/portable/{{ my_build_src_name }}.tar.gz"


# - Define the top location to build stuff.
#   -> Use an absolute pathname.
#   -> Access is required by the ansible_user account, both with and without sudo.
#
my_build_src_dir:
    /home/jdb/src/{{ my_app }}

my_build_pkg_dir:
    "{{ my_build_src_dir}}/zPkgs"


# - Here's a list of tuples:
#   [ "file to be replaced", "replacing file" ]
#
# - If the first string is empty, the replacing file is simply added into the source dir.
#   Another copy is made into the "zCopyrights" directory, for license compliance.
#
my_build_patch_files:
    - [ "auth-passwd.c", "auth-passwd.c-v1.48" ]
##  - [ "", "stub-eroni"]


# - Parameters to ./configure
#
my_build_configure:
    "--prefix={{ my_app_dir }}  --sysconfdir={{ my_app_dir }}/etc/ssh/  --with-pid-dir=/var/run  --with-pam"


# - Define a location where the Makefile will "install" built stuff, prior to packaging.
#   -> Use an absolute pathname.
#   -> Access is required by the ansible_user account, both with and without sudo.

my_build_dest_dir:
    "{{ my_build_src_dir }}/zDist"

my_build_libs_check:
    - "zlib1g-dev"


# - Define the preferred sshd config files, and make symlinks.
#   Sym links are made.

my_build_sshd_conf_template:        # - Sym link:  sshd_config.d/sshd.conf
    sshd-2223.conf-template

my_build_sshd_conf_main:            # - Sym link:  sshd_config
    sshd_config-9.0p1



    #########################
    # Repository parameters #
    #########################
    #
    # - These paths are relative to the Ansible server.


# - Source code, for the build server:
#
my_build_repo_dir:
    "~/zGit/pi/builds/{{ my_app }}.zSrc/"


# - Packages, for the target destination server:
#
my_app_pkg_dir:
    "{{ my_app_pkg_dir_sshd_jdb }}"

my_app_pkg:
    "{{ my_app_pkg_dir }}/{{ my_app_deb }}"


