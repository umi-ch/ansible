## - source this file in a Bash shell -- ##
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

# v_ansible='7.0.0'

echo "$ f_apep 22"
f_apep 22

rc=$?
if [[ $rc -ne 0 ]]; then
    echo "- Error, rc: $rc"
    return $rc
fi

echo
echo "$ env | grep APE_PORT"
env | grep APE_PORT

