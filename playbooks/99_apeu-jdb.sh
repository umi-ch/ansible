## - source this file in a Bash shell -- ##
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

# v_ansible='7.0.0'

echo "$ f_apeu jdb"
f_apeu jdb

rc=$?
if [[ $rc -ne 0 ]]; then
    echo "- Error, rc: $rc"
    return $rc
fi

echo
echo "$ env | grep APE_USER"
env | grep APE_USER

