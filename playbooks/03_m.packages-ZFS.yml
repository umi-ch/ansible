# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.
# v_ansible='8.0.0'

# - Install packages for Ubuntu Linux.
#   -> ZFS
#   -> Anticipated platfrom is PI, not Cloud.

# - FYI:
#   Installing ZFS utilities might/will rebuild the kernel configs.
#   This seems to work well, BUT with a strong dependency on deterministic
#     root disk mounting after reboots.
#   Unforunately, "Raspberry PI 4" disk device assignment, and
#     boot disk selected, seems to have ad-hoc variance.
#   Be careful here, and be sure to "fix" the Linux boot disk in
#     /boot/firmware/cmdline.txt
#
# - FYI:
#   https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#raspberry-pi-4-boot-flow


---

- hosts: localhost
  gather_facts: yes

  tasks:

    - name: "01. Timestamp"
      debug:
        msg: "{{ ansible_date_time.date }} {{ ansible_date_time.time }} {{ ansible_date_time.tz }}"


- hosts: "{{ lookup ('env', 'APE_HOSTS') }}"

  vars:
    APE_v:      "{{ lookup ('env', 'APE_verbose') or 0 }}"                  # - verbose mode
    LOOP_v_tag: "{{ (APE_v | int > 0) | ternary ('always', 'never') }}"     # - verbose-only LOOP
    steps:      "{{ lookup ('env', 'APE_STEPS') | default ([], true) }}"    # - list of sections (optional)

  tasks:

    #  - This info may be useful for troubleshooting installation errors.
    #
    - name: "05. PI disk info"
      shell: |
        echo "$ date; uptime; uname -r"
        date; uptime; uname -r
        #
        # - Special tracking for Raspberry PI with multiple USB bootable disks:
        if [[ -d /boot/firmware/ ]]; then
            echo
            echo "$ ls -lhFt /boot/ | grep -v ^total | head -3"
            ls -lhFt /boot/ | grep -v ^total | head -3
            #
            echo
            ## ls -lh /boot/firmware/{cmdline.*,*start.elf*}  2>&1
            echo "$ ls -lht /boot/firmware | grep -v ^total | head -3"
            ls -lht /boot/firmware | grep -v ^total | head -3
            #
            echo
            echo "$ ls -lhF /boot/firmware/zTags/ | grep -v ^total"
            ls -lhF /boot/firmware/zTags/  2>&1 | grep -v ^total
            #
            echo
            echo "$ ls -lhF /zTags/ansible.*"
            ls -lhF /zTags/ansible.*  2>&1
            #
            echo
            echo "$ df -h | grep -v tmpfs"
            df -h | grep -v tmpfs  2>&1
            #
            dirs="/dev/disk/by-id /dev/disk/by-partuuid"
            disks=$(df -h | grep ^/ | cut -f1 -d' ' | cut -f3 -d/ | sort)
            for disk in ${disks}; do
                echo
                echo "$ ls -lhF ${dirs}/ | grep ${disk}"
                ls -lhF ${dirs}/ 2>&1 | grep ${disk} | perl -ple 's/^(\S+\s+){8}(\S.*)$/$2/;'
            done
            #
            echo
            echo "$ cat /boot/firmware/cmdline.txt | perl ..."
            cat /boot/firmware/cmdline.txt | perl -ple 's/^.*?(root=\S+)\s.*$/$1/;'
        fi
      args:
        executable: /bin/bash
      register: tout
      when:
        (('05' in steps) or (steps | length == 0))

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('05' in steps) or (steps | length == 0))  and
        ( (APE_v | int >= 0)  or
          (('rc' in tout) and (tout.rc != 0)) )


    - name: "10. Install zfsutils-linux"
      become: true
      ignore_errors: true
      apt:
        pkg:
        - zfsutils-linux
        - zfsutils
      register: tout
      when:
        ('10' in steps) or (steps | length == 0)

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('10' in steps) or (steps | length == 0))  and
        ( (('rc'     in tout) and (tout.rc     != 0))    or
          (('failed' in tout) and (tout.failed == true)) or
          (APE_v | int >= 1) )

    - name: "10. Installation failed?"
      fail:
        msg: "zfsutils-linux installation failed."
      when:
        (('10' in steps) or (steps | length == 0))  and
        ( (('rc'     in tout) and (tout.rc     != 0))    or
          (('failed' in tout) and (tout.failed == true)) )


    - name: "11. Install zfs-dkms"
      become: true
      ignore_errors: true
      apt:
        pkg:
        - zfs-dkms
      register: tout
      when:
        ('11' in steps) or (steps | length == 0)

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('11' in steps) or (steps | length == 0))  and
        ( (('rc'     in tout) and (tout.rc     != 0))    or
          (('failed' in tout) and (tout.failed == true)) or
          (APE_v | int >= 1) )

    - name: "11. Installation failed?"
      fail:
        msg: "zfs-dmks installation failed."
      when:
        (('11' in steps) or (steps | length == 0))  and
        ( (('rc'     in tout) and (tout.rc     != 0))    or
          (('failed' in tout) and (tout.failed == true)) )


    # - Bug fix: zfs-dracut has a library dependency which is now (?) a sub-directory
    #   below where it's expected. A simple symlink fixes it.
    #
    - name: "12. Bug fix? symlink to libgcc_s.so"
      become: true
      ignore_errors: true
      shell:  |
        echo "$ ls -lhF /usr/lib*/gcc/**/libgcc_s.so*"
        out=$(ls -lhF /usr/lib*/gcc/**/libgcc_s.so*  2>&1)      # - Failure expected.
        rc=$?
        echo "${out}"
        if [[ $rc -ne 0 ]]; then
            echo "- Expected error, rc: $rc"
        else
            echo "- File found, no bug-fix needed."
            exit 0
        fi
        #
        echo
        echo "$ ls -lhF /usr/lib*/gcc/*/*/libgcc_s.so*"
        out=$(ls -lhF /usr/lib*/gcc/*/*/libgcc_s.so*  2>&1)     # - Failure NOT expected.
        rc=$?
        echo "${out}"
        if [[ $rc -ne 0 ]]; then
            echo "- Error, rc: $rc"
            exit $rc
        fi
        #
        echo
        echo "$ find /usr/lib*/gcc/* -maxdepth 0 -type d"
        out=$(find /usr/lib*/gcc/* -maxdepth 0 -type d  2>&1)
        rc=$?
        echo "${out}"
        if [[ $rc -ne 0 ]]; then
            echo "- Error, rc: $rc"
            exit $rc
        fi
        #
        echo
        dir=$(echo "${out}" | head -1)          # - Edge case, more than one directory.
        echo "$ cd ${dir}/"
        cd "${dir}/"  2>&1
        rc=$?
        echo "${out}"
        if [[ $rc -ne 0 ]]; then
            echo "- Error, rc: $rc"
            exit $rc
        fi
        #
        echo
        echo "$ ls -lhF"
        out=$(ls -lhF  2>&1)
        rc=$?
        echo "${out}"
        if [[ $rc -ne 0 ]]; then
            echo "- Error, rc: $rc"
            exit $rc
        fi
        #
        echo
        echo "$ find . -type f -name 'libgcc_s.so*' | sort | tail -1"
        out=$(find . -type f -name 'libgcc_s.so*'  2>&1)
        rc=$?
        echo "${out}"
        if [[ $rc -ne 0 ]]; then
            echo "- Error, rc: $rc"
            exit $rc
        fi
        #
        echo
        file=$(echo "${out}" | sort | tail -1)
        echo "$ ln -s ${file} ."
        ln -s ${file} .  2>&1
        rc=$?
        echo "${out}"
        if [[ $rc -ne 0 ]]; then
            echo "- Error, rc: $rc"
            exit $rc
        fi
        #
        echo
        echo "$ ls -lhF"
        out=$(ls -lhF  2>&1)
        rc=$?
        echo "${out}"
        if [[ $rc -ne 0 ]]; then
            echo "- Error, rc: $rc"
            exit $rc
        fi
        #
        echo
        echo "$ ls -lhF /usr/lib*/gcc/**/libgcc_s.so*"
        out=$(ls -lhF /usr/lib*/gcc/**/libgcc_s.so*  2>&1)
        rc=$?
        echo "${out}"
        if [[ $rc -ne 0 ]]; then
            echo "- Error, rc: $rc"
            exit $rc
        fi
        #
        echo
        echo "$ md5sum /usr/lib*/gcc/**/libgcc_s.so*"
        out=$(md5sum /usr/lib*/gcc/**/libgcc_s.so*  2>&1)
        rc=$?
        echo "${out}"
        if [[ $rc -ne 0 ]]; then
            echo "- Error, rc: $rc"
            exit $rc
        fi
        #
        echo "-> Bug-fix successful."
        true
      args:
        executable: /bin/bash
      register: tout
      when:
        ('12' in steps) or (steps | length == 0)

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('12' in steps) or (steps | length == 0))  and
        ( (APE_v | int >= 0)  or
          (('rc' in tout) and (tout.rc != 0)) )

    - name: "12. Failed?"
      fail:
        msg: "Failed."
      when:
        (('12' in steps) or (steps | length == 0))  and
        (('rc' in tout) and (tout.rc != 0))



    # https://wiki.ubuntu.com/ZFS
    #   "In addition to be able to have ZFS on root, install zfs-initramfs"
    #
    - name: "13. install zfs-initramfs ? "
      become: true
      ignore_errors: true
      apt:
        pkg:
        - zfs-initramfs
        - initramfs-tools
      register: tout
      when:
        ('13' in steps) or (steps | length == 0)
      tags: [ always ]


    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('13' in steps) or (steps | length == 0))  and
        ( (('rc'     in tout) and (tout.rc     != 0))    or
          (('failed' in tout) and (tout.failed == true)) or
          (APE_v | int >= 1) )
      tags: [ always ]


    - name: "13. Installation failed?"
      fail:
        msg: "zfs-initramfs installation failed."
      when:
        (('13' in steps) or (steps | length == 0))  and
        ( (('rc'     in tout) and (tout.rc     != 0))    or
          (('failed' in tout) and (tout.failed == true)) )
      tags: [ always ]



    # https://dracut.wiki.kernel.org/index.php/Main_Page
    # https://linuxconfig.org/how-to-build-an-initramfs-using-dracut-on-linux
    # https://www.admin-magazine.com/Archive/2020/55/Rebuilding-the-Linux-ramdisk
    #
    # https://packages.debian.org/sid/zfs-dracut
    # https://wiki.gentoo.org/wiki/Dracut
    # https://fedoramagazine.org/initramfs-dracut-and-the-dracut-emergency-shell/
    #
    # $ man dracut
    # $ ls -lhF /etc/kernel/postinst.d/dracut   # - Bash script, generating new images

    - name: "15. Remove zfs-dracut, if present"
      become: true
      ignore_errors: true
      apt:
        autoclean:  yes
        state:      absent
        pkg:
        - zfs-dracut
      register: tout
      when:
        ('15' in steps) or (steps | length == 0)
      tags: [ always ]

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('15' in steps) or (steps | length == 0))  and
        ( (('rc'     in tout) and (tout.rc     != 0))    or
          (('failed' in tout) and (tout.failed == true)) or
          (APE_v | int >= 1) )
      tags: [ always ]


    - name: "16. Install zfs-dracut ? "
      become: true
      ignore_errors: true
      apt:
        autoclean:  yes
        state:      latest
        pkg:
        - zfs-dracut
      register: tout
      when:
        ('16' in steps) or (steps | length == 0)
      tags: [ always ]


    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('16' in steps) or (steps | length == 0))  and
        ( (('rc'     in tout) and (tout.rc     != 0))    or
          (('failed' in tout) and (tout.failed == true)) or
          (APE_v | int >= 1) )
      tags: [ always ]


    - name: "16. Installation failed?"
      fail:
        msg: "zfs-dracut installation failed."
      when:
        (('16' in steps) or (steps | length == 0))  and
        ( (('rc'     in tout) and (tout.rc     != 0))    or
          (('failed' in tout) and (tout.failed == true)) )
      tags: [ always ]



    # - This seems not available on Raspberry PI,
    #   and also not necessary these days.
    #
    - name: "19. Install zfs-fuse"
      become: true
      ignore_errors: true
      apt:
        pkg:
        - zfs-fuse
      register: tout
      when:
        ('19' in steps) or (steps | length == 0)
      tags: [ never ]

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('19' in steps) or (steps | length == 0))  and
        ( (('rc'     in tout) and (tout.rc     != 0))    or
          (('failed' in tout) and (tout.failed == true)) or
          (APE_v | int >= 1) )
      tags: [ never ]


    - name: "20. Review /var/run/reboot-required*"
      shell:  |
        echo "$ ls -lhF /var/run/reboot-required*"
        ls -lhF /var/run/reboot-required*  2>&1
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo '-> Reboot not required.'
            exit 0
        fi
        #
        for file in $(/bin/ls /var/run/reboot-required*); do
            echo
            echo "$ cat ${file}"
            cat ${file}  2>&1
        done
        true
      args:
        executable: /bin/bash
      register: tout
      when:
        (('20' in steps) or (steps | length == 0))

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
       (('20' in steps) or (steps | length == 0))  and
        (APE_v | int >= 0)


