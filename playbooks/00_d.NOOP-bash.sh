## - source this file in a Bash shell -- ##
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

# v_ansible='7.0.0'

echo "Hello world"

export MY_TEST=TEST


# - This is OK:
return 0

# - Do not do this:
##  exit 0

