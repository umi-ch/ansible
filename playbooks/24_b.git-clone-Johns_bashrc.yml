# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.
# v_ansible='7.4.0'

# - Git clone bashrc
#   -> Assume SSH authentication to "git clouds" is available,
#      either with z64 or SSH-agent forwarding.

# - FYI:
#   https://docs.ansible.com/ansible/latest/collections/ansible/builtin/git_module.html
#   https://docs.ansible.com/ansible/latest/user_guide/playbooks_debugger.html

# - FYI: "SSH agent mega-notes" at the end


---

- hosts: localhost
  gather_facts: yes

  vars:
    APE_v:      "{{ lookup ('env', 'APE_verbose') or 0 }}"                  # - verbose mode
    LOOP_v_tag: "{{ (APE_v | int > 0) | ternary ('always', 'never') }}"     # - verbose-only LOOP

    APE_var_1:  "{{ lookup ('env', 'APE_var_1') }}"         # - username
    APE_var_2:  "{{ lookup ('env', 'APE_var_2') }}"         # - bashrc-id (optional)

    ## x_my_repos: ...                                      # - No: Hostvar is not localhost

  tasks:

    - debug:
        msg: "APE verbose mode: APE_v = {{ APE_v }}, LOOP_v_tag = {{ LOOP_v_tag }}"
      when:
        APE_v | int > 0

    - name: 'Local Timestamp'
      debug:
        msg: "{{ ansible_date_time.date }} {{ ansible_date_time.time }} {{ ansible_date_time.tz }}"


    - name: "Sanity test 1: APE_var_1 (User account)"
      fail: msg="User account is not defined, in APE_var_1."
      when: APE_var_1 is not defined or APE_var_1 == ""


- hosts: "{{ lookup ('env', 'APE_HOSTS') }}"
  gather_facts: no

  vars:
    APE_v:      "{{ lookup ('env', 'APE_verbose') or 0 }}"                  # - verbose mode
    LOOP_v_tag: "{{ (APE_v | int > 0) | ternary ('always', 'never') }}"     # - verbose-only LOOP

    APE_var_1:  "{{ lookup ('env', 'APE_var_1') }}"         # - username
    APE_var_2:  "{{ lookup ('env', 'APE_var_2') }}"         # - bashrc-id (optional)

    x_my_repo_p:            "{{ my_repo_prefix      | default ('', true) }}"    # - prefix
    x_my_repos:             "{{ my_repos            | default ([], true) }}"    # - repository list
    x_my_web_repo_dir:      "{{ my_web_repo_dir     | default ('', true) }}"    # - repository directory
    x_my_repo_zrc:          "{{ my_repo_zrc         | default ('', true) }}"    # - repo linked from .zrc

    ## # - Stub: See below.
    ## befores: []

  tasks:

    - name: 'Sanity test: Repositories 1'
      fail: msg="No respositories defined, in var my_repos"
      when: not x_my_repos


    - name: 'Sanity test: Repositories 2'
      debug:
        msg: "{{ item }}"
      loop:
        "{{ x_my_repos }}"
      ## tags: [ "{{ LOOP_v_tag }}" ]


    - name: 'Sanity test: Repo prefix'
      fail: msg="Respository prefix not defined, in var my_repo_p"
      when: not x_my_repo_p


    - name: 'Sanity test: Repo zrc'
      fail: msg="Respository prefix not defined, in var my_repo_zrc"
      when: not x_my_repo_zrc


    - name: 'Sanity test: Repo dir'
      fail: msg="Respository directory not defined, in var my_web_repo_dir"
      when: not x_my_web_repo_dir


    - name: 'Sanity test: Repositories 3'
      debug:
        msg: "{{ x_my_repo_p }}/{{ item }}.git"
      loop:
        "{{ x_my_repos }}"
      ## tags: [ "{{ LOOP_v_tag }}" ]

##  - debug:
##      var: tout


    - name: 'Remote Timestamp 1, no sudo: -> Ignore warning about "Module remote_tmp created".'
      become:      true
      become_user: "{{ APE_var_1 }}"

      shell: |
        echo "-$ date; whoami; hostname -f; pwd"
        date; whoami; hostname -f; pwd
        echo
        echo '-$ env | egrep -i "SSH|ansible_" | sort; echo x-x'
        env | egrep -i 'SSH|ansible_' | sort
        echo x-x
      register: tout

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        APE_v | int > 0


    - name: 'Remote Timestamp 2, with sudo:'
      become:      true
      become_user: "{{ APE_var_1 }}"
      become_flags: "-E"                # - sudo -E

      vars:
        # - Used when "become_user" != root, and become_flags = "-E"
        ansible_remote_tmp: "~{{ APE_var_1 }}/.ansible/tmp/"

      shell: |
        echo "-$ date; whoami; hostname -f; pwd"
        date; whoami; hostname -f; pwd
        echo
        echo '-$ env | egrep -i "SSH|ansible_" | sort; echo x-x'
        env | egrep -i 'SSH|ansible_' | sort
        echo x-x
      register: tout

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines


    - name: "mkdir -p {{ x_my_web_repo_dir }}"
      become:      true
      become_user: "{{ APE_var_1 }}"

      shell: |
        echo -n "$ pwd  : "
        pwd
        echo "$ cd ~"
        cd ~
        echo -n "$ pwd  : "
        pwd
        echo
        echo "-$ mkdir -p {{ x_my_web_repo_dir }}"
        mkdir -p {{ x_my_web_repo_dir }}
        echo
        echo "-$ ls -ladhF ~/zGit*"
        ls -ladhF ~/zGit*
      register: tout

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        APE_v | int > 0


    - name: 'Create sym link zGit'
      become:      true
      become_user: "{{ APE_var_1 }}"

      ansible.builtin.file:
        src:   "{{ x_my_web_repo_dir }}"
        dest:  ~/zGit
        state: link
      register: tout

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - src
          - dest
          - changed
      when:
        APE_v | int > 0


    - name: 'ssh-add -L : without become'
      ignore_errors: yes        # - Handled manually

      shell: |
        echo -n '$ whoami  : '
        whoami
        echo -n "$ pwd  : "
        pwd
        echo "$ cd ~"
        cd ~
        echo -n "$ pwd  : "
        pwd
        echo
        echo -n "$ which ssh-add  = "
        which ssh-add
        echo "$ ssh-add -L"
        ssh-add -L
      register: tout

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (APE_v | int > 0) or (tout.rc | int > 0)

    - name: "Sanity check the result: ssh-add -L"
      fail: msg='No SSH-Agent keys detected, ssh-add -L, rc={{ tout.rc }}'
      when: tout.rc != 0


    - name: 'ssh-add -L : with become "root"'
      become:       true
      become_user:  root
      become_flags: "-E"

      ## vars:
      ##  # - Don't do this, when become_user = root :
      ##  ansible_remote_tmp: "~{{ APE_var_1 }}/.ansible/tmp/"

      shell: |
        echo -n '$ whoami  : '
        whoami
        echo -n "$ pwd  : "
        pwd
        echo
        echo -n "$ which ssh-add  = "
        which ssh-add
        #
        echo
        echo "$ env | grep -i SSH | sort"
        env | grep -i SSH | sort
        #
        echo
        echo "$ ls -ladhF /tmp/ssh*"
        ls -lahdF /tmp/ssh*
        #
        echo
        echo "$ ls -ladhF \${SSH_AUTH_SOCK}"
        ls -lahdF ${SSH_AUTH_SOCK}
        #
        echo
        echo "$ ls -ladhF \$(dirname \${SSH_AUTH_SOCK})"
        ls -lahdF $(dirname ${SSH_AUTH_SOCK})
        #
        echo
        echo "# - Temporary security opening:"
        echo "$ chmod -R 777 \$(dirname \${SSH_AUTH_SOCK})"
        chmod -R 777 $(dirname ${SSH_AUTH_SOCK})
        #
        echo
        echo "$ ls -ladhF \$(dirname \${SSH_AUTH_SOCK})"
        ls -lahdF $(dirname ${SSH_AUTH_SOCK})
        echo
        echo "$ ssh-add -L"
        ssh-add -L      # - Must be last, to set tout.rc
      register: tout

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        APE_v | int > 0


    - name: 'ssh-add -L : with become "{{ APE_var_1 }}"'
      ignore_errors: yes        # - Handled manually

      become:       true
      become_user:  "{{ APE_var_1 }}"
      become_flags: "-E"

      vars:
        # - Used when "become_user" != root, and become_flags = "-E"
        ansible_remote_tmp: "~{{ APE_var_1 }}/.ansible/tmp/"

      shell: |
        echo "$ whoami"
        whoami
        echo "$ pwd"
        pwd
        echo
        echo "$ which ssh-add"
        which ssh-add
        echo
        echo "$ env | grep -i SSH | sort"
        env | grep -i SSH | sort
        echo
        echo "$ ls -ladhF /tmp/ssh*"
        ls -lahdF /tmp/ssh*
        echo
        echo "$ ls -ladhF \${SSH_AUTH_SOCK}"
        ls -lahdF ${SSH_AUTH_SOCK}
        echo
        echo "$ ssh-add -L"
        ssh-add -L      # - Must be last, to set tout.rc
      register: tout

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (APE_v | int > 0) or (tout.rc | int > 0)


    - name: "Sanity check: ssh-add -L"
      fail: msg='No SSH-Agent keys detected, ssh-add -L, rc={{ tout.rc }}'
      when: tout.rc != 0


    - name: 'ssh-add -L : summary as "{{ APE_var_1 }}"'
      become:       true
      become_user:  "{{ APE_var_1 }}"
      become_flags: "-E"

      vars:
        # - Used when "become_user" != root, and become_flags = "-E"
        ansible_remote_tmp: "~{{ APE_var_1 }}/.ansible/tmp/"

      shell: |
        echo "$ whoami"
        whoami
        echo
        echo "$ ls -ladhF \${SSH_AUTH_SOCK}"
        ls -lahdF ${SSH_AUTH_SOCK}
        echo
        echo "$ ssh-add -L"
        ssh-add -L      # - Must be last, to set tout.rc
      register: tout

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines


    - name: "git confirm Bashrc, #1"
      become:       true
      become_user:  "{{ APE_var_1 }}"
      become_flags: "-E"

      vars:
        # - Used when "become_user" != root, and become_flags = "-E"
        ansible_remote_tmp: "~{{ APE_var_1 }}/.ansible/tmp/"

      ansible.builtin.git:
        ## accept_newhostkey: yes   # - Requires v2.12, not in Ubuntu 20.04 LTS
        accept_hostkey: yes
        repo:   "{{ x_my_repo_p }}/{{ item }}.git"
        dest:   ~{{ APE_var_1 }}/zGit/{{ item }}/
        clone:  no
        update: no
      register: tout

      loop:
        "{{ x_my_repos }}"
      ## tags: [ "{{ LOOP_v_tag }}" ]

##  - debug:
##      var: tout


    # - This works, but the output sucks:
    #   -> The "verbose iterator" should not be displayed.
    #   -> Workaround: Fix with post-processing (truncation) in f_ape().

    - debug:
        msg:
            'before: {{ item.before }} , after: {{ item.after }} , repo: {{ item.item }}'
      loop:
        "{{ tout.results }}"
      tags: [ "{{ LOOP_v_tag }}" ]


    # - ToDo: Inline Jinja2 ?
    #   -> This doesn't seem to do anything. More investigation needed.
    #
    - name: 'Diagnostics 2'
      set_fact:
        befores =
            {% for result in tout.results %}
               before {{ result.before }}
               after  {{ result.after }}
            {% endfor %}
      when:
        APE_v | int > 0
      tags: [ never ]

    - debug:
        msg: "{{ item }}"
      loop:
        "{{ befores }}"
      tags: [ never ]


    - name: "git clones"
      ## vars:
      ##   ansible_remote_tmp: "/tmp/ape.SOCKET.{{ APE_var_1 }}/ansible/tmp/"

      become:      true
      become_user: "{{ APE_var_1 }}"
      become_flags: "-E"

      vars:
        # - Used when "become_user" != root, and become_flags = "-E"
        ansible_remote_tmp: "~{{ APE_var_1 }}/.ansible/tmp/"

      ansible.builtin.git:
        ## accept_newhostkey: yes   # - Requires v2.12, not in Ubuntu 20.04 LTS
        accept_hostkey: yes
        repo:   "{{ x_my_repo_p }}/{{ item }}.git"
        dest:   ~{{ APE_var_1 }}/zGit/{{ item }}/
        clone:  yes
        update: yes
      register: tout

      loop:
        "{{ x_my_repos }}"

    - debug:
        msg:
            'before: {{ item.before }} , after: {{ item.after }} , repo: {{ item.item }}'
      loop:
        "{{ tout.results }}"


    # - The SSH_AUTH_SOCK soon disappears after the Playbook finishes.
    #   But to be safe, and for good cleanup, undo the openings made above.
    #
    - name: 'Revoke SSH_AUTH permissions. This Playbook does no more GIT actions.'
      become:       true
      become_user:  root
      become_flags: "-E"

      ## vars:
      ##   # - Don't do this, when become_user = root :
      ##   ansible_remote_tmp: "~{{ APE_var_1 }}/.ansible/tmp/"

      shell: |
        echo "$ date"
        date
        echo "$ whoami"
        whoami
        echo "$ pwd"
        pwd
        echo
        #
        echo "$ ls -ladhF \${SSH_AUTH_SOCK}"
        ls -lahdF ${SSH_AUTH_SOCK}
        echo
        echo "$ ls -ladhF \$(dirname \${SSH_AUTH_SOCK})"
        ls -lahdF $(dirname ${SSH_AUTH_SOCK})
        #
        echo
        echo "$ # - Revoke security openings:"
        echo "$ chmod -R g-rwx,o-rwx \$(dirname \${SSH_AUTH_SOCK})"
        chmod -R g-rwx,o-rwx $(dirname ${SSH_AUTH_SOCK})
        #
        echo
        echo "$ ls -ladhF \${SSH_AUTH_SOCK}"
        ls -lahdF ${SSH_AUTH_SOCK}
        echo
        echo "$ ls -ladhF \$(dirname \${SSH_AUTH_SOCK})"
        ls -lahdF $(dirname ${SSH_AUTH_SOCK})
      register: tout

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        APE_v | int > 0


    - name: 'Show home directory'
      become:       true
      become_user:  "{{ APE_var_1 }}"

      shell: |
        echo "$ whoami"
        whoami
        echo "$ pwd"
        pwd
        echo "$ cd ~"
        cd ~
        echo "$ pwd"
        pwd
        echo
        echo "$ ls -lahF"
        ls -lahF
        echo
        echo "$ ls -lhF ./zGit  {{ x_my_web_repo_dir }}  .zrc"
        ls -lhF ./zGit  {{ x_my_web_repo_dir }}  .zrc
        true
      register: tout
      when:
        APE_v | int > 0

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        APE_v | int > 0


    - name: 'Show zGit'
      become:       true
      become_user:  "{{ APE_var_1 }}"

      shell: |
        echo "$ cd ~"
        cd ~
        echo "$ ls -lhF ./zGit/"
        ls -lhF ./zGit/
      register: tout

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        APE_v | int > 0


    - name: 'Create sym link .zrc'
      become:      true
      become_user: "{{ APE_var_1 }}"

      ansible.builtin.file:
        src:   ./zGit/{{ x_my_repo_zrc }}
     ## dest:  "~{{ APE_var_1 }}/.zrc"
        dest:  ~/.zrc
        state: link
      register: tout

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - src
          - dest
          - changed
      when:
        APE_v | int > 0


    - name: Run linker.sh script
      become:      true
      become_user: "{{ APE_var_1 }}"

      shell: |
        echo "$ whoami"
        whoami
        echo "$ pwd"
        pwd
        echo "$ cd ~"
        cd ~
        echo "$ pwd"
        pwd
        echo
        echo "$ .zrc/binp/linker.sh"
        .zrc/binp/linker.sh
      register: tout

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines


    - name: Check if .bashrc-id exists
      become:      true
      become_user: "{{ APE_var_1 }}"

      ansible.builtin.stat:
        path="~{{ APE_var_1 }}/.bashrc-id"
      register: file


    - name: "If .bashrc-id doesn't exist, either create it, or set it to APE_var_2"
      become:      true
      become_user: "{{ APE_var_1 }}"

      shell: |
        if [[ -n "{{ APE_var_2 }}" ]]; then
            echo "$ echo '{{ APE_var_2 }}' | tee ~/.bashrc-id"
            echo '{{ APE_var_2 }}' | tee ~/.bashrc-id
        else
            echo "$ echo \"\$(date +%s)-\${RANDOM}\" | tee ~/.bashrc-id"
            echo "$(date +%s)-${RANDOM}" | tee ~/.bashrc-id
        fi
      args:
        executable: /bin/bash
      register: tout
      when: not file.stat.exists

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines


    - name: 'Show sym-links in home directory'
      become:       true
      become_user:  "{{ APE_var_1 }}"

      shell: |
        echo "$ whoami"
        whoami
        echo "$ pwd"
        pwd
        echo "$ cd ~"
        cd ~
        echo "$ pwd"
        pwd
        echo
        echo "$ ls -lahF | egrep ' ->'"
        ls -lahF | egrep ' ->' | perl -ple '
            $s=" "x10;
            if (/(\s\S+)(\s+)(->)/) {
                my $a = $1;
                my $b = $2;
                my $c = $3;
                my $len = length ($a);
                my $pad = " " x (15 - $len);
                s/${a}${b}${c}/${a}${pad}${b}${c}/;
            }
        '
        echo
        echo "$ ls -lhF ./zGit/"
        ls -lhF ./zGit/
      register: tout

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines





