## - source this file in a Bash shell -- ##
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

# v_ansible='7.0.0'

echo "$ f_apek ~/.ssh-jdb/id_ed25519"
f_apek ~/.ssh-jdb/id_ed25519

rc=$?
if [[ $rc -ne 0 ]]; then
    echo "- Error, rc: $rc"
    return $rc
fi

echo
echo "$ env | grep APE_KEY"
env | grep APE_KEY

