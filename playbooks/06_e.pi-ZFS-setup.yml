# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.
# v_ansible='7.1.0'

# - Deploy ZFS to the USB ZFS disk(s) for "target" (eg: "cherry-pi"),
#   after booting from the "source" USB disk (eg: "cow-pi").
#
# - By default, the ZFS disks are also the Linux boot disks.

# - A split arrangement also works, with distinct & dedicated ZFS disks,
#   when  zfs_t1_by_id,  zfs_t1_by_id  are defined in advance.
#
# - In this "distinct ZFS disk" scenario,  my_t1  and  my_t2  start out as the
#   normal Linux device specs (eg: /dev/sdb, /dev/sdc) for formatting,
#   and  zfs_t1_by_id, zfs_t2_by_id  are used to defined the ZFS zpool.
#
# - Raspberry PI is somewhat arbitrary at defined  /dev/sdX  devices at boot,
#   so it's not necessary that  zfs_t1_by_id  corresponds to  my_t1 (etc)
#   as long as the aggregate disk lists match each other.


---

- hosts: localhost
  gather_facts: yes

  tasks:

    - name: "01. Timestamp"
      debug:
        msg: "{{ ansible_date_time.date }} {{ ansible_date_time.time }} {{ ansible_date_time.tz }}"


- hosts: "{{ lookup ('env', 'APE_HOSTS') }}"
  gather_facts: no

  vars:
    APE_v:      "{{ lookup ('env', 'APE_verbose') or 0 }}"                  # - verbose mode
    LOOP_v_tag: "{{ (APE_v | int > 0) | ternary ('always', 'never') }}"     # - verbose-only LOOP
    steps:      "{{ lookup ('env', 'APE_STEPS') | default ([], true) }}"    # - list of sections (optional)

    xx_target:  "{{ my_target_server            | default (ansible_host, true) }}"

    my_src: "{{ hostvars[ xx_target ].my_pi_src | default ('', false) }}"   # - Boot disk, copied from
    my_t1:  "{{ hostvars[ xx_target ].my_pi_t1  | default ('', false) }}"   # - Target disk 1, eg: USB white
    my_t2:  "{{ hostvars[ xx_target ].my_pi_t2  | default ('', false) }}"   # - Target disk 2, eg: USB yellow


    # - Use the unique ID, with vendor serial number and partition.
    #   You can define this explicitly, with "behind the scenes" awareness,
    #   or let the playbook find it for you.
    #   However, some disks produce multiple  /dev/disk/by-id/  entries, and
    #   some entries are "more unique" than others (eg, with vendor serial number).

    zfs_size:   "{{ hostvars[ xx_target ].my_zfs_size   | default ('90GB',      true) }}"   # - Size of Linux ZFS
    zfs_mount:  "{{ hostvars[ xx_target ].my_zfs_mount  | default ('/zfs',      true) }}"   # - Mount it here
    zfs_pool:   "{{ hostvars[ xx_target ].my_zfs_pool   | default ('zpool',     true) }}"   # - Zpool name
    zfs_ashift: "{{ hostvars[ xx_target ].my_zfs_ashift | default ('ashift=12', true) }}"   # - SSD: ashift=12, or ashift=13
    zfs_raid:   "{{ hostvars[ xx_target ].my_zfs_raid   | default ('',          true) }}"   # - RAID type, "" == "RAID-0"
    #
    zfs_t1_by_id:   "{{ hostvars[ xx_target ].my_zfs_t1_by_id  | default ('', true) }}"
    zfs_t2_by_id:   "{{ hostvars[ xx_target ].my_zfs_t2_by_id  | default ('', true) }}"

    do_20:          "{{ ((zfs_t1_by_id == '') or (zfs_t2_by_id == '')) | ternary ('always', 'never') }}"
    do_zfs:         "{{ my_do_zfs_import | default ('', true) }}"

    zpool_guid: "-no-zpool_guid-"
    dd_out:     []


  tasks:

    # - This playbook is only applicable when ZFS is desired.
    #   -> Quit now, if  do_zfs  is not defined, or set to "no".
    #
    - name: "03. meta end_host?"
      meta:
        end_host
      when:
        (('03' in steps) or (steps | length == 0))  and
        (do_zfs is not defined) or (do_zfs == '') or (do_zfs == 'no')


    - name: "05. Show target-server vars"
      debug:
        msg:
            - "xx_target        : '{{ xx_target }}'"
            - "my_src           : '{{ my_src }}'"
            - "my_t1            : '{{ my_t1 }}'"
            - "my_t2            : '{{ my_t2 }}'"
            - ""
            - "do_20            : '{{ do_20 }}'"
            - "virtual do_20    : '{{ (zfs_t1_by_id == '') | ternary ('always', 'never') }}'"
            - ""
            - "zfs_size         : '{{ zfs_size }}'"
            - "zfs_mount        : '{{ zfs_mount }}'"
            - "zfs_pool         : '{{ zfs_pool }}'"
            - "zfs_ashift       : '{{ zfs_ashift }}'"
            - "zfs_raid         : '{{ zfs_raid }}'"
            - ""
            - "zfs_t1_by_id     : '{{ zfs_t1_by_id }}'"
            - "zfs_t2_by_id     : '{{ zfs_t2_by_id }}'"
      when:
        (APE_v | int >= 0)      # - Unconditional.



    - name: "10. Get info: sfdisk -l"
      become: true
      ignore_errors: true
      shell:  |
        src="{{ my_src }}"
        t1="{{ my_t1 }}"
        t2="{{ my_t2 }}"
        #
        echo "$ sudo parted -l -m"
        out=$(sudo parted -l -m  2>&1)
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "${out}"
            echo "- Error, rc: $rc"
            exit 1
        fi
        echo "${out}" | egrep -v "^BYT"
        #
        for disk in ${src}  ${t1}  ${t2}; do
            if [[ -z "${disk}" ]]; then
                continue
            fi
            bn="$(basename ${disk})"        # - Eg: sdb2
            #
            echo
            echo "$ sudo sfdisk -l ${disk}"
            out=$(sfdisk -l ${disk} 2>&1)
            rc=$?
            if [[ $rc -ne 0 ]]; then
                echo "${out}"
                echo "- Error, rc: $rc"
                exit 1
            fi
            echo "${out}" | egrep "^Disk /|^Device|^/|^$"
        done
      args:
        executable: /bin/bash
      register: tout
      when:
        ('10' in steps) or (steps | length == 0)


    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('10' in steps) or (steps | length == 0))  and
        ( (APE_v | int >= 0)  or
          (('rc' in tout) and (tout.rc != 0)) )


    - name: "10. Sanity test: Get info fdisk??"
      fail:
        msg: "fdisk failed."
      when:
        (('10' in steps) or (steps | length == 0))  and
        (('rc' in tout) and (tout.rc != 0))


    # - If zpool already exists, remove it first.
    #
    - name: "14. Remove existing zpool?"
      become: true
      ignore_errors: true
      shell:  |
        mount="{{ zfs_mount }}"
        pool="{{ zfs_pool }}"
        ashift="{{ zfs_ashift }}"
        raid="{{ zfs_raid }}"
        fail=0
        #
        echo "$ zpool list -L | egrep -v ^NAME | egrep ${pool}"
        out=$(zpool list -L 2>&1)
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "${out}"
            echo "- Error, rc: $rc"
            exit $rc
        fi
        echo "${out}" | egrep -v ^NAME | egrep ${pool}
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "-> Pool not found: '${pool}'. Removal skipped."
            exit 0
        fi
        echo "$ zpool destroy ${pool}"
                zpool destroy ${pool}  2>&1
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "- Error, rc: $rc"
            exit $rc
        fi
        true
      args:
        executable: /bin/bash
      register: tout
      when:
        ('14' in steps) or (steps | length == 0)

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('14' in steps) or (steps | length == 0))  and
        ( (APE_v | int >= 0)  or
          (('rc' in tout) and (tout.rc != 0)) )

    - name: "14. Remove zpool failed?"
      fail:
        msg: "Failed."
      when:
        (('14' in steps) or (steps | length == 0))  and
        (('rc' in tout) and (tout.rc != 0))


    # - Create "Non-FS data" (type=da) disk partitions for ZFS.
    #   Strictly speaking, this is the wrong partition type,
    #     but until the partition is created,
    #     the requested paths  zfs_t1_by_id & zfs_t2_by_id  don't exist yet.
    #   ZFS activation will change the partition types to
    #     appropriate values.
    #
    # - FYI:
    #   https://wiki.archlinux.org/title/Mdadm#Prepare_the_devices
    #   https://raid.wiki.kernel.org/index.php/Partition_Types
    #
    # - The default size 90 Gb should work for commodity USB 128 Gb disks,
    #   allowing for a boot partition and 30 Gb Linux ext4 partition.
    #
    - name: "15. Create Linux ZFS partition on each USB target"
      become: true
      ignore_errors: true
      shell:  |
        t1="{{ my_t1 }}"
        t2="{{ my_t2 }}"
        size="{{ zfs_size }}"
        fail=0
        #
        for partition in  ${t1}  ${t2}; do
            if [[ -z "${partition}" ]]; then
                continue
            fi
            bn="$(basename ${partition})"        # - Eg: sdb2
            #
            uuidgen=$(uuidgen 2>&1)
            rc=$?
            if [[ $rc -ne 0 ]]; then
                echo "$ uuidgen"
                echo "${out}"
                echo "- Error, rc: $rc"
                fail=1
                continue        # - Tolerate errors, and continue the loop.
            fi
            cmd="size=${size},type=da,uuid=${uuidgen}\n"    # - 90 Gb partition, "Linux non-FS data"
            #
            echo
            echo "$ printf '${cmd}' | sudo sfdisk -N3 ${partition}"
            out=$(printf "${cmd}" | sfdisk -N3 ${partition}  2>&1)
            rc=$?
            echo "${out}"
            if [[ $rc -ne 0 ]]; then
                echo "- Error, rc: $rc"
                fail=1
                ## exit 1
                continue        # - Tolerate errors, and continue the loop.
            else
                echo "- rc: $rc"
            fi
        done
        #
        echo
        echo "$ sudo parted -l -m"
        out=$(sudo parted -l -m  2>&1)
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "${out}"
            echo "- Error, rc: $rc"
            exit 1
        fi
        echo "${out}" | egrep -v "^BYT"
        #
        for disk in ${src}  ${t1}  ${t2}; do
            if [[ -z "${partition}" ]]; then
                continue
            fi
            bn="$(basename ${partition})"        # - Eg: sdb2
            #
            echo
            echo "$ sudo sfdisk -l ${disk}"
            out=$(sfdisk -l ${disk} 2>&1)
            rc=$?
            if [[ $rc -ne 0 ]]; then
                echo "${out}"
                echo "- Error, rc: $rc"
                exit 1
            fi
            echo "${out}" | egrep "^Disk /|^Device|^/|^$"
        done
        #
        if [[ $fail -ne 0 ]]; then
            exit $fail
        fi
        true
      args:
        executable: /bin/bash
      register: tout
      when:
        ('15' in steps) or (steps | length == 0)

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('15' in steps) or (steps | length == 0))  and
        ( (APE_v | int >= 0)  or
          (('rc' in tout) and (tout.rc != 0)) )


    # - Get the  /dev/dsk/by-id/...  associated with each target disk partition.
    #   Setting Ansible variables is unfortunately too complicated;
    #   with no errors, the output is in the same order as the input in stdout_lines.
    #
    # - Output from this Shell snippet is written to STDERR.
    #
    # - Method:
    #   cow_$ cd /dev/dsk/by-id/
    #   cow_$ find . -exec ls -lhdF {} \; | grep sdb3
    #   lrwxrwxrwx 1 root root 10 Jun 28 14:48 ./usb-Samsung_Flash_Drive_FIT_0317521110000442-0:0-part3 -> ../../sdb3

    # - ToDo: Somehow these  set_fact  statements fail to take effect inside the "tags" below,
    #     although debug diagnostics print expected values.
    #   A workaround is to set the value above in the vars section.
    #   -> But doesn't  set_fact  do the same thing, dynamically?
    #   -> Or maybe the "tags" stuff is pre-processed statically ??  A bizarre time-waster.
    #
    ##  - name: "20_a. prep 1"
    ##    set_fact:
    ##      do_20: "never"
    ##    when:
    ##      (('20' in steps) or (steps | length == 0))
    ##
    ##  - name: "20_b. prep 2"
    ##    set_fact:
    ##      do_20: "always"
    ##      when:
    ##        (('20' in steps) or (steps | length == 0))  and
    ##        ((zfs_t1_by_id == '') or (zfs_t2_by_id == ''))

    - name: "20_a. do_20 ?"
      debug:
        msg:
            - "xx_target:       '{{ xx_target }}'"
            - "do_20:           '{{ do_20 }}'"
            - "virtual do_20:   '{{ (zfs_t1_by_id == '') | ternary ('always', 'never') }}'"
            - "zfs_t1_by_id:    '{{ zfs_t1_by_id }}'"
            - "zfs_t2_by_id:    '{{ zfs_t2_by_id }}'"
      when:
        (('20' in steps) or (steps | length == 0))  and
        (APE_v | int >= 0)


    - name: "20_c. Get disk-by-id"
      ignore_errors: true
      shell: |
        if [[ -z "{{ item }}" ]]; then
            exit 0
        fi
        #
        disk_full="{{ item }}"                      # - Eg: /dev/sdb
        slice='3'
        disk="$(basename ${disk_full})${slice}"     # - Eg: sdb3
        #
        echo                            >&2
        echo "$ echo \$disk: ${disk}"   >&2
        echo "$ cd /dev/disk/by-id/"    >&2
        cd /dev/disk/by-id/             1>&2
        rc=$?
        if [[ $rc != 0 ]]; then
            echo "- Error, rc: $rc"     >&2
            exit $rc
        fi
        #
        echo "$ for xx in *; do echo \$xx : \$(readlink \$xx); done | grep ${disk}" >&2
        out=$(for xx in *; do echo $xx : $(readlink $xx); done  2>&1)
        rc=$?
        if [[ $rc != 0 ]]; then
            echo "${out}"               >&2
            echo "- Error, rc: $rc"     >&2
            exit $rc
        fi
        #
        lines=$(echo "${out}" | grep "${disk}" 2>&1)
        rc=$?
        echo "${lines}"                 >&2
        if [[ $rc != 0 ]]; then
            echo "- Error, rc: $rc"     >&2
            exit $rc
        fi

        # - Usually a single line:
        #   usb-Samsung_Flash_Drive_FIT_0362021110000459-0:0-part3 : ../../sdc3
        #
        # - Caution: can be multiple!
        #   scsi-35000000000000001-part1 : ../../sdd1
        #   scsi-SSamsung_PSSD_T7_E12231CR0SNWT6S-part1 : ../../sdd1
        #   wwn-0x5000000000000001-part1 : ../../sdd1
        #
        id=$(echo "${out}" | grep "${disk}" | head -1 | cut -f1 -d' ')
        echo "${id}"
        true
      args:
        executable: /bin/bash
      register: toutL
      loop:
        - "{{ my_t1 }}"     # - Eg: /dev/sdb
        - "{{ my_t2 }}"     # - Eg: /dev/sdc
      when:
        (('20' in steps) or (steps | length == 0)) and
        ((zfs_t1_by_id == ''))                              # - virtual do_20
      tags: [ "{{ do_20 }}" ]                               # - real do_20


    - name: "20_c2. Diagnostic"
      debug:
        var: toutL
      when:
        (('20' in steps) or (steps | length == 0))  and
        ((zfs_t1_by_id == ''))                      and
        (APE_v | int >= 1)
      tags: [ "{{ do_20 }}" ]                               # - real do_20


    - name: "20_d. Clean the output 1"
      set_fact:
        dd_out: []
      when:
        (('20' in steps) or (steps | length == 0))  and
        ((zfs_t1_by_id == ''))                              # - virtual do_20
      tags: [ "{{ do_20 }}" ]                               # - real do_20


    - name: "20_e. Clean the output 2"
      set_fact:
        dd_out: "{{ dd_out + item.stderr_lines }}"
      loop:
        "{{ toutL.results }}"
      when:
        (('20' in steps) or (steps | length == 0))  and
        ((zfs_t1_by_id == ''))                              # - virtual do_20
      tags: [ "{{ do_20 }}" ]                               # - real do_20

    - name: "20_f: diagnostic"
      debug:
        var:
          dd_out
      when:
        (('20' in steps) or (steps | length == 0))  and
        ( (APE_v | int >= 0)  or
          (('rc'     in toutL) and (toutL.rc     != 0)) or
          (('failed' in toutL) and (toutL.failed == true)) ) and
        ((zfs_t1_by_id == ''))                              # - virtual do_20
      tags: [ "{{ do_20 }}" ]                               # - real do_20


    - name: "20_g. Get failed?"
      fail:
        msg: "Get failed."
      when:
        (('20' in steps) or (steps | length == 0))  and
        (
          (('rc'     in toutL) and (toutL.rc     != 0)) or
          (('failed' in toutL) and (toutL.failed == true)) ) and
        ((zfs_t1_by_id == ''))                              # - virtual do_20
      tags: [ "{{ do_20 }}" ]                               # - real do_20


    - name: "20_h. set zfs_t1_by_id"
      set_fact:
        zfs_t1_by_id: "{{ toutL.results[0].stdout_lines[0] }}"
      when:
        (('20' in steps) or (steps | length == 0))  and
        ((zfs_t1_by_id == ''))                              # - virtual do_20
      tags: [ "{{ do_20 }}" ]                               # - real do_20


    - name: "20_i. set zfs_t2_by_id"
      set_fact:
        zfs_t2_by_id: "{{ toutL.results[1].stdout_lines[0] }}"
      when:
        (('20' in steps) or (steps | length == 0))  and
        ((zfs_t1_by_id == ''))                              # - virtual do_20
      tags: [ "{{ do_20 }}" ]                               # - real do_20


    - name: "20_i. Diagnostic"
      debug:
        msg:
            - "xx_target:       '{{ xx_target }}'"
            - "do_20:           '{{ do_20 }}'"
            - "virtual do_20:   '- no longer accurate -'"
            - "zfs_t1_by_id:    '{{ zfs_t1_by_id }}'"
            - "zfs_t2_by_id:    '{{ zfs_t2_by_id }}'"
      when:
        (('20' in steps) or (steps | length == 0))  and
        (APE_v | int >= 0)



    # - Create the zpool, with  /dev/dsk/by-id/
    # - https://openzfs.github.io/openzfs-docs/Project%20and%20Community/FAQ.html#selecting-dev-names-when-creating-a-pool-linux
    #   https://wiki.archlinux.org/title/ZFS#Creating_ZFS_pools
    #   https://arstechnica.com/information-technology/2020/05/zfs-101-understanding-zfs-storage-and-performance/
    #   https://pve.proxmox.com/wiki/ZFS_on_Linux
    #
    - name: "31. Create zpool"
      become: true
      ignore_errors: true
      shell:  |
        zt1="{{ zfs_t1_by_id }}"
        zt2="{{ zfs_t2_by_id }}"
        #
        disks=""
        if [[ -n "${zt1}" ]]; then
            disks="${disks} ${zt1}"
        fi
        if [[ -n "${zt2}" ]]; then
            disks="${disks} ${zt2}"
        fi
        #
        mount="{{ zfs_mount }}"
        pool="{{ zfs_pool }}"
        ashift="{{ zfs_ashift }}"
        raid="{{ zfs_raid }}"
        fail=0
        #
        for cmd in \
            "zpool version"                 \
            "zpool create -f -o ${ashift} -m ${mount} ${pool} ${raid} ${disks}"  \
            "zfs set atime=off ${pool}"     \
            "zpool list"                    \
            "zpool status -v"               \
            "df -h ${mount}"                \
        ; do
            echo
            echo "$ sudo ${cmd}"
            out=$(${cmd} 2>&1)
            rc=$?
            echo "${out}" | expand
            if [[ $rc -ne 0 ]]; then
                echo "- Error, rc: $rc"
                exit 1
            else
                true
                ## echo "- rc: $rc"
            fi
        done
        true
      args:
        executable: /bin/bash
      register: tout
      when:
        ('31' in steps) or (steps | length == 0)

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('31' in steps) or (steps | length == 0))  and
        ( (APE_v | int >= 0)  or
          (('rc' in tout) and (tout.rc != 0)) )

    - name: "31. Create zpool failed?"
      fail:
        msg: "Failed."
      when:
        (('31' in steps) or (steps | length == 0))  and
        (('rc' in tout) and (tout.rc != 0))



    - name: "32. Create /zfs/ sub-directories"
      become: true
      ignore_errors: true
      shell:  |
        mount="{{ zfs_mount }}"
        pool="{{ zfs_pool }}"
        fail=0
        #
        for fs in \
            "/jdb/"                         \
            "/usr/local2/"                  \
            "/var/log2/"                    \
            "/zTags/"                       \
        ; do
            echo "$ sudo mkdir -p ${mount}/${fs}/"
            out=$(mkdir -p ${mount}/${fs}  2>&1)
            rc=$?
            echo "${out}"
            if [[ $rc -ne 0 ]]; then
                echo "- Error, rc: $rc"
                fail=1
                continue
            else
                true
                ## echo "- rc: $rc"
            fi
        done
        #
        echo
        echo "$ sudo cp -p /zTags/*  ${mount}/"
        cp -p /zTags/*  ${mount}/zTags/  2>&1
        rc=$?
        echo "${out}"
        if [[ $rc -ne 0 ]]; then
            echo "- Error, rc: $rc"
            fail=1
        fi
        #
        echo
        echo "$ sudo tree -F ${mount}"
        tree -F ${mount}  2>&1
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "- Error, rc: $rc"
            fail=1
        fi
        #
        if [[ $fail -ne 0 ]]; then
            exit $fail
        fi
        true
      args:
        executable: /bin/bash
      register: tout
      when:
        ('32' in steps) or (steps | length == 0)

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('32' in steps) or (steps | length == 0))  and
        ( (APE_v | int >= 0)  or
          (('rc' in tout) and (tout.rc != 0)) )

    - name: "32. Create zpool failed?"
      fail:
        msg: "Failed."
      when:
        (('32' in steps) or (steps | length == 0))  and
        (('rc' in tout) and (tout.rc != 0))



    - name: "33. Find zpool numeric id -> zpool_guid"
      ignore_errors: true
      shell:  |
        mount="{{ zfs_mount }}"
        zpool="{{ zfs_pool }}"
        fail=0
        #
        echo "$ zpool get -H -o value guid ${zpool}"        >&2     # - stderr
        out=$(zpool get -H -o value guid "${zpool}"  2>&1)
        rc=$?
        echo "${out}"                                       >&2
        if [[ $rc -ne 0 ]]; then
            echo "- Error, rc: $rc"                         >&2
            exit $rc
        fi
        echo "${out}"
        true
      args:
        executable: /bin/bash
      register: tout
      when:
        ('33' in steps) or (steps | length == 0)


    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('33' in steps) or (steps | length == 0))  and
        ( (APE_v | int >= 0)  or
          (('rc' in tout) and (tout.rc != 0)) )


    - name: "33. Failed?"
      fail:
        msg: "Failed."
      when:
        (('33' in steps) or (steps | length == 0))  and
        (('rc' in tout) and (tout.rc != 0))


    - name: "33. Set zpool_guid"
      set_fact:
        zpool_guid: "{{ tout.stdout_lines[0] }}"
      when:
        (('33' in steps) or (steps | length == 0))



    # - Stash the ID in the target disks' Linux ext4 file  /zTags/zpool-guid
    #   so it can readily imported when the target server reboots as itself.
    #
    # - Directories:
    #   ##-no: /zTags/zpool-guid
    #   zfs_t1_by_id : /zTags/zpool-guid
    #   zfs_t1_by_id : /boot/firmware/zTags/zpool-guid
    #   zfs_t2_by_id : /zTags/zpool-guid
    #   zfs_t2_by_id : /boot/firmware/zTags/zpool-guid
    #
    - name: "34. Stash numeric ID"
      become: true
      ignore_errors: true

      shell:  |
        t1="{{ my_t1 }}"
        t2="{{ my_t2 }}"
        mount="{{ zfs_mount }}"
        guid="{{ zpool_guid }}"
        fail=0      # - False
        #
        partitions=""
        if [[ -n "${t1}" ]]; then
            partitions="${partitions} ${t1}1 ${t1}2"
        fi
        if [[ -n "${t2}" ]]; then
            partitions="${partitions} ${t2}1 ${t2}2"
        fi
        #
        for partition in ${partitions}; do
            echo
            echo "$ sudo mount ${partition} /mnt"
            out=$(mount ${partition} /mnt  2>&1)
            rc=$?
            echo "${out}"
            if [[ $rc -ne 0 ]]; then
                echo "- Error, rc: $rc"
                exit $rc
            fi
            #
            echo "$ sudo echo '${guid}'  > /mnt/zTags/zpool-guid"   # - sudo not accurate
            echo "${guid}"  > /mnt/zTags/zpool-guid
            rc=$?
            if [[ $rc -ne 0 ]]; then
                echo "- Error, rc: $rc"
                exit $rc
            fi
            #
            echo "$ sudo umount /mnt"
            out=$(umount /mnt  2>&1)
            rc=$?
            echo "${out}"
            if [[ $rc -ne 0 ]]; then
                echo "- Error, rc: $rc"
                exit $rc
            fi
        done
        #
        if [[ $fail -ne 0 ]]; then
            exit $fail
        fi
        true
      args:
        executable: /bin/bash
      register: tout
      when:
        ('34' in steps) or (steps | length == 0)

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('34' in steps) or (steps | length == 0))  and
        ( (APE_v | int >= 0)  or
          (('rc' in tout) and (tout.rc != 0)) )

    - name: "34. Create zpool failed?"
      fail:
        msg: "Failed."
      when:
        (('34' in steps) or (steps | length == 0))  and
        (('rc' in tout) and (tout.rc != 0))



    - name: "35. Take zpool offline"
      become: true
      ignore_errors: true
      shell:  |
        mount="{{ zfs_mount }}"
        pool="{{ zfs_pool }}"
        fail=0
        #
        for cmd in \
            "zpool get -H -o value guid ${zpool}"   \
            "df -h ${mount}"                        \
            "zfs unmount ${mount}"                  \
            "zpool list"                            \
            "zpool export ${pool}"                  \
            "zpool list"                            \
            "df -h ${mount}"                        \
            "zpool import"                          \
        ; do
            echo
            echo "$ sudo ${cmd}"
            out=$(${cmd} 2>&1)
            rc=$?
            echo "${out}" | expand
            if [[ $rc -ne 0 ]]; then
                echo "- Error, rc: $rc"
                exit 1
            else
                true
                ## echo "- rc: $rc"
            fi
        done
        true
      args:
        executable: /bin/bash
      register: tout
      when:
        ('35' in steps) or (steps | length == 0)

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('35' in steps) or (steps | length == 0))  and
        ( (APE_v | int >= 0)  or
          (('rc' in tout) and (tout.rc != 0)) )

    - name: "35. Failed?"
      fail:
        msg: "Failed."
      when:
        (('35' in steps) or (steps | length == 0))  and
        (('rc' in tout) and (tout.rc != 0))



    - name: "40. Get info: lsblk -f"
      become: true
      ignore_errors: true
      shell:  |
        echo "$ sudo lsblk -f | grep -v ^loop"
        out=$(lsblk -f  2>&1)
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "${out}"
            echo "- Error, rc: $rc"
            exit 1
        fi
        echo "${out}" | egrep -v "^loop"
        true
      args:
        executable: /bin/bash
      register: tout
      when:
        ('40' in steps) or (steps | length == 0)

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('40' in steps) or (steps | length == 0))  and
        ( (APE_v | int >= 0)  or
          (('rc' in tout) and (tout.rc != 0)) )

    - name: "40. Sanity test: Get info fdisk??"
      fail:
        msg: "Fdisk failed."
      when:
        (('40' in steps) or (steps | length == 0))  and
        (('rc' in tout) and (tout.rc != 0))


