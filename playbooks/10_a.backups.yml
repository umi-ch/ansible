# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.
# v_ansible='7.0.1'

# - Ad-hoc backup script.
#   -> A tar backup is made on the remote server, then fetched back to the Ansible server.
#   -> If everything succeeds, the remote backup file is removed.

# - FYI:
#   https://www.thegeekdiary.com/what-is-the-purpose-of-utmp-wtmp-and-btmp-files-in-linux/
#   d$ sudo utmpdump /var/log/btmp | tail

# - FYI:
#   #!/usr/bin/env bash
#   set -ex
#   tar cpf - ./files | aws s3 cp - s3://my-bucket/files.tar
#   echo $?

# - FYi:
#   This statement in a shell gives Ansible errors:
#       warn="--warning=no-file-changed"
#   ->
#   ok: [cow-pi] => {
#       "tout": {
#       "changed": false,
#       "failed": true,
#       msg": "argument 'warn' is of type <class 'str'> and we were unable to convert to bool: The value '-warning=no-file-changed' is not a valid boolean.  Valid booleans include: 0, 1, '0', 'yes', 'off', 'f', 't', '1', 'y', 'true', 'on', 'no', 'false', 'n'


---

- hosts: localhost
  gather_facts: yes

  vars:
    APE_v:      "{{ lookup ('env', 'APE_verbose') or 0 }}"                  # - verbose mode
    LOOP_v_tag: "{{ (APE_v | int > 0) | ternary ('always', 'never') }}"     # - verbose-only LOOP
    steps:      "{{ lookup ('env', 'APE_STEPS') | default ([], true) }}"    # - list of sections (optional)

    local_bdir: '~/Backups/zUMI/'           # - On the Ansible server

  tasks:

    - name: '01. Timestamp'
      debug:
        msg: "{{ ansible_date_time.date }} {{ ansible_date_time.time }} {{ ansible_date_time.tz }}"


    - name: '02. Ensure local backup directory exists: {{ local_bdir }}'
      ansible.builtin.file:
        path:       "{{ local_bdir }}"
        state:      directory
      register: toutb
      when:
        (('02' in steps) or (steps | length == 0))

    - debug:
        var: toutb
      when:
        (('02' in steps) or (steps | length == 0))  and
        (APE_v | int >= 2)


- hosts: "{{ lookup ('env', 'APE_HOSTS') }}"
  gather_facts: no

  vars:
    APE_v:      "{{ lookup ('env', 'APE_verbose') or 0 }}"                  # - verbose mode
    LOOP_v_tag: "{{ (APE_v | int > 0) | ternary ('always', 'never') }}"     # - verbose-only LOOP

    steps:      "{{ lookup ('env', 'APE_STEPS')      | default ([], true) }}"   # - list of sections (optional)

    dirs_list:  []
    dirs_str:   ""      ## "{{ dirs_list | join(' ') }}"

    ex_list:    []
    ex_str:     ""

    bdir:       '/var/backups/zUMI/'        # - Backups stored here on the remote server.
    bfile:      'backup'                    # - Construct a backup file name.
    local_bdir: '~/Backups/zUMI/'           # - On the Ansible server, to where backups are fetched.

  tasks:

    - name: "09_a. Set dirs_list base"
      set_fact:
        dirs_list:                  # - Relative to root directory:  /
            - ./usr/local/etc/
            - ./etc/
            - ./var/lib/             # /var/lib/fail2ban/
            - ./var/log/
            - ./var/www/

    - name: "09_b. Set dirs_list extras 1"
      set_fact:
        my_backup_dirs: []
      when:
        (my_backup_dirs is not defined)

    - name: "09_c. Set dirs_list extras 2"
      set_fact:
        dirs_list:  "{{ dirs_list }} + {{ item }}"
      loop:
         - "{{ my_backup_dirs }}"

    - name: "09_d. Set ex_list base"      # - Exclude glob patterns, with GNU tar
      set_fact:
        ex_list:
            - .cache
            - .cpan
            #
            - ./var/lib/amazon/*
            - ./var/lib/apt/*
            - ./var/lib/aptitude/*
            - ./var/lib/docker/*
            - ./var/lib/dpkg/*
            - ./var/lib/elasticsearch/*
            - ./var/lib/snapd/*
            #
            - ./var/log/amazon/*
            - ./var/log/atop/*
            - ./var/log/cloud-init.log
            - ./var/log/dist-upgrade/*
            - ./var/log/docker/*
            - ./var/log/dpkg.log*
            - ./var/log/elasticsearch/*
            - ./var/log/journal/*

    - name: "09_e. Set excludes ex_list extras 1"
      set_fact:
        my_backup_excludes: []
      when:
        (my_backup_excludes is not defined)


    - name: "09_e. Set excludes ex_list extras 2"
      set_fact:
        ex_list:  "{{ ex_list }} + {{ item }}"
      loop:
         - "{{ my_backup_excludes }}"

    - debug:
        var: ex_list
      when:
        (('09_e' in steps) or (steps | length == 0))  and
        (APE_v | int >= 1)


    - name: "09_f. Set ex_str"
      set_fact:
        ex_str: "{{ ex_list | join(' --exclude=') }}"


    - name: '10. Timestamp 1'
      shell: |
        echo "$ date; whoami; hostname -f"
        date; whoami; hostname -f
      args:
        executable: /bin/bash
      register: tout
      when:
        (('10' in steps) or (steps | length == 0))

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('10' in steps) or (steps | length == 0))  and
        (APE_v | int >= 1)


    - name: '11. Set backup dir'
      become: true
      ansible.builtin.file:
        path:       "{{ bdir }}"
        state:      directory
      register: toutb
      when:
        (('11' in steps) or (steps | length == 0))

    - debug:
        var: toutb
      when:
        (('11' in steps) or (steps | length == 0))  and
        (APE_v | int >= 1)


    - name: '12. Set backup tar file'
      shell: |
        isodate=$(date '+%Y-%m-%d')
        plus=''
        bfile="backup.$(hostname).${isodate}${plus}.tgz"
        while [[ -f "{{ bdir }}/${bfile}" ]]; do
            plus="${plus}+"         # - Append to get a unique file name.
            bfile="backup.$(hostname).${isodate}${plus}.tgz"
        done
        echo ${bfile}
      args:
        executable: /bin/bash
      register: tout
      when:
        (('12' in steps) or (steps | length == 0))


    - name: '12. Set fact: bfile'
      set_fact:
        bfile: "{{ tout.stdout }}"
      when:
        (('12' in steps) or (steps | length == 0))

    - debug:
        msg: "bfile = '{{ bdir }}/{{ bfile }}'"
      when:
        (('12' in steps) or (steps | length == 0))  and
        (APE_v | int >= 0)


    - name: '13. Confirm backup directories: 1'
      ignore_errors: true
      stat:
        path: "/{{ item }}"         # - Prepend root directory:  /
      register: tout_stats
      loop:     ## with_items:
        "{{ dirs_list }}"
      when:
        (('13' in steps) or (steps | length == 0))

    - debug:
        var: tout_stats
      when:
        (('13' in steps) or (steps | length == 0))  and
        (APE_v | int >= 2)


    - name: '13. Confirm backup directories: 2'
      debug:
        msg: "- Skipping non-existant location: '{{ item.item }}'"
      loop:     ## with_items:
        "{{ tout_stats.results }}"
      when:
        (('13' in steps) or (steps | length == 0))  and
        (item.stat.exists == false)


    - name: '13. Confirm backup directories: 3'
      set_fact:
        dirs_str: "{{ dirs_str }} {{ item.item }}"
      loop:     ## with_items:
        "{{ tout_stats.results }}"
      when:
        (('13' in steps) or (steps | length == 0))  and
        (item.stat.exists == true)
      ## no_log: true
      loop_control:
        label:  "{{ item.item }}"

    - debug:
        var: dirs_str
      when:
        (('13' in steps) or (steps | length == 0))  and
        (APE_v | int >= 0)

    - debug:
        var: ex_str
      when:
        (('13' in steps) or (steps | length == 0))  and
        (APE_v | int >= 0)


    - name: '18. Backup Sqlite Fail2Ban database'
      become: true
      shell:  |
        fail2ban_backup="/var/log/fail2ban.db.bak"
        #
        echo "$ cd /var/lib/fail2ban/"
        cd /var/lib/fail2ban/  2>&1
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "- Error, rc: $rc"
            exit 0
        fi
        #
        cmd_1=".timeout 10000"      # - milliseconds
        cmd_2=".backup ${fail2ban_backup}"
        echo "$ sqlite3  fail2ban.sqlite3  '${cmd_1}' '${cmd_2}'  2>&1"
        sqlite3  fail2ban.sqlite3  "${cmd_1}" "${cmd_2}"  2>&1
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "- Error, rc: $rc"
            exit 0
        fi
        #
        echo "$ chmod 600 ${fail2ban_backup}"
        chmod 600 ${fail2ban_backup}
        echo "$ ls -lhF ${fail2ban_backup}"
        ls -lhF "${fail2ban_backup}"  2>&1
        true
      args:
        executable: /bin/bash
      register: tout
      when:
        (('18' in steps) or (steps | length == 0))

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('18' in steps) or (steps | length == 0))  and
        (APE_v | int >= 0)


    - name: '19. Extract Fail2Ban IP addresses from Sqlite'
      become: true
      shell:  |
        fail2ban_ip="/var/log/fail2ban.db.unx"
        #
        echo "$ cd /var/lib/fail2ban/"
        cd /var/lib/fail2ban/  2>&1
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "- Error, rc: $rc"
            exit 0
        fi
        #
        sql='select ip,jail,timeofban,bancount from bips'
        echo "$ sqlite3  fail2ban.sqlite3  '${sql}'  > ${fail2ban_ip}  2>&1"
        sqlite3  fail2ban.sqlite3  "${sql}"  > ${fail2ban_ip}  2>&1
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "- Error, rc: $rc"
            exit 0
        fi
        #
        echo
        echo "$ chmod 600 ${fail2ban_ip}"
        chmod 600 ${fail2ban_ip}
        echo "$ ls -lhF /var/log/fail2ban*"
        ls -lhF /var/log/fail2ban*  2>&1
        #
        echo
        echo "$ wc -l ${fail2ban_ip}"
        wc -l "${fail2ban_ip}"  2>&1
        #
        echo
        echo "$ head ${fail2ban_ip}  2>&1"
        head "${fail2ban_ip}"  2>&1
        true
      args:
        executable: /bin/bash
      register: tout
      when:
        (('19' in steps) or (steps | length == 0))

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('19' in steps) or (steps | length == 0))  and
        (APE_v | int >= 0)


    - name: "20. Create temp file for listing"
      become: true
      ansible.builtin.tempfile:
        state:  "file"
        suffix: ".temp"
      register:
        tempfile_1


    - name: '21. Touch backup file'
      become: true
      ignore_errors: true
      shell:  |
        tmp="{{ tempfile_1.path }}"
        bfile="{{ bdir }}/{{ bfile }}"
        #
        echo "$ sudo touch ${bfile}"
        touch "${bfile}"  2>&1
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "- Error, rc: $rc"
            exit $rc
        fi
        #
        echo "$ sudo chmod 600 ${bfile}"
        chmod 600 "${bfile}"  2>&1
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "- Error, rc: $rc"
            exit $rc
        fi
        #
        echo "$ ls -lhF ${tmp}"
        ls -lhF "${tmp}"  2>&1
        rc=$?
        exit $rc
      args:
        executable: /bin/bash
      register: tout
      when:
        (('21' in steps) or (steps | length == 0))

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('21' in steps) or (steps | length == 0))  and
        ( (('rc' in tout) and (tout.rc != 0)) or
          (APE_v | int >= 1) )

    - name: '21. Touch failed?'
      fail:
        msg: "Touch failed."
      when:
        (('21' in steps) or (steps | length == 0))  and
        (tout.rc != 0)



    - name: '22. tar backup'
      become: true
      ignore_errors: true
      shell:  |
        tmp="{{ tempfile_1.path }}"
        bdir="{{ bdir }}"
        bfile="${bdir}/{{ bfile }}"
        #
        dirs_str="{{ dirs_str }}"
        ex_str="--exclude={{ ex_str }}"
        #
        vx={{ APE_v }}
        if [[ "${vx}" -ge 2 ]]; then        # - tar verbose?
            v='v'
        else
            v=''
        fi
        #
        which expand  > /dev/null  2>&1
        rc=$?
        if [[ $rc -eq 0 ]]; then
            expand="expand"         # - Convert tabs to spaces, fix Ansible '\t' output.
        else
            expand=cat              # - Just skip it.
        fi
        #
        echo "$ cd /"
        cd /
        echo "$ bfile='${bfile}'"
        echo "$ ex_str='${ex_str} '"
        echo "$ dirs_str='${dirs_str} '"
        #
        w="--warning=no-file-changed"               # - Tolerate this, which also generates rc=1
        echo
        echo "$ sudo time (tar cz${v}f  \${bfile} ${w} \${ex_str}  \${dirs_str})"
        out=$(time (tar cz${v}f  "${bfile}"  ${w}  ${ex_str}  ${dirs_str} 2>&1) 2>&1) # - Screwy Bash
        rc=$?
        echo "${out}" | ${expand} 2>&1
        if [[ $rc -eq 1 ]]; then
            echo "- Warning: rc=1, ignoring it."    # - tar warning: "file changed while reading" => rc=1
            rc=0
        fi
        if [[ $rc -ne 0 ]]; then
            echo "- Error, rc: $rc"
            exit $rc
        fi
        #
        echo
        echo "$ sudo tar tzvf \${bfile}  > ${tmp}  2>&1"
        out=$(tar tzvf "${bfile}"  > "${tmp}"  2>&1)
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "${out}" | head
            echo "- Error, rc: $rc"
            exit $rc
        fi
        #
        echo
        echo "$ sudo ls -lhF \${bfile}"
        ls -lhF "${bfile}"  2>&1
        rc=$?
        if [[ $rc -ne 0 ]]; then
            head "${tmp}"
            echo "- Error, rc: $rc"
            exit $rc
        fi
        true
      args:
        executable: /bin/bash
      register: tout
      when:
        (('22' in steps) or (steps | length == 0))


    - debug:
        var: tout
      when:
        (('22' in steps) or (steps | length == 0))  and
        ( (('rc'     in tout) and (tout.rc    != 0))      or
          (('failed' in tout) and (tout.failed == true))  or
          (APE_v | int >= 0) )


    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('22' in steps) or (steps | length == 0))  and
        ( (('rc' in tout) and (tout.rc != 0))             or
          (('failed' in tout) and (tout.failed == true))  or
          (APE_v | int >= 0) )


    - name: '22. Backup failed?'
      fail:
        msg: "Backup failed."
      when:
        (('22' in steps) or (steps | length == 0))  and
        ( (('failed' in tout) and (tout.failed == true))  or
          (('rc' in tout) and (tout.rc != 0)) )



    - name: '23. tar FYI'
      become: true
      shell:  |
        tmp="{{ tempfile_1.path }}"
        bdir="{{ bdir }}"
        bfile="${bdir}/{{ bfile }}"
        #
        which expand  > /dev/null  2>&1
        rc=$?
        if [[ $rc -eq 0 ]]; then
            expand="expand"         # - Convert tabs to spaces, fix Ansible '\t' output.
        else
            expand=cat              # - Just skip it.
        fi
        #
        echo "$ cat ${tmp} | sort -rn -k3 | head -20"
        out=$(cat "${tmp}" 2>&1)
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "${out}"
            echo "- Error, rc: $rc"
            exit 0
        fi
        echo "${out}"    | \
            sort -rn -k3 | \
            head -20     | \
            perl -ne '
                chomp;
                if (/^(\S+\s+)(\S+)\s+(\d+)\s(.*)$/) {
                    my $t = commify ($3);
                    print $1. " ". (sprintf ("%-12s %12s", $2, $t)). "  ". $4. "\n"
                } else {
                    print $_. "\n";
                }
            sub commify {
                my $a = shift;
                my $x = ",";        # - Delimiter: comma
                while ($a =~ s/(\d+)(\d\d\d)/$1${x}$2/) {};
                return $a;
            }
            '
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "${out}"
            echo "- Error, rc: $rc"
            exit 0
        fi
      args:
        executable: /bin/bash
      register: tout
      when:
        (('23' in steps) or (steps | length == 0))

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('23' in steps) or (steps | length == 0))  and
        (APE_v | int >= 0)


    - name: '30. Timestamp 2'
      shell: |
        echo "$ date"
        date
      register: tout
      when:
        (('30' in steps) or (steps | length == 0))

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('30' in steps) or (steps | length == 0))  and
        (APE_v | int >= 1)


    - name: '31. Fetch backup to local backup directory: {{ local_bdir }}'
      become: true
      ignore_errors: true
      ansible.builtin.fetch:
        src:    "{{ bdir }}/{{ bfile }}"
        dest:   "{{ local_bdir }}/{{ inventory_hostname }}/"
        flat:   yes
      register: toutb
      when:
        ('31' in steps) or (steps | length == 0)

    - debug:
        var: toutb
      when:
        (('31' in steps) or (steps | length == 0))  and
        ( (('failed' in toutb) and (toutb.failed == true)) or
            (APE_v | int >= 1) )

    - name: '31. Fetch failed?'
      fail:
        msg: "Fetch failed."
      when:
        (('31' in steps) or (steps | length == 0))  and
        (('failed' in toutb) and (toutb.failed == true))


    - name: '40. Cleanup: Remove temporary backup files.'
      become: true
      ignore_errors: true
      ansible.builtin.file:
        path: "{{ item }}"
        state: absent
      register: toutb
      loop:
        - "/var/log/fail2ban.db.bak"
        - "/var/log/fail2ban.db.unx"
        - "{{ tempfile_1.path }}"
        - "{{ bdir }}/{{ bfile }}"
      when:
        (('40' in steps) or (steps | length == 0))

    - debug:
        var: toutb
      when:
        (('40' in steps) or (steps | length == 0))  and
        ( (('failed' in toutb) and (tout.failed == true)) or
          (APE_v | int >= 2) )


