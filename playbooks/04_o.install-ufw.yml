# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.
# v_ansible='8.0.0'

# - Config Ubuntu Linux.
#   -> UFW

# - FYI:
#
#   https://docs.ansible.com/ansible/latest/collections/community/general/ufw_module.html
#   https://docs.ansible.com/ansible/2.9/modules/ufw_module.html
#
#   https://debugfactor.com/all-about-ansible-loops-with-examples/
#   https://medium.com/opsops/using-default-value-if-string-is-empty-4333e0abc9b3
#   https://blog.crisp.se/2016/10/20/maxwenzin/how-to-append-to-lists-in-ansible
#   https://docs.ansible.com/ansible/latest/user_guide/playbooks_tags.html#special-tags-always-and-never


---

- hosts: localhost
  gather_facts: yes

  vars:
    APE_v:      "{{ lookup ('env', 'APE_verbose') or 0 }}"                  # - verbose mode
    LOOP_v_tag: "{{ (APE_v | int > 0) | ternary ('always', 'never') }}"     # - verbose-only LOOP
    steps:      "{{ lookup ('env', 'APE_STEPS') | default ([], true) }}"    # - list of sections (optional)

  tasks:

    - name: "00. Timestamp"
      debug:
        msg: "{{ ansible_date_time.date }} {{ ansible_date_time.time }} {{ ansible_date_time.tz }}"
      when:
        (('00' in steps) or (steps | length == 0))


- hosts: "{{ lookup ('env', 'APE_HOSTS') }}"
  gather_facts: yes

  vars:
    APE_v:      "{{ lookup ('env', 'APE_verbose') or 0 }}"                  # - verbose mode
    LOOP_v_tag: "{{ (APE_v | int > 0) | ternary ('always', 'never') }}"     # - verbose-only LOOP
    steps:      "{{ lookup ('env', 'APE_STEPS') | default ([], true) }}"    # - list of sections (optional)

    # - Load and confirm these host_vars.
    #   Provide a default, because some lists may be empty in the definitions file.
    # - FYI
    #   ... | default ('off',true)   -> Provide default content, if variable is not defined.
    #   ... or 'off'                 -> Provide default content, if variable is defined but empty.

    x_my_ufw_log:       "{{ my_ufw_log       or 'off' }}"
    x_my_ufw_logging:   "{{ my_ufw_logging   or 'off' }}"

    x_my_ufw_networks:  "{{ my_ufw_networks  | default ([], true) }} "  # - A list is needed, for loop.
    x_my_ufw_ports_tcp: "{{ my_ufw_ports_tcp | default ([], true) }} "  #   An empty list is OK.
    x_my_ufw_ports_udp: "{{ my_ufw_ports_udp | default ([], true) }} "
    x_my_ufw_ports_all: "{{ my_ufw_ports_all | default ([], true) }} "
    x_my_ufw_ip_esp:    "{{ my_ufw_ip_esp    | default ([], true) }} "  # - Optional
    x_my_ufw_ip_ah:     "{{ my_ufw_ip_ah     | default ([], true) }} "  # - Optional

  tasks:

    - name: '01. Diagnostic: Show scalars'
      debug:
        msg: "item: {{ item }}"
      loop:
        - "{{ x_my_ufw_log }}"
        - "{{ x_my_ufw_logging }}"
      when:
        (('01' in steps) or (steps | length == 0))
      tags: [ "{{ LOOP_v_tag }}" ]      # - Loop only in verbose mode.


    - name: '02. Diagnostic: Show lists'
      debug:
        msg: "item: {{ item }}"
      loop:
        "{{
            x_my_ufw_networks  +
            x_my_ufw_ports_tcp +
            x_my_ufw_ports_udp +
            x_my_ufw_ports_all +
            x_my_ufw_ip_esp +
            x_my_ufw_ip_ah
         }}"
      when:
        (('02' in steps) or (steps | length == 0))
      tags: [ "{{ LOOP_v_tag }}" ]      # - Loop only in verbose mode.


    - name: '03. Sanity check prep #1'
      package_facts:
        manager: "auto"
      when:
        (('03' in steps) or (steps | length == 0))

    - name: '04. Sanity check #1'
      fail:
        msg='Package not installed- ufw'
      when:
        (('04' in steps) or (steps | length == 0))  and
        ('ufw' not in ansible_facts.packages)


    - name: '05. Reset UFW'
      become: true
      community.general.ufw:
        state: reset
      when:
        (('05' in steps) or (steps | length == 0))


    - name: '06. Enable UFW and allow everything.'
      become: true
      community.general.ufw:
        state:   enabled
        default: allow
        log:     '{{ x_my_ufw_log }}'
        logging: '{{ x_my_ufw_logging }}'
      when:
        (('06' in steps) or (steps | length == 0))


    - name: '07. Allow ports, via TCP protocol'
      become: true
      community.general.ufw:
        rule:  allow
        port:  '{{ item }}'
        proto: tcp
      loop: "{{ x_my_ufw_ports_tcp }}"
      when:
        (('07' in steps) or (steps | length == 0))


    - name: '08. Allow networks'
      become: true
      community.general.ufw:
        rule:    allow
        from_ip: '{{ item }}'
      loop: "{{ x_my_ufw_networks }}"
      when:
        (('08' in steps) or (steps | length == 0))


    - name: '09. Allow ports, via UDP protocol'
      become: true
      community.general.ufw:
        rule:  allow
        port:  '{{ item }}'
        proto: udp
      loop: "{{ x_my_ufw_ports_udp }}"
      when:
        (('09' in steps) or (steps | length == 0))


    - name: '10. Allow ports, via all protocols'
      become: true
      community.general.ufw:
        rule:  allow
        port:  '{{ item }}'
      loop: "{{ x_my_ufw_ports_all }}"
      when:
        (('10' in steps) or (steps | length == 0))


    - name: '11. Allow IPsec IP address, all ports, ESP protocol'
      become: true
      community.general.ufw:
        rule:   allow
        to_ip:  '{{ item }}'
        proto:  esp
      loop: "{{ x_my_ufw_ip_esp }}"
      when:
        (('11' in steps) or (steps | length == 0))


    - name: '12. Allow IPsec IP address, all ports, AH protocol'
      become: true
      community.general.ufw:
        rule:   allow
        to_ip:  '{{ item }}'
        proto:  ah
      loop: "{{ x_my_ufw_ip_ah }}"
      when:
        (('12' in steps) or (steps | length == 0))


    - name: '13. Set the default to DENY all. if none of the above rules match.'
      become: true
      community.general.ufw:
        default: reject
      when:
        (('13' in steps) or (steps | length == 0))


    - name: '14. Enable UFW service, after next boot.'
      become: true
      community.general.ufw:
        state: enabled
      register: toutb
      when:
        (('14' in steps) or (steps | length == 0))

    - debug:
        var: toutb
      when:
        (('14' in steps) or (steps | length == 0))  and
        (APE_v | int >= 1)


    - name: '15. Restart UFW service now.'
      become: true
      ansible.builtin.systemd:
        daemon_reload: yes
        enabled:       yes
        state:         restarted
        name:          ufw
      register: toutb
      when:
        (('15' in steps) or (steps | length == 0))

    - debug:
        var: toutb['enabled']
      when:
        (('15' in steps) or (steps | length == 0))  and
        (APE_v | int >= 1)


    - name: '16. Show UFW config (aka ufw status)... assuming inbound Ansible SSH still works.'
      become: true
      shell: |
        echo '-$ sudo ufw status verbose | grep -i status'
        ufw status verbose | grep -i status
        echo
        echo '-$ sudo ufw status verbose'
        ufw status verbose
        true
      register: tout
      when:
        (('16' in steps) or (steps | length == 0))  and
        (APE_v | int >= 1)

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('16' in steps) or (steps | length == 0))  and
        (APE_v | int >= 1)


    - name: '17. ns: Show listening ports'
      become: true
      shell: |
        echo "-\$ netstat -an | grep -i listen | egrep ^tcp | sort"
        netstat -an | grep -i listen | egrep ^tcp | sort
        true
      args:
        executable: /bin/bash
      register: tout
      when:
        (('17' in steps) or (steps | length == 0))

    - debug:
        var: tout | dict2items | selectattr('key', 'in', _keys_filter) | items2dict
      vars:
        _keys_filter:
          - stdout_lines
          - stderr_lines
      when:
        (('17' in steps) or (steps | length == 0))  and
        (APE_v | int >= 1)

