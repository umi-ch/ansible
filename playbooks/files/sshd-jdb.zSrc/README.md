
# Ubuntu Honeypot: Show passwords of failed SSH logins

Here I compile a custom build an **SSHD** which logs failed passwords.<br>
Although this sounds like a big project, it proceeds smoothly and quickly<br>
on the Raspberry PI platform.<br>

There is no security risk here, as all my SSH logins use **public key authentication**,<br>
and **none** of these SSH-enabled user accounts have a **defined password**.<br>

Therefore all SSH password login attempts here are **inappropriate**,<br>
and usually from **Internet Malware hackers**.<br>
So it can be interesting and useful to analyze just what **credentials** are attempted.<br>

The standard open-source SSHD has no provision to log passwords,<br>
but this is readily accomplished with a small source-code patch.

My custom SSHD listens on port **2223** (for example).<br>
This means, it can run in **parallel** with Ubuntu standard SSHD listening on port **22**.<br>
(In my secured Ecosystem, I **disable** the standard SSHD port 22 listener.)

A dedicated directory is used:   ```/usr/local/ssh-jdb/```

The **GCC** compiler is required, and helper source-code packages are used:<br>

- https://hackernoon.com/how-ive-captured-all-passwords-trying-to-ssh-into-my-server-d26a2a6263ec
- https://www.systutorials.com/how-to-install-the-zlib-library-in-ubuntu/
- https://packages.ubuntu.com/source/focal/openssl

Example:

```
pi$ sudo tail -50f /var/log/auth.log | egrep -i "Honey:"
Oct 23 10:28:17 cherry-pi sshd[35032]: Honey:  Port: 2223   IP: 211.58.232.134   Username: sammy     Password: '123123'
Oct 23 10:30:19 cherry-pi sshd[35044]: Honey:  Port: 2223   IP: 43.154.228.228   Username: riccardo  Password: 'riccardo'
Oct 23 10:35:02 cherry-pi sshd[35054]: Honey:  Port: 2223   IP: 193.123.231.194  Username: root      Password: 'QAZ123qaz'
Oct 23 11:00:49 cherry-pi sshd[35114]: Honey:  Port: 2223   IP: 165.22.102.152   Username: root      Password: 'P@SSW0RD'
Oct 23 11:01:23 cherry-pi sshd[35123]: Honey:  Port: 2223   IP: 43.154.143.45    Username: mysql     Password: 'mysql@123'
```
<br>

There are other approaches to extra SSHD security. Eg: Modify **PAM**<br>

- http://www.adeptus-mechanicus.com/codex/logsshp/logsshp.html
- https://github.com/desaster/kippo
- https://serverfault.com/questions/636412/ssh-failed-password-login-log-password
<br>

## Summary

```
pi$ date; whoami; uname -n
    Fri Nov 11 07:38:08 CET 2022
    jdb
    apple-pi

pi$ sudo apt install -y gcc make
pi$ sudo apt install -y zlib1g-dev zlib1g
pi$ sudo apt install -y libssl-dev libssl-doc libssl1.1
pi$ sudo apt install -y libpam0g-dev

# - Summary:
pi$ export OPENSSH_DEV=~/src/openssh2.jdb/  OPENSSH_DESTDIR=~/src/openssh2.jdb/zRoot/  OPENSSH=/usr/local/sshd-jdb/


# - Location: DEV (for building)
#
pi$ export OPENSSH_DEV=~/src/openssh2.jdb/
pi$ mkdir -p  ${OPENSSH_DEV}/zRaw/  ${OPENSSH_DEV}/dist/


# - Location: DEV "DESTDIR" (for packaging)
#   https://www.gnu.org/prep/standards/html_node/DESTDIR.html
# - This folder will be changed later, to be owned by root.
# - If $OPENSSH_DESTDIR is NFS mounted, that might not work as expected:
#   -> user "root" is often mapped to user "nobody".
#   In this case, put the folder elsewhere, eg:
#   pi$ export OPENSSH_DESTDIR=/tmp/zRoot/
#
pi$ export OPENSSH_DESTDIR=${OPENSSH_DEV}/zRoot/
pi$ mkdir -p  ${OPENSSH_DESTDIR}


# - Location: PROD (for installation)
#   -> subfolder ./dist/ is deployed here, with with tar or package.
#
pi$ export OPENSSH=/usr/local/openssh2.jdb/
pi$ sudo mkdir -p  ${OPENSSH}


# - Prep for this OpenSSH, both DEV and PROD.
#
pi$ sudo mkdir -p /var/empty/
pi$ sudo chown 755 /var/empty/


# - FYI: The build process is sensitive to versions.
#
pi$ cat /etc/os-release | grep VERSION
    VERSION="20.10 (Groovy Gorilla)"
    VERSION_ID="20.10"
    VERSION_CODENAME=groovy


# - FYI: The OpenSSL version must compatible with the OpenSSH version.
#
pi$ which openssl; openssl version
    /usr/bin/openssl
    OpenSSL 1.1.1f  31 Mar 2020


# - FYI:  Show contents of ZLIB package
#
pi$ dpkg-query -L zlib1g-dev | egrep -v '/share'
    /.
    /usr
    /usr/include
    /usr/include/zconf.h
    /usr/include/zlib.h
    /usr/lib
    /usr/lib/aarch64-linux-gnu
    /usr/lib/aarch64-linux-gnu/libz.a
    /usr/lib/aarch64-linux-gnu/pkgconfig
    /usr/lib/aarch64-linux-gnu/pkgconfig/zlib.pc
    /usr/lib/aarch64-linux-gnu/libz.so
```
<br>


## Get the source code

```
###################
# Get source code #
###################

pi$ cd ${OPENSSH_DEV}/

# - Get "the latest" OpenSSH source code package.
#   -> Browse to the site, to check what's available:
#   https://www.openssh.com/releasenotes.html
#   https://mirror.ungleich.ch/pub/OpenBSD/OpenSSH/portable

pi$ curl -o zRaw/openssh-8.4p1.tar.gz  https://mirror.ungleich.ch/pub/OpenBSD/OpenSSH/portable/openssh-8.4p1.tar.gz
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 1701k  100 1701k    0     0  5283k      0 --:--:-- --:--:-- --:--:-- 5283k

pi$ md5sum zRaw/openssh-8.4p1.tar.gz
    8f897870404c088e4aa7d1c1c58b526b  zRaw/openssh-8.4p1.tar.gz

pi$ tar xvfz zRaw/openssh-8.4p1.tar.gz
    ...
```
<br>


## Patch the source code

```
#########################
# Patch the source code #
#########################

pi$ cd ${OPENSSH_DEV}/openssh-8.4p1/

pi$ cp -p  auth-passwd.c  auth-passwd.c-ORIG

pi$ cp -p  ~/zGit.pi/pi/sshd/auth-passwd.c  .


pi$ diff auth-passwd.c auth-passwd.c-ORIG  2>&1 | tee q0_a.diffs
    79,84d78
    <       static char h_port[10];
    <       static char h_ipaddr[50];
    <       static char h_user[100];
    <       static char h_pass[100];
    <       static char tmp[100];
    <
    86,107c80
    <       /* logit("Honey: Port: %d  IP: %s  Username: %s  Password: '%s'", ssh_local_port(ssh), ssh_remote_ipaddr(ssh), authctxt->user, password); */
    <
    <       /*
    <               https://www.tutorialspoint.com/c_standard_library/c_function_sprintf.htm
    <               https://stackoverflow.com/questions/1496313/returning-a-c-string-from-a-function
    <               int sprintf(char *str, const char *format, ...)
    <               sprintf(str, "Value of Pi = %f", M_PI);
    <       */
    <       sprintf (h_port,     "%4d",  ssh_local_port(ssh));
    <       sprintf (h_ipaddr, "%-16s",  ssh_remote_ipaddr(ssh));
    <
    <       strncpy (tmp, authctxt->user, 99);
    <       sprintf (h_user, "%-14s", tmp);
    <
    <       strcpy  (tmp, "'");
    <       strncat (tmp, password, 97);
    <       strncat (tmp, "'", 1);
    <       sprintf (h_pass, "%-20s", tmp);
    <
    <       logit("Honey:  Port: %s   IP: %s Username: %s  Password: %s", h_port, h_ipaddr, h_user, h_pass);
    <
    < struct passwd *pw = authctxt->pw;
    ---
    >       struct passwd *pw = authctxt->pw;
    222,223c195
    <         /* ## logit("Honey-2: Username: %s Password: %s", authctxt->user, password); */
    < struct passwd *pw = authctxt->pw;
    ---
    >       struct passwd *pw = authctxt->pw;
```

&nbsp;

## Confirm the OpenSSL source-code headers.

```
#################################################
# FYI: Confirm the OpenSSL source-code headers. #
#################################################
#
# - The output below looks OK.
#   This area is often a cause for OpenSSH build failures.
#
pi$ chmod 755 ./contrib/findssl.sh

pi$ ./contrib/findssl.sh  2>&1  |  tee q0_b.findssl
    Searching for OpenSSL header files.
    OPENSSL_VERSION_NUMBER /usr/include/openssl/opensslv.h

    Searching for OpenSSL shared library files.
    0x100020efL /snap/core18/1949/usr/lib/aarch64-linux-gnu/libcrypto.so.1.0.0
    0x100020efL /snap/core18/1888/usr/lib/aarch64-linux-gnu/libcrypto.so.1.0.0

    Searching for OpenSSL static library files.
pi$
```
<br>


## Configure

```
#############
# Configure #
#############
#
#   -> A distinct run directory is needed for the pid file,
#      to prevent collusions with the standard location:  /var/run/sshd.pid

pi$ time ./configure  --prefix=${OPENSSH}/dist/  --with-pid-dir=${OPENSSH}/dist/run  --with-pam  2>&1  | tee q1.conf
...
    OpenSSH has been configured with the following options:
                         User binaries: /usr/local/openssh2.jdb//dist/bin
                       System binaries: /usr/local/openssh2.jdb//dist/sbin
                   Configuration files: /usr/local/openssh2.jdb//dist/etc
                       Askpass program: /usr/local/openssh2.jdb//dist/libexec/ssh-askpass
                          Manual pages: /usr/local/openssh2.jdb//dist/share/man/manX
                              PID file: /usr/local/openssh2.jdb//dist/run
      Privilege separation chroot path: /var/empty
                sshd default user PATH: /usr/bin:/bin:/usr/sbin:/sbin:/usr/local/openssh2.jdb//dist/bin
                        Manpage format: doc
                           PAM support: yes
                       OSF SIA support: no
                     KerberosV support: no
                       SELinux support: no
                  MD5 password support: no
                       libedit support: no
                       libldns support: no
      Solaris process contract support: no
               Solaris project support: no
             Solaris privilege support: no
           IP address in $DISPLAY hack: no
               Translate v4 in v6 hack: yes
                      BSD Auth support: no
                  Random number source: OpenSSL internal ONLY
                 Privsep sandbox style: seccomp_filter
                       PKCS#11 support: yes
                      U2F/FIDO support: yes

                  Host: aarch64-unknown-linux-gnu
              Compiler: cc
        Compiler flags: -g -O2 -pipe -Wno-error=format-truncation -Wall -Wextra -Wpointer-arith -Wuninitialized -Wsign-compare -Wformat-security -Wsizeof-pointer-memaccess -Wno-pointer-sign -Wno-unused-parameter -Wno-unused-result -Wimplicit-fallthrough -fno-strict-aliasing -D_FORTIFY_SOURCE=2 -ftrapv -fno-builtin-memset -fstack-protector-strong -fPIE
    Preprocessor flags:  -D_XOPEN_SOURCE=600 -D_BSD_SOURCE -D_DEFAULT_SOURCE
          Linker flags:  -Wl,-z,relro -Wl,-z,now -Wl,-z,noexecstack -fstack-protector-strong -pie
             Libraries: -lcrypto -ldl -lutil -lz  -lcrypt -lresolv
             +for sshd:  -lpam

    PAM is enabled. You may need to install a PAM control file
    for sshd, otherwise password authentication may fail.
    Example PAM control files can be found in the contrib/
    subdirectory

    real    2m45.210s
    user    1m10.258s
    sys     1m19.583s


pi$ grep OpenSSL q1.conf
    checking whether OpenSSL will be used for cryptography... yes
    checking OpenSSL header version... 1010106f (OpenSSL 1.1.1f  31 Mar 2020)
    checking for OpenSSL_version... yes
    checking for OpenSSL_version_num... yes
    checking OpenSSL library version... 1010106f (OpenSSL 1.1.1f  31 Mar 2020)
    checking whether OpenSSL's headers match the library... yes
    checking if programs using OpenSSL functions will link... yes
    checking for OpenSSL_add_all_algorithms... no
    checking whether OpenSSL_add_all_algorithms is declared... yes
    checking whether OpenSSL has crippled AES support... no
    checking whether OpenSSL has AES CTR via EVP... yes
    checking whether OpenSSL has AES GCM via EVP... yes
    checking whether OpenSSL has NID_X9_62_prime256v1... yes
    checking whether OpenSSL has NID_secp384r1... yes
    checking whether OpenSSL has NID_secp521r1... yes
    checking if OpenSSL's NID_secp521r1 is functional... yes
    checking whether OpenSSL's PRNG is internally seeded... yes
                  Random number source: OpenSSL internal ONLY
```
<br>


## Build / Compile

```
###################
# Build / Compile #
###################

pi$ time make  2>&1 | tee q2.make
    ...
    cc -g -O2 -pipe -Wno-error=format-truncation -Wall -Wextra -Wpointer-arith -Wuninitialized -Wsign-compare -Wformat-security -Wsizeof-pointer-memaccess -Wno-pointer-sign -Wno-unused-parameter -Wno-unused-result -Wimplicit-fallthrough -fno-strict-aliasing -D_FORTIFY_SOURCE=2 -ftrapv -fno-builtin-memset -fstack-protector-strong -fPIE   -I. -I.  -D_XOPEN_SOURCE=600 -D_BSD_SOURCE -D_DEFAULT_SOURCE -DSSHDIR=\"/usr/local/openssh2.jdb//dist/etc\" -D_PATH_SSH_PROGRAM=\"/usr/local/openssh2.jdb//dist/bin/ssh\" -D_PATH_SSH_ASKPASS_DEFAULT=\"/usr/local/openssh2.jdb//dist/libexec/ssh-askpass\" -D_PATH_SFTP_SERVER=\"/usr/local/openssh2.jdb//dist/libexec/sftp-server\" -D_PATH_SSH_KEY_SIGN=\"/usr/local/openssh2.jdb//dist/libexec/ssh-keysign\" -D_PATH_SSH_PKCS11_HELPER=\"/usr/local/openssh2.jdb//dist/libexec/ssh-pkcs11-helper\" -D_PATH_SSH_SK_HELPER=\"/usr/local/openssh2.jdb//dist/libexec/ssh-sk-helper\" -D_PATH_SSH_PIDDIR=\"/var/run\" -D_PATH_PRIVSEP_CHROOT_DIR=\"/var/empty\" -DHAVE_CONFIG_H -c sk-usbhid.c -o sk-usbhid.o
    cc -o ssh-sk-helper ssh-sk-helper.o ssh-sk.o sk-usbhid.o -L. -Lopenbsd-compat/  -Wl,-z,relro -Wl,-z,now -Wl,-z,noexecstack -fstack-protector-strong -pie  -lssh -lopenbsd-compat -lssh -lopenbsd-compat -lcrypto -ldl -lutil -lz  -lcrypt -lresolv

    real    2m53.268s
    user    2m19.723s
    sys     0m29.851s
```
<br>


## Deploy into packaging directory

```
###################################
# Deploy into packaging directory #
###################################
#
# - Installation into a "dev" location.

pi$ cd ${OPENSSH_DEV}/openssh-8.4p1/


# - Prep: Clean previous work from the DESTDIR.
#
pi$ if [[ -d "${OPENSSH_DESTDIR}" ]]; then echo " - \$ sudo rm -r ${OPENSSH_DESTDIR}"; sudo rm -r "${OPENSSH_DESTDIR}"; mkdir -p "${OPENSSH_DESTDIR}"; else echo "Skipped #1"; fi
 - $ sudo rm -r /home/jdb/src/openssh2.jdb//zRoot/


# - Decision:
#   The deployment package (eg: tar file) does NOT contain SSH host keys.
#   On the target system, the Ubuntu default keys are used -- as per distributed config file.
#   Alternately, new host keys are readily re-created:  $ ssh-keygen -A
#
# - Bug/Quirk:
#   OpenSSH "make install" does not look for, or create, SSH host keys in ${DESTDIR}.
#   Instead, it looks for these in the ultimate installation directory:  ${OPENSSL}/dist/etc/
#   This is almost reasonable, as the default host key location is built into the SSHD binary,
#   and alternate locations can appear in the config file.
# - By default, SSHD host key files are only readable by root.
# - The only problem is, the last step
#
# - Mission: Manually make host keys if absent, to enable succesful "make install".
#   These host keys won't be distributed.
#   Unfortunately, the OpenSSH makefile does not honor DESTDIR in that deployment step.
# - ToDo: Can this "deployment config directory" be passed to make?   --> No, seems not.
#
# - sudo is required, to work with host keys in production locations.

pi$ /bin/ls -ld ${OPENSSH}/dist/etc/ > /dev/null 2>&1; rc=$?
pi$ if [[ $rc -ne 0 ]]; then echo " - \$sudo mkdir ..."; sudo mkdir -p ${OPENSSH}/dist/etc/; else echo "- skipped #2"; fi
    - $sudo mkdir ...

pi$ /bin/ls ${OPENSSH}/dist/etc/ssh_host*key > /dev/null 2>&1; rc=$?
pi$ if [[ $rc -ne 0 ]]; then echo " - \$sudo ./ssh-keygen ..."; sudo ./ssh-keygen -A; else echo "- skipped #3"; fi
    - $sudo ./ssh-keygen ...
    ssh-keygen: generating new host keys: RSA DSA ECDSA ED25519


pi$ ls -l ${OPENSSH}/dist/etc/
    total 612K
    -r--r--r-- 1 root root 565K Jan 21 21:54 moduli
    -r--r--r-- 1 root root 1.5K Jan 21 21:54 ssh_config
    -rw------- 1 root root 1.4K Jan 21 22:21 ssh_host_dsa_key
    -rw-r--r-- 1 root root  603 Jan 21 22:21 ssh_host_dsa_key.pub
    -rw------- 1 root root  505 Jan 21 22:21 ssh_host_ecdsa_key
    -rw-r--r-- 1 root root  175 Jan 21 22:21 ssh_host_ecdsa_key.pub
    -rw------- 1 root root  399 Jan 21 22:21 ssh_host_ed25519_key
    -rw-r--r-- 1 root root   95 Jan 21 22:21 ssh_host_ed25519_key.pub
    -rw------- 1 root root 2.6K Jan 21 22:21 ssh_host_rsa_key
    -rw-r--r-- 1 root root  567 Jan 21 22:21 ssh_host_rsa_key.pub
    -r--r--r-- 1 root root 3.3K Jan 21 21:55 sshd_config
    dr-xr-xr-x 2 root root 4.0K Jan 21 21:55 sshd_config.d/


# - Temporarily enable world-read access, so `make install` works.
pi$ sudo chmod 644 ${OPENSSH}/dist/etc/ssh_host*


# - Install, into distribution directory.
#
pi$ time make  DESTDIR=${OPENSSH_DESTDIR}  install  2>&1 | tee q3.install
    Makefile:655: warning: ignoring prerequisites on suffix rule definition
    (cd openbsd-compat && make)
    make[1]: Entering directory '/home/jdb/src/openssh2.jdb/openssh-8.4p1/openbsd-compat'
    make[1]: Nothing to be done for 'all'.
    make[1]: Leaving directory '/home/jdb/src/openssh2.jdb/openssh-8.4p1/openbsd-compat'
    /usr/bin/mkdir -p /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/bin
    /usr/bin/mkdir -p /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/sbin
    /usr/bin/mkdir -p /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/share/man/man1
    /usr/bin/mkdir -p /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/share/man/man5
    /usr/bin/mkdir -p /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/share/man/man8
    /usr/bin/mkdir -p /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/libexec
    /usr/bin/mkdir -p -m 0755 /home/jdb/src/openssh2.jdb//zRoot//var/empty
    /usr/bin/install -c -m 0755 -s ssh /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/bin/ssh
    /usr/bin/install -c -m 0755 -s scp /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/bin/scp
    /usr/bin/install -c -m 0755 -s ssh-add /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/bin/ssh-add
    /usr/bin/install -c -m 0755 -s ssh-agent /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/bin/ssh-agent
    /usr/bin/install -c -m 0755 -s ssh-keygen /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/bin/ssh-keygen
    /usr/bin/install -c -m 0755 -s ssh-keyscan /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/bin/ssh-keyscan
    /usr/bin/install -c -m 0755 -s sshd /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/sbin/sshd
    /usr/bin/install -c -m 4711 -s ssh-keysign /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/libexec/ssh-keysign
    /usr/bin/install -c -m 0755 -s ssh-pkcs11-helper /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/libexec/ssh-pkcs11-helper
    /usr/bin/install -c -m 0755 -s ssh-sk-helper /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/libexec/ssh-sk-helper
    /usr/bin/install -c -m 0755 -s sftp /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/bin/sftp
    /usr/bin/install -c -m 0755 -s sftp-server /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/libexec/sftp-server
    /usr/bin/install -c -m 644 ssh.1.out /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/share/man/man1/ssh.1
    /usr/bin/install -c -m 644 scp.1.out /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/share/man/man1/scp.1
    /usr/bin/install -c -m 644 ssh-add.1.out /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/share/man/man1/ssh-add.1
    /usr/bin/install -c -m 644 ssh-agent.1.out /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/share/man/man1/ssh-agent.1
    /usr/bin/install -c -m 644 ssh-keygen.1.out /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/share/man/man1/ssh-keygen.1
    /usr/bin/install -c -m 644 ssh-keyscan.1.out /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/share/man/man1/ssh-keyscan.1
    /usr/bin/install -c -m 644 moduli.5.out /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/share/man/man5/moduli.5
    /usr/bin/install -c -m 644 sshd_config.5.out /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/share/man/man5/sshd_config.5
    /usr/bin/install -c -m 644 ssh_config.5.out /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/share/man/man5/ssh_config.5
    /usr/bin/install -c -m 644 sshd.8.out /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/share/man/man8/sshd.8
    /usr/bin/install -c -m 644 sftp.1.out /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/share/man/man1/sftp.1
    /usr/bin/install -c -m 644 sftp-server.8.out /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/share/man/man8/sftp-server.8
    /usr/bin/install -c -m 644 ssh-keysign.8.out /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/share/man/man8/ssh-keysign.8
    /usr/bin/install -c -m 644 ssh-pkcs11-helper.8.out /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/share/man/man8/ssh-pkcs11-helper.8
    /usr/bin/install -c -m 644 ssh-sk-helper.8.out /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/share/man/man8/ssh-sk-helper.8
    /usr/bin/mkdir -p /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/etc
    /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/sbin/sshd -t -f /home/jdb/src/openssh2.jdb//zRoot//usr/local/openssh2.jdb//dist/etc/sshd_config

    real    0m0.529s
    user    0m0.126s
    sys     0m0.413s


# - Restore root-only access to the host keys.
#
pi$ sudo chmod 600 ${OPENSSH}/dist/etc/ssh_host*
```
<br>


## Post-install prep for distribution

```
    ######################################
    # Post-install prep for distribution #
    ######################################

# - Directory for PID file
pi$ mkdir -p "${OPENSSH_DESTDIR}/${OPENSSH}"/dist/run/

# - Add custom configs, from Git.
pi$ mkdir -p "${OPENSSH_DESTDIR}/${OPENSSH}"/dist/etc/sshd_config.d/

pi$ cp -p  ~/zGit.pi/pi/sshd/sshd-pi-*conf*  "${OPENSSH_DESTDIR}/${OPENSSH}"/dist/etc/sshd_config.d/

pi$ (cd "${OPENSSH_DESTDIR}/${OPENSSH}/dist/etc/sshd_config.d/"; ln -s sshd-pi-2223.conf-NO sshd-pi.conf)

pi$ chmod 755 "${OPENSSH_DESTDIR}/${OPENSSH}/dist/etc/sshd_config.d/"

pi$ (cd "${OPENSSH_DESTDIR}/${OPENSSH}/dist/etc/sshd_config.d/"; ls -lhF)
    total 12K
    -rw-rw-r-- 1 jdb jdb 846 Jan 20 12:37 sshd-pi-22.conf-NO
    -rw-rw-r-- 1 jdb jdb 848 Jan 20 12:37 sshd-pi-2223.conf-NO
    -rw-rw-r-- 1 jdb jdb 848 Jan 20 12:37 sshd-pi-2224.conf-NO
    lrwxrwxrwx 1 jdb jdb  20 Jan 21 20:56 sshd-pi.conf -> sshd-pi-2223.conf-NO

# - Patch the config file, for local includes:
pi$ printf "\n\n%s\n\n"  "Include ${OPENSSH}/dist/etc/sshd_config.d/*.conf"  >> "${OPENSSH_DESTDIR}/${OPENSSH}"/dist/etc/sshd_config

# - Add the systemd control file:
pi$ mkdir -p "${OPENSSH_DESTDIR}/${OPENSSH}"/dist/tmp/
pi$ cp -p  ~/zGit.pi/pi/sshd/sshd-jdb.service  "${OPENSSH_DESTDIR}/${OPENSSH}"/dist/tmp/

# - Prep file ownership: root
pi$ sudo chown -R root:root "${OPENSSH_DESTDIR}"
pi$ sudo chmod -R g-w,u-w   "${OPENSSH_DESTDIR}"
```
<br>


## Package: tar file

```
#####################
# Package: tar file #
#####################
#
# - Only the dist/ directory is needed.

pi$ alias isodate
    alias isodate='date +%Y-%m-%d'

pi$ isodate
    2019-01-21

pi$ cd "${OPENSSH_DESTDIR}/${OPENSSH}"

pi$ ls -l
    total 4.0K
    dr-xr-xr-x 7 root root 4.0K Jan 21 20:42 dist/

pi$ ls -l dist
    total 20K
    dr-xr-xr-x 2 root root 4.0K Jan 21 20:42 bin/
    dr-xr-xr-x 3 root root 4.0K Jan 21 20:54 etc/
    dr-xr-xr-x 2 root root 4.0K Jan 21 20:42 libexec/
    dr-xr-xr-x 2 root root 4.0K Jan 21 20:42 sbin/
    dr-xr-xr-x 3 root root 4.0K Jan 21 20:42 share/


pi$ sudo tar czvf - ./dist | cat > ~/tmp/openssh2.jdb.$(isodate).tgz
    ./dist/
    ./dist/sbin/
    ./dist/sbin/sshd
    ./dist/share/
    ./dist/share/man/
    ./dist/share/man/man8/
    ./dist/share/man/man8/sftp-server.8
    ./dist/share/man/man8/ssh-sk-helper.8
    ./dist/share/man/man8/ssh-pkcs11-helper.8
    ./dist/share/man/man8/ssh-keysign.8
    ./dist/share/man/man8/sshd.8
    ./dist/share/man/man1/
    ./dist/share/man/man1/scp.1
    ./dist/share/man/man1/ssh.1
    ./dist/share/man/man1/ssh-keyscan.1
    ./dist/share/man/man1/ssh-add.1
    ./dist/share/man/man1/sftp.1
    ./dist/share/man/man1/ssh-keygen.1
    ./dist/share/man/man1/ssh-agent.1
    ./dist/share/man/man5/
    ./dist/share/man/man5/ssh_config.5
    ./dist/share/man/man5/sshd_config.5
    ./dist/share/man/man5/moduli.5
    ./dist/run/
    ./dist/etc/
    ./dist/etc/ssh_config
    ./dist/etc/sshd_config.d/
    ./dist/etc/sshd_config.d/sshd-pi-22.conf-NO
    ./dist/etc/sshd_config.d/sshd-pi.conf
    ./dist/etc/sshd_config.d/sshd-pi-2223.conf-NO
    ./dist/etc/sshd_config.d/sshd-pi-2224.conf-NO
    ./dist/etc/sshd_config
    ./dist/etc/moduli
    ./dist/libexec/
    ./dist/libexec/ssh-pkcs11-helper
    ./dist/libexec/ssh-sk-helper
    ./dist/libexec/ssh-keysign
    ./dist/libexec/sftp-server
    ./dist/bin/
    ./dist/bin/ssh-keyscan
    ./dist/bin/ssh
    ./dist/bin/ssh-add
    ./dist/bin/sftp
    ./dist/bin/scp
    ./dist/bin/ssh-keygen
    ./dist/bin/ssh-agent


pi$ ls -l ~/tmp/*.tgz
    -rw-rw-r-- 1 jdb jdb 2.2M Jan 21 23:57 /home/jdb/tmp/openssh2.jdb.2019-01-21.tgz

pi$ tar tzvf ~/tmp/openssh2.jdb.$(isodate).tgz | head
    dr-xr-xr-x root/root         0 2019-01-21 20:42 ./dist/
    dr-xr-xr-x root/root         0 2019-01-21 20:42 ./dist/sbin/
    -r-xr-xr-x root/root    834872 2019-01-21 20:42 ./dist/sbin/sshd
    dr-xr-xr-x root/root         0 2019-01-21 20:42 ./dist/share/
    dr-xr-xr-x root/root         0 2019-01-21 20:42 ./dist/share/man/
    dr-xr-xr-x root/root         0 2019-01-21 20:42 ./dist/share/man/man8/
    -r--r--r-- root/root      5096 2019-01-21 20:42 ./dist/share/man/man8/sftp-server.8
    -r--r--r-- root/root      1730 2019-01-21 20:42 ./dist/share/man/man8/ssh-sk-helper.8
    -r--r--r-- root/root      1727 2019-01-21 20:42 ./dist/share/man/man8/ssh-pkcs11-helper.8
    -r--r--r-- root/root      3265 2019-01-21 20:42 ./dist/share/man/man8/ssh-keysign.8

pi$ md5sum ~/tmp/*.tgz
    bfeb7ddbb153f3c5e1390d4b9cd1d3a0  /home/jdb/tmp/openssh2.jdb.2019-01-21.tgz


# - FYI: No host keys are distributed.
#
pi$ tar tzvf ~/tmp/openssh2.jdb.$(isodate).tgz | grep _key
pi$

pi$ tar tzvf ~/tmp/openssh2.jdb.$(isodate).tgz | grep etc/
    dr-xr-xr-x root/root         0 2019-01-21 20:54 ./dist/etc/
    -r--r--r-- root/root      1531 2019-01-21 20:42 ./dist/etc/ssh_config
    dr-xr-xr-x root/root         0 2019-01-21 20:56 ./dist/etc/sshd_config.d/
    -r--r--r-- root/root       846 2019-01-20 12:37 ./dist/etc/sshd_config.d/sshd-pi-22.conf-NO
    lrwxrwxrwx root/root         0 2019-01-21 20:56 ./dist/etc/sshd_config.d/sshd-pi.conf -> sshd-pi-2223.conf-NO
    -r--r--r-- root/root       848 2019-01-20 12:37 ./dist/etc/sshd_config.d/sshd-pi-2223.conf-NO
    -r--r--r-- root/root       848 2019-01-20 12:37 ./dist/etc/sshd_config.d/sshd-pi-2224.conf-NO
    -r--r--r-- root/root      3306 2019-01-21 20:42 ./dist/etc/sshd_config
    -r--r--r-- root/root    577834 2019-01-21 20:42 ./dist/etc/moduli
```
<br>


## Debian package

```
###########################
# Package: Debian package #
###########################
#
# - ${OPENSSH_DESTDIR} has the needed distribution files, in the needed structure.
#
# - Info:
#   https://askubuntu.com/questions/1130558/how-to-build-deb-package-for-ubuntu-18-04
#   https://stackoverflow.com/questions/130894/how-to-build-a-debian-ubuntu-package-from-source
#   https://tldp.org/HOWTO/html_single/Debian-Binary-Package-Building-HOWTO/
#
# - Basically, you need to:
#   Set up your folder structure
#   Create a control file
#   Optionally create postinst or prerm scripts
#   Run dpkg-deb
#
# - ToDo: migrate installation directory:  /usr/local/openssh-jdb/ --> /opt/openssh-jdb/
#   https://askubuntu.com/questions/1148/when-installing-user-applications-where-do-best-practices-suggest-they-be-loc
#   https://askubuntu.com/questions/6897/where-to-install-programs
#   https://lintian.debian.org/tags.html
#   https://www.debian.org/doc/debian-policy/ch-controlfields.html
#   https://www.debian.org/doc/manuals/debmake-doc/ch05.en.html#rules


# - FYI: arm64 vs. aarch64
#   https://stackoverflow.com/questions/31851611/differences-between-arm64-and-aarch64#31852003
#
pi$ uname -m
    aarch64

pi$ dpkg-architecture -L | grep arm64
    uclibc-linux-arm64
    musl-linux-arm64
    arm64ilp32
    arm64
    kfreebsd-arm64
    knetbsd-arm64
    kopensolaris-arm64
    hurd-arm64
    darwin-arm64
    dragonflybsd-arm64
    freebsd-arm64
    netbsd-arm64
    openbsd-arm64
    aix-arm64
    solaris-arm64
    uclinux-arm64

pi$ dpkg-architecture -L | grep $(uname -n)
pi$


pi$ mkdir -p ${OPENSSH_DEV}/zPackage/
pi$ cd ${OPENSSH_DEV}/


# - Undo root-ownership of files, as bundled for tar.
#
pi$ sudo chown -R jdb:jdb  ${OPENSSH_DESTDIR}/

pi$ mkdir -p ${OPENSSH_DESTDIR}/DEBIAN/
pi$ mkdir -p ${OPENSSH_DESTDIR}/etc/systemd/system/

pi$ ls -l ${OPENSSH_DESTDIR}/
    total 16K
    drwxr-xr-x 2 jdb  jdb  4.0K Jan 24 15:18 DEBIAN/
    drwxr-xr-x 2 jdb  jdb  4.0K Jan 24 15:18 etc/
    dr-xr-xr-x 3 root root 4.0K Jan 21 23:56 usr/
    dr-xr-xr-x 3 root root 4.0K Jan 21 23:56 var/

pi$ cp -p  ~/zGit.pi/pi/sshd/DEBIAN-control    ${OPENSSH_DESTDIR}/DEBIAN/control
pi$ cp -p  ~/zGit.pi/pi/sshd/sshd-jdb.service  ${OPENSSH_DESTDIR}/etc/systemd/system/


pi$ dpkg-deb --build  ${OPENSSH_DESTDIR}/  ./zPackage/sshd-jdb_1.0-1.deb
    dpkg-deb: building package 'sshd-jdb' in './zPackage/sshd-jdb_1.0-1.deb'.


pi$ ls -l zPackage/
    total 720K
    -rw-r--r-- 1 root root 717K Jan 24 15:29 sshd-jdb_1.0-1.deb

pi$ file *.deb
    sshd-jdb_1.0-1.deb: Debian binary package (format 2.0), with control.tar.xz, data compression xz


pi$ dpkg-deb -I  ./zPackage/opensshd-jdb_1.0-1.deb
     new Debian package, version 2.0.
     size 1336 bytes: control archive=372 bytes.
         207 bytes,     8 lines      control
     Package: sshd-jdb
     Version: 1.0-1
     Section: base
     Priority: optional
     Architecture: aarch64
     Depends:
     Maintainer: John_DB <john@zuri.ch>
     Description: OpenBSD Secure Shell server, patched to log failed passwords


# - Lint: Show package complaints. ToDo: Address these complaints?
#
pi$ lintian  ./zPackage/opensshd-jdb_1.0-1.deb
    lintian  ./zPackage/sshd-jdb_1.0-1.deb
    E: sshd-jdb: dir-in-usr-local usr/local/openssh2.jdb/
    E: sshd-jdb: dir-in-usr-local usr/local/openssh2.jdb/dist/
    E: sshd-jdb: dir-in-usr-local usr/local/openssh2.jdb/dist/bin/
    E: sshd-jdb: dir-in-usr-local ... use --no-tag-display-limit to see all (or pipe to a file/program)
    E: sshd-jdb: extended-description-is-empty
    E: sshd-jdb: file-in-etc-not-marked-as-conffile etc/systemd/system/sshd-jdb.service
    E: sshd-jdb: file-in-usr-local usr/local/openssh2.jdb/dist/bin/scp
    E: sshd-jdb: file-in-usr-local usr/local/openssh2.jdb/dist/bin/sftp
    E: sshd-jdb: file-in-usr-local usr/local/openssh2.jdb/dist/bin/ssh
    E: sshd-jdb: file-in-usr-local ... use --no-tag-display-limit to see all (or pipe to a file/program)
    E: sshd-jdb: no-changelog usr/share/doc/sshd-jdb/changelog.Debian.gz (non-native package)
    E: sshd-jdb: no-copyright-file
    E: sshd-jdb: non-standard-dir-in-var var/empty/
    E: sshd-jdb: systemd-service-file-outside-lib etc/systemd/system/sshd-jdb.service
    E: sshd-jdb: wrong-file-owner-uid-or-gid etc/ 501/501
    E: sshd-jdb: wrong-file-owner-uid-or-gid etc/systemd/ 501/501
    E: sshd-jdb: wrong-file-owner-uid-or-gid etc/systemd/system/ 501/501
    E: sshd-jdb: wrong-file-owner-uid-or-gid ... use --no-tag-display-limit to see all (or pipe to a file/program)
    W: sshd-jdb: empty-field Depends
    W: sshd-jdb: executable-is-not-world-readable usr/local/openssh2.jdb/dist/libexec/ssh-keysign 0511
    W: sshd-jdb: file-in-unusual-dir usr/local/openssh2.jdb/dist/bin/scp
    W: sshd-jdb: file-in-unusual-dir usr/local/openssh2.jdb/dist/bin/sftp
    W: sshd-jdb: file-in-unusual-dir usr/local/openssh2.jdb/dist/bin/ssh
    W: sshd-jdb: file-in-unusual-dir ... use --no-tag-display-limit to see all (or pipe to a file/program)
    W: sshd-jdb: missing-depends-line
    W: sshd-jdb: non-standard-dir-perm usr/ 0555 != 0755
    W: sshd-jdb: non-standard-dir-perm usr/local/ 0555 != 0755
    W: sshd-jdb: non-standard-dir-perm usr/local/openssh2.jdb/ 0555 != 0755
    W: sshd-jdb: non-standard-dir-perm ... use --no-tag-display-limit to see all (or pipe to a file/program)
    W: sshd-jdb: non-standard-executable-perm usr/local/openssh2.jdb/dist/bin/scp 0555 != 0755
    W: sshd-jdb: non-standard-executable-perm usr/local/openssh2.jdb/dist/bin/sftp 0555 != 0755
    W: sshd-jdb: non-standard-executable-perm usr/local/openssh2.jdb/dist/bin/ssh 0555 != 0755
    W: sshd-jdb: non-standard-executable-perm ... use --no-tag-display-limit to see all (or pipe to a file/program)
    W: sshd-jdb: non-standard-file-perm usr/local/openssh2.jdb/dist/etc/moduli 0444 != 0644
    W: sshd-jdb: non-standard-file-perm usr/local/openssh2.jdb/dist/etc/ssh_config 0444 != 0644
    W: sshd-jdb: non-standard-file-perm usr/local/openssh2.jdb/dist/etc/sshd_config 0444 != 0644
    W: sshd-jdb: non-standard-file-perm ... use --no-tag-display-limit to see all (or pipe to a file/program)
    W: sshd-jdb: unknown-architecture aarch64
    W: sshd-jdb: unknown-section base

pi$ dpkg-deb -c  ./zPackage/sshd-jdb_1.0-1.deb
```
<br>


## Tests

```
#########
# Tests #
#########
#
# - Prior to installation, on a few tests are valid.
#   The DESTDIR config file is explicitly specified, as the real one isn't yet installed.

pi$ cd ${OPENSSH_DEV}/openssh-8.4p1/


# - Test 1:
#
pi$ "${OPENSSH_DESTDIR}/${OPENSSH}"/dist/sbin/sshd -v
    unknown option -- v
    OpenSSH_8.4p1, OpenSSL 1.1.1f  31 Mar 2020
    usage: sshd [-46DdeiqTt] [-C connection_spec] [-c host_cert_file]
                [-E log_file] [-f config_file] [-g login_grace_time]
                [-h host_key_file] [-o option] [-p port] [-u len]


# - Test 2:
#   -> sudo is required, to read the host keys.
#
pi$ sudo "${OPENSSH_DESTDIR}/${OPENSSH}"/dist/sbin/sshd -t; echo rc: $?
    rc: 0


# - Test 3:
#
pi$ (sudo "${OPENSSH_DESTDIR}/${OPENSSH}"/dist/sbin/sshd -T  -f "${OPENSSH_DESTDIR}/${OPENSSH}"/dist/etc/sshd_config; echo rc: $?) 2>&1 | tee q4.test
    port 2223
    addressfamily any
    listenaddress [::]:2223
    listenaddress 0.0.0.0:2223
    usepam no
    logingracetime 120
    x11displayoffset 10
    maxauthtries 6
    maxsessions 10
    clientaliveinterval 1200
    clientalivecountmax 3
    streamlocalbindmask 0177
    permitrootlogin without-password
    ignorerhosts yes
    ignoreuserknownhosts no
    hostbasedauthentication no
    hostbasedusesnamefrompacketonly no
    pubkeyauthentication yes
    passwordauthentication yes
    kbdinteractiveauthentication yes
    challengeresponseauthentication yes
    printmotd yes
    printlastlog yes
    x11forwarding no
    x11uselocalhost yes
    permittty yes
    permituserrc yes
    strictmodes yes
    tcpkeepalive yes
    permitemptypasswords no
    compression yes
    gatewayports no
    usedns no
    allowtcpforwarding yes
    allowagentforwarding yes
    disableforwarding no
    allowstreamlocalforwarding yes
    streamlocalbindunlink no
    fingerprinthash SHA256
    exposeauthinfo no
    pidfile /usr/local/openssh2.jdb//dist/run/sshd.pid
    xauthlocation /usr/bin/xauth
    ciphers chacha20-poly1305@openssh.com,aes128-ctr,aes192-ctr,aes256-ctr,aes128-gcm@openssh.com,aes256-gcm@openssh.com
    macs umac-64-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com,hmac-sha1-etm@openssh.com,umac-64@openssh.com,umac-128@openssh.com,hmac-sha2-256,hmac-sha2-512,hmac-sha1
    banner none
    forcecommand none
    chrootdirectory none
    trustedusercakeys none
    revokedkeys none
    securitykeyprovider internal
    authorizedprincipalsfile none
    versionaddendum none
    authorizedkeyscommand none
    authorizedkeyscommanduser none
    authorizedprincipalscommand none
    authorizedprincipalscommanduser none
    hostkeyagent none
    kexalgorithms curve25519-sha256,curve25519-sha256@libssh.org,ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521,diffie-hellman-group-exchange-sha256,diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,diffie-hellman-group14-sha256
    casignaturealgorithms ecdsa-sha2-nistp256,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,sk-ecdsa-sha2-nistp256@openssh.com,ssh-ed25519,sk-ssh-ed25519@openssh.com,rsa-sha2-512,rsa-sha2-256
    hostbasedacceptedkeytypes ecdsa-sha2-nistp256-cert-v01@openssh.com,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp521-cert-v01@openssh.com,sk-ecdsa-sha2-nistp256-cert-v01@openssh.com,ssh-ed25519-cert-v01@openssh.com,sk-ssh-ed25519-cert-v01@openssh.com,rsa-sha2-512-cert-v01@openssh.com,rsa-sha2-256-cert-v01@openssh.com,ssh-rsa-cert-v01@openssh.com,ecdsa-sha2-nistp256,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,sk-ecdsa-sha2-nistp256@openssh.com,ssh-ed25519,sk-ssh-ed25519@openssh.com,rsa-sha2-512,rsa-sha2-256,ssh-rsa
    hostkeyalgorithms ecdsa-sha2-nistp256-cert-v01@openssh.com,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp521-cert-v01@openssh.com,sk-ecdsa-sha2-nistp256-cert-v01@openssh.com,ssh-ed25519-cert-v01@openssh.com,sk-ssh-ed25519-cert-v01@openssh.com,rsa-sha2-512-cert-v01@openssh.com,rsa-sha2-256-cert-v01@openssh.com,ssh-rsa-cert-v01@openssh.com,ecdsa-sha2-nistp256,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,sk-ecdsa-sha2-nistp256@openssh.com,ssh-ed25519,sk-ssh-ed25519@openssh.com,rsa-sha2-512,rsa-sha2-256,ssh-rsa
    pubkeyacceptedkeytypes ecdsa-sha2-nistp256-cert-v01@openssh.com,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp521-cert-v01@openssh.com,sk-ecdsa-sha2-nistp256-cert-v01@openssh.com,ssh-ed25519-cert-v01@openssh.com,sk-ssh-ed25519-cert-v01@openssh.com,rsa-sha2-512-cert-v01@openssh.com,rsa-sha2-256-cert-v01@openssh.com,ssh-rsa-cert-v01@openssh.com,ecdsa-sha2-nistp256,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,sk-ecdsa-sha2-nistp256@openssh.com,ssh-ed25519,sk-ssh-ed25519@openssh.com,rsa-sha2-512,rsa-sha2-256,ssh-rsa
    loglevel INFO
    syslogfacility AUTH
    authorizedkeysfile .ssh/authorized_keys
    hostkey /etc/ssh/ssh_host_rsa_key
    hostkey /etc/ssh/ssh_host_ecdsa_key
    hostkey /etc/ssh/ssh_host_ed25519_key
    denyusers ubuntu
    authenticationmethods any
    subsystem sftp /usr/local/openssh2.jdb//dist/libexec/sftp-server
    maxstartups 10:30:100
    permittunnel no
    ipqos af21 cs1
    rekeylimit 0 0
    permitopen any
    permitlisten any
    permituserenvironment no
    pubkeyauthoptions none
    rc: 0


pi$ grep pid q4.test
    pidfile /usr/local/sshd-jdb//dist/run//sshd.pid
```
<br>


## Local Installation and Tests

```
################################
# Local Installation and Tests #
################################
#
# - For these tests, you should be logged into the console (no ssh),
#   or ssh login with the default Ubuntu sshd listening on port 22.
#
# - Ports:
#   22:   Default sshd listener port.
#   2223: Default listener, with this SSHD build.
#   2224: Listener with this SSHD build, for tests.


pi$ alias ns='echo " -\$ netstat -an | grep -i listen | egrep ^tcp | sort"; netstat -an | grep -i listen | egrep ^tcp | sort'

pi$ ns
     -$ netstat -an | grep -i listen | egrep ^tcp | sort
    tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN
    tcp6       0      0 ::1:25                  :::*                    LISTEN
    tcp6       0      0 ::1:631                 :::*                    LISTEN
    tcp6       0      0 :::22                   :::*                    LISTEN


pi$ alias psshd='echo " -\$ ps -ef | grep sshd | grep -v grep"; ps -ef | grep sshd | grep -v grep'

pi$ psshd
     -$ ps -ef | grep sshd | grep -v grep
    root       46740       1  0 21:28 ?        00:00:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
    root       47024   46740  0 21:32 ?        00:00:00 sshd: jdb [priv]
    jdb        47094   47024  0 21:32 ?        00:00:00 sshd: jdb@pts/1
```
<br>


## Open the firewall

```
    #####################
    # Open the firewall #
    #####################
    #
    # - If not done earlier...

pi$ sudo ufw allow  22/tcp
    Rule added
    Rule added (v6)

pi$ sudo ufw allow 2223/tcp
    Rules updated
    Rules updated (v6)

pi$ sudo ufw allow 2224/tcp
    Rules updated
    Rules updated (v6)

pi$ sudo ufw status verbose
    Status: inactive

pi$  sudo ufw enable
    Command may disrupt existing ssh connections. Proceed with operation (y|n)? y
    Firewall is active and enabled on system startup


pi$ sudo ufw status verbose
    Status: active
    Logging: on (low)
    Default: deny (incoming), allow (outgoing), disabled (routed)
    New profiles: skip

    To                         Action      From
    --                         ------      ----
    2224/tcp                   ALLOW IN    Anywhere
    2223/tcp                   ALLOW IN    Anywhere
    22/tcp                     ALLOW IN    Anywhere
    2224/tcp (v6)              ALLOW IN    Anywhere (v6)
    2223/tcp (v6)              ALLOW IN    Anywhere (v6)
    22/tcp (v6)                ALLOW IN    Anywhere (v6)
```
<br>


## Deploy

```
    ##########
    # Deploy #
    ##########

pi$ OPENSSH=/usr/local/openssh2.jdb/
pi$ sudo rm -r -f ${OPENSSH}
pi$ sudo mkdir -p "${OPENSSH}"
pi$ sudo mkdir -p /var/empty/

pi$ cd "${OPENSSH}"
pi$ sudo tar xzf ~/tmp/openssh2.jdb.2019-01-21.tgz
```
<br>


## Pick a config

```
    #################
    # Pick a config #
    #################
    #
    # - Optional: Change the listener port to 2224.
    #   Without this, port 2223 is used.

pi$ cd $OPENSSH/dist/etc/sshd_config.d/

pi$ ln -s  sshd-pi-2224.conf-NO  sshd-pi-jdb.conf

pi$ ls -l
    total 8.0K
    -rw-rw-r-- 1 jdb jdb 859 Jan 19 21:07 sshd-pi-2223.conf-NO
    -rw-rw-r-- 1 jdb jdb 859 Jan 19 21:07 sshd-pi-2224.conf-NO
    lrwxrwxrwx 1 jdb jdb  20 Jan 19 21:30 sshd-pi-jdb.conf -> sshd-pi-2224.conf-NO
```
<br>


## Run

```
    #######
    # Run #
    #######
    #
    # - When compiled with PAM support, there's an extra delay between login attempts.
    # - Run with sudo, to read and use default host keys in /etc/ssh/

pi$ sudo "${OPENSSH}"/dist/sbin/sshd -D -e

Server listening on 0.0.0.0 port 2223.
Server listening on :: port 2223.


    mac$ ssh -p 2223  jdb@apple-pi.home.umi.ch  whoami
    jdb

Accepted publickey for jdb from 192.168.1.114 port 65212 ssh2: RSA SHA256:aOgf2EZO6kbTHjbIPjMHF/Dwm8+fuz8wcLZeOwLP/TA
Received disconnect from 192.168.1.114 port 65212:11: disconnected by user
Disconnected from user jdb 192.168.1.114 port 65212


    mac$ ssh -p 2223  ubuntu@apple-pi.home.umi.ch  whoami
    ubuntu@apple-pi.home.umi.ch's password:
    Permission denied, please try again.
    ubuntu@apple-pi.home.umi.ch's password:
    Permission denied, please try again.
    ubuntu@apple-pi.home.umi.ch's password:
    ubuntu@apple-pi.home.umi.ch: Permission denied (publickey,password,keyboard-interactive).

User ubuntu from 192.168.1.114 not allowed because listed in DenyUsers
Honey:  Port: 2223   IP: 192.168.1.114    Username: ubuntu          Password: 'x'
Could not get shadow information for NOUSER
Failed password for invalid user ubuntu from 192.168.1.114 port 65382 ssh2
Honey:  Port: 2223   IP: 192.168.1.114    Username: ubuntu          Password: 'y'
Failed password for invalid user ubuntu from 192.168.1.114 port 65382 ssh2
Honey:  Port: 2223   IP: 192.168.1.114    Username: ubuntu          Password: 'z'
Failed password for invalid user ubuntu from 192.168.1.114 port 65382 ssh2
Connection closed by invalid user ubuntu 192.168.1.114 port 65382 [preauth]


    mac$ ssh -p 2223 -o PubkeyAuthentication=no  jdb@apple-pi.home.umi.ch  whoami
    jdb@apple-pi.home.umi.ch's password:
    Permission denied, please try again.
    jdb@apple-pi.home.umi.ch's password:
    Permission denied, please try again.
    jdb@apple-pi.home.umi.ch's password:
    jdb@apple-pi.home.umi.ch: Permission denied (publickey,password).

Honey:  Port: 2223   IP: 192.168.1.114    Username: jdb             Password: 'x'
Failed password for jdb from 192.168.1.114 port 65401 ssh2
Honey:  Port: 2223   IP: 192.168.1.114    Username: jdb             Password: 'y'
Failed password for jdb from 192.168.1.114 port 65401 ssh2
Honey:  Port: 2223   IP: 192.168.1.114    Username: jdb             Password: 'z'
Failed password for jdb from 192.168.1.114 port 65401 ssh2
Connection closed by authenticating user jdb 192.168.1.114 port 65401 [preauth]

^C


# - FYI:
#   The new build is compatible with the Ubuntu-default sshd config.
#   -> Run this from the console, to avoid port bind conflicts.
#
pi$ sudo "${OPENSSH}"/dist/sbin/sshd -D -e -f /etc/ssh/sshd_config
    ...
```
<br>


## Daemon test

```
    ###############
    # Daemon test #
    ###############

# - Listeners: Before
#
pi$ ns
     -$ netstat -an | grep -i listen | egrep ^tcp | sort
    tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN
    tcp6       0      0 ::1:25                  :::*                    LISTEN
    tcp6       0      0 ::1:631                 :::*                    LISTEN
    tcp6       0      0 :::22                   :::*                    LISTEN


# - SSHD processes: Before
#
pi$ psshd
     -$ ps -ef | grep sshd | grep -v grep
    root       46740       1  0 21:28 ?        00:00:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
    root       47024   46740  0 21:32 ?        00:00:00 sshd: jdb [priv]
    jdb        47094   47024  0 21:32 ?        00:00:00 sshd: jdb@pts/1


# - Demonize:
#
## pi$ sudo "${OPENSSH}"/dist/sbin/sshd
## pi$
#
pi$ sudo systemctl start sshd-jdb


pi$ ns
     -$ netstat -an | grep -i listen | egrep ^tcp | sort
    tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN
    tcp        0      0 0.0.0.0:2223            0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN
    tcp6       0      0 ::1:25                  :::*                    LISTEN
    tcp6       0      0 ::1:631                 :::*                    LISTEN
    tcp6       0      0 :::22                   :::*                    LISTEN
    tcp6       0      0 :::2223                 :::*                    LISTEN

pi$ psshd
     -$ ps -ef | grep sshd | grep -v grep
    root       46740       1  0 21:28 ?        00:00:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
    root       47024   46740  0 21:32 ?        00:00:00 sshd: jdb [priv]
    jdb        47094   47024  0 21:32 ?        00:00:01 sshd: jdb@pts/1
    root       48307       1  0 22:35 ?        00:00:00 sshd: /home/jdb/src/openssh2.jdb//zRoot///usr/local/openssh2.jdb//dist/sbin/sshd [listener] 0 of 10-100 startups

pi$ cat "${OPENSSH}"/dist/run/sshd.pid
    48307

# ... Tests here ...

# - Kill the new daemon
pi$ sudo kill $(cat "${OPENSSH}"/dist/run/sshd.pid)

pi$ cat "${OPENSSH}"/dist/run/sshd.pid
    cat: /usr/local/sshd-jdb//dist/run/sshd.pid: No such file or directory


pi$ ns
     -$ netstat -an | grep -i listen | egrep ^tcp | sort
    tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN
    tcp6       0      0 ::1:25                  :::*                    LISTEN
    tcp6       0      0 ::1:631                 :::*                    LISTEN
    tcp6       0      0 :::22                   :::*                    LISTEN

pi$ psshd
     -$ ps -ef | grep sshd | grep -v grep
    root       46740       1  0 21:28 ?        00:00:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
    root       47024   46740  0 21:32 ?        00:00:00 sshd: jdb [priv]
    jdb        47094   47024  0 21:32 ?        00:00:01 sshd: jdb@pts/1
```
<br>


## Deploy & Test on cherry-pi

```
##############################
# Deploy & Test on cherry-pi #
##############################
#
# - Do this on an Internet-connected system, with active SSHD hackers.

pi_C$ date; whoami; uname -n
    Tue Jan 08 10:59:54 CET 2019
    jdb
    cherry-pi

    ########
    # Prep #
    ########

pi_C$ sudo mkdir -p /var/empty/
pi_C$ sudo chown root:root /var/empty
pi_C$ sudo chmod 755 /var/empty


pi_C$ OPENSSH=/usr/local/openssh2.jdb/

pi_C$ sudo mkdir -p "${OPENSSH}"

pi_C$ cd "${OPENSSH}"

pi_C$ sudo rm -r -f ./dist/


pi_C$ md5sum ~/tmp/openssh2.jdb.*.tgz
    bfeb7ddbb153f3c5e1390d4b9cd1d3a0  /home/jdb/tmp/openssh2.jdb.2019-01-21.tgz
...


pi_C$ sudo tar xzvf ~/tmp/sshd-jdb.jdb.2019-01-21.tgz
    ./dist/
    ./dist/sbin/
    ./dist/sbin/sshd
    ./dist/share/
    ./dist/share/man/
    ./dist/share/man/man8/
    ./dist/share/man/man8/sftp-server.8
    ./dist/share/man/man8/ssh-sk-helper.8
    ./dist/share/man/man8/ssh-pkcs11-helper.8
    ./dist/share/man/man8/ssh-keysign.8
    ./dist/share/man/man8/sshd.8
    ./dist/share/man/man1/
    ./dist/share/man/man1/scp.1
    ./dist/share/man/man1/ssh.1
    ./dist/share/man/man1/ssh-keyscan.1
    ./dist/share/man/man1/ssh-add.1
    ./dist/share/man/man1/sftp.1
    ./dist/share/man/man1/ssh-keygen.1
    ./dist/share/man/man1/ssh-agent.1
    ./dist/share/man/man5/
    ./dist/share/man/man5/ssh_config.5
    ./dist/share/man/man5/sshd_config.5
    ./dist/share/man/man5/moduli.5
    ./dist/run/
    ./dist/etc/
    ./dist/etc/ssh_host_rsa_key
    ./dist/etc/ssh_host_ecdsa_key
    ./dist/etc/ssh_host_ed25519_key.pub
    ./dist/etc/ssh_config
    ./dist/etc/ssh_host_rsa_key.pub
    ./dist/etc/ssh_host_ed25519_key
    ./dist/etc/ssh_host_dsa_key.pub
    ./dist/etc/ssh_host_dsa_key
    ./dist/etc/ssh_host_ecdsa_key.pub
    ./dist/etc/sshd_config.d/
    ./dist/etc/sshd_config.d/sshd-pi-jdb.conf
    ./dist/etc/sshd_config
    ./dist/etc/moduli
    ./dist/libexec/
    ./dist/libexec/ssh-pkcs11-helper
    ./dist/libexec/ssh-sk-helper
    ./dist/libexec/ssh-keysign
    ./dist/libexec/sftp-server
    ./dist/bin/
    ./dist/bin/ssh-keyscan
    ./dist/bin/ssh
    ./dist/bin/ssh-add
    ./dist/bin/sftp
    ./dist/bin/scp
    ./dist/bin/ssh-keygen
    ./dist/bin/ssh-agent


pi_C$ sudo "${OPENSSH}"/dist/sbin/sshd -D -e
    Server listening on 0.0.0.0 port 2223.
    Server listening on :: port 2223.
    ^C


## # - All files in ./dist/ should be owned by root:
## #   Also:
## #   pi$ sudo chmod -R g-w ./dist/
## #
## pi_C$ ls -lhF dist/
## ...


    #################
    # Pick a config #
    #################
    #
    # - In this example, we start a new SSHD listener on port 2224.
    # - No other SSHD instances are running.

pi_C$ cd ${OPENSSH}/dist/etc/sshd_config.d/

pi_C$ sudo ln -s  sshd-pi-2224.conf-NO  sshd-pi-2224.conf


    ####################
    # Activate systemd #
    ####################
    #
    # - systemd service name: sshd-jdb
    # - Do this from an ssh login on port 22
    #
    # - Build notes:
    #   https://unix.stackexchange.com/questions/313080/infinite-activating-state-for-custom-build-openssh-hpn-sshd-on-ubuntu-16
    #   https://www.issihosts.com/haveged/faq.html
    #   https://superuser.com/questions/1274901/systemd-forking-vs-simple
    #
    #   https://www.freedesktop.org/software/systemd/man/systemd.service.html
    #   https://www.freedesktop.org/software/systemd/man/systemd.unit.html
    #   https://www.freedesktop.org/software/systemd/man/systemd.kill.html

pi_C$ ns
     -$ netstat -an | grep -i listen | egrep ^tcp | sort
    tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN
    tcp6       0      0 ::1:25                  :::*                    LISTEN
    tcp6       0      0 ::1:631                 :::*                    LISTEN
    tcp6       0      0 :::22                   :::*                    LISTEN
    tcp6       0      0 :::443                  :::*                    LISTEN
    tcp6       0      0 :::80                   :::*                    LISTEN


pi_C$ sudo cp -p  "${OPENSSH}"/dist/tmp/sshd-jdb.service   /etc/systemd/system/
pi_C$ sudo chmod 644  /etc/systemd/system/sshd-jdb.service
pi_C$ sudo systemctl daemon-reload

pi_C$ sudo systemctl status sshd-jdb
    ● sshd-jdb.service - OpenBSD Secure Shell server, patched to log failed passwords
         Loaded: loaded (/etc/systemd/system/sshd-jdb.service; disabled; vendor preset: enabled)
         Active: inactive (dead)
           Docs: man:sshd(8)
                 man:sshd_config(5)

# - Not needed:
## pi_C$ sudo systemctl enable sshd-jdb

pi_C$ sudo systemctl start sshd-jdb

pi_C$ sudo systemctl status sshd-jdb
    ● sshd-jdb.service - OpenBSD Secure Shell server, patched to log failed passwords
         Loaded: loaded (/etc/systemd/system/sshd-jdb.service; enabled; vendor preset: enabled)
         Active: active (running) since Fri 2019-01-22 00:20:36 CET; 4s ago
           Docs: man:sshd(8)
                 man:sshd_config(5)
        Process: 21466 ExecStartPre=/usr/local/openssh2.jdb/dist/sbin/sshd -t (code=exited, status=0/SUCCESS)
       Main PID: 21467 (sshd)
          Tasks: 1 (limit: 9244)
         CGroup: /system.slice/sshd-jdb.service
                 └─21467 sshd: /usr/local/openssh2.jdb/dist/sbin/sshd -D [listener] 0 of 10-100 startups

    Jan 22 00:20:36 cherry-pi systemd[1]: Starting OpenBSD Secure Shell server, patched to log failed passwords...
    Jan 22 00:20:36 cherry-pi systemd[1]: Started OpenBSD Secure Shell server, patched to log failed passwords.
    Jan 22 00:20:36 cherry-pi sshd[21467]: Server listening on 0.0.0.0 port 2224.
    Jan 22 00:20:36 cherry-pi sshd[21467]: Server listening on :: port 2224.


     -$ netstat -an | grep -i listen | egrep ^tcp | sort
    tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN
    tcp        0      0 0.0.0.0:2224            0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN
    tcp6       0      0 ::1:25                  :::*                    LISTEN
    tcp6       0      0 ::1:631                 :::*                    LISTEN
    tcp6       0      0 :::22                   :::*                    LISTEN
    tcp6       0      0 :::2224                 :::*                    LISTEN
    tcp6       0      0 :::443                  :::*                    LISTEN
    tcp6       0      0 :::80                   :::*                    LISTEN


pi_C$ sudo systemctl stop sshd-jdb

pi_C$ sudo systemctl status sshd-jdb
    ● sshd-jdb.service - OpenBSD Secure Shell server, patched to log failed passwords
         Loaded: loaded (/etc/systemd/system/sshd-jdb.service; enabled; vendor preset: enabled)
         Active: inactive (dead)
           Docs: man:sshd(8)
                 man:sshd_config(5)

    Jan 22 00:21:33 cherry-pi systemd[1]: sshd-jdb.service: Succeeded.
    Jan 22 00:21:33 cherry-pi systemd[1]: Stopped OpenBSD Secure Shell server, patched to log failed passwords.
    Jan 22 00:21:37 cherry-pi systemd[1]: Starting OpenBSD Secure Shell server, patched to log failed passwords...
    Jan 22 00:21:37 cherry-pi systemd[1]: Started OpenBSD Secure Shell server, patched to log failed passwords.
    Jan 22 00:21:37 cherry-pi sshd[21512]: Server listening on 0.0.0.0 port 2224.
    Jan 22 00:21:37 cherry-pi sshd[21512]: Server listening on :: port 2224.
    Jan 22 00:21:39 cherry-pi sshd[21512]: Received signal 15; terminating.
    Jan 22 00:21:39 cherry-pi systemd[1]: Stopping OpenBSD Secure Shell server, patched to log failed passwords...
    Jan 22 00:21:39 cherry-pi systemd[1]: sshd-jdb.service: Succeeded.
    Jan 22 00:21:39 cherry-pi systemd[1]: Stopped OpenBSD Secure Shell server, patched to log failed passwords.


    ########
    # Test #
    ########

# - Carefully try a "Wrong password" login.
#   -> Be sure fail2ban exempts this IP address, else you'll need a console login to fix sshd.

    mac$ ssh -p 2224 ubuntu@cherry-pi.umi.ch whoami
    ubuntu@cherry-pi.umi.ch's password:
    Permission denied, please try again.
    ubuntu@cherry-pi.umi.ch's password:
    Permission denied, please try again.
    ubuntu@cherry-pi.umi.ch's password:
    ubuntu@cherry-pi.umi.ch: Permission denied (publickey,password,keyboard-interactive).

pi_C$ sudo tail -100 /var/log/auth.log | egrep 'Failed password|Honey:'
    ...
    Jan 12 11:11:42 cherry-pi sshd[117012]: Honey: Username: ubuntu Password: Bad Password
    Jan 12 11:11:42 cherry-pi sshd[117012]: Honey: Username: ubuntu Password: Bad Password
    Jan 12 11:11:42 cherry-pi sshd[117012]: Failed password for invalid user ubuntu from 85.7.122.95 port 52065 ssh2
    Jan 12 11:11:44 cherry-pi sshd[117012]: Honey: Username: ubuntu Password: Also Bad
    Jan 12 11:11:44 cherry-pi sshd[117012]: Honey: Username: ubuntu Password: Also Bad
    Jan 12 11:11:44 cherry-pi sshd[117012]: Failed password for invalid user ubuntu from 85.7.122.95 port 52065 ssh2
    Jan 12 11:11:47 cherry-pi sshd[117012]: Honey: Username: ubuntu Password: Still Bad
    Jan 12 11:11:47 cherry-pi sshd[117012]: Honey: Username: ubuntu Password: Still Bad
    Jan 12 11:11:47 cherry-pi sshd[117012]: Failed password for invalid user ubuntu from 85.7.122.95 port 52065 ssh2


    #########
    # Final #
    #########
    #
    # - Configure:  after reboot, sshd listens only on port 2224.

# - Now, stop the main daemon listener on port 2223, and wait for hackers to find port 2224.

pi_C$ sudo ufw status verbose
    Status: active
    Logging: on (low)
    Default: deny (incoming), allow (outgoing), disabled (routed)
    New profiles: skip

    To                         Action      From
    --                         ------      ----
    22/tcp                     ALLOW IN    Anywhere
    80/tcp                     ALLOW IN    Anywhere
    443/tcp                    ALLOW IN    Anywhere
    2223/tcp                   ALLOW IN    Anywhere
    2224/tcp                   ALLOW IN    Anywhere
    22/tcp (v6)                ALLOW IN    Anywhere (v6)
    80/tcp (v6)                ALLOW IN    Anywhere (v6)
    443/tcp (v6)               ALLOW IN    Anywhere (v6)
    2223/tcp (v6)              ALLOW IN    Anywhere (v6)
    2224/tcp (v6)              ALLOW IN    Anywhere (v6)


pi_C$ sudo systemctl stop sshd
pi_C$ sudo systemctl disable sshd
    Removed /etc/systemd/system/multi-user.target.wants/ssh.service.
    Removed /etc/systemd/system/sshd.service.


pi_C$ sudo systemctl status sshd-jdb
    ● sshd-jdb.service - OpenBSD Secure Shell server, patched to log failed passwords
         Loaded: loaded (/etc/systemd/system/sshd-jdb.service; enabled; vendor preset: enabled)
         Active: active (running) since Fri 2019-01-22 00:24:26 CET; 3min 1s ago
           Docs: man:sshd(8)
                 man:sshd_config(5)
       Main PID: 21559 (sshd)
          Tasks: 7 (limit: 9244)
         CGroup: /system.slice/sshd-jdb.service
                 ├─21559 sshd: /usr/local/openssh2.jdb/dist/sbin/sshd -D [listener] 0 of 10-100 startups
                 ├─21602 sshd: jdb [priv]
                 ├─21605 sshd: jdb@pts/1
                 ├─21606 -bash
                 ├─21670 sudo systemctl status sshd-jdb
                 ├─21671 systemctl status sshd-jdb
                 └─21672 pager

    Jan 22 00:24:26 cherry-pi sshd[21559]: Server listening on :: port 2224.
    Jan 22 00:26:40 cherry-pi sshd[21602]: Accepted publickey for jdb from 178.196.100.90 port 52307 ssh2: RSA SHA256:aOgf2EZO6kbTHjbIPjMHF/Dwm8+fuz8wcLZeOwLP/TA
    Jan 22 00:26:54 cherry-pi sudo[21618]:      jdb : TTY=pts/1 ; PWD=/home/jdb ; USER=root ; COMMAND=/usr/bin/systemctl stop sshd
    Jan 22 00:26:54 cherry-pi sudo[21618]: pam_unix(sudo:session): session opened for user root by jdb(uid=0)
    Jan 22 00:26:54 cherry-pi sudo[21618]: pam_unix(sudo:session): session closed for user root
    Jan 22 00:26:59 cherry-pi sudo[21621]:      jdb : TTY=pts/1 ; PWD=/home/jdb ; USER=root ; COMMAND=/usr/bin/systemctl disable sshd
    Jan 22 00:26:59 cherry-pi sudo[21621]: pam_unix(sudo:session): session opened for user root by jdb(uid=0)
    Jan 22 00:27:01 cherry-pi sudo[21621]: pam_unix(sudo:session): session closed for user root
    Jan 22 00:27:27 cherry-pi sudo[21670]:      jdb : TTY=pts/1 ; PWD=/home/jdb ; USER=root ; COMMAND=/usr/bin/systemctl status sshd-jdb
    Jan 22 00:27:27 cherry-pi sudo[21670]: pam_unix(sudo:session): session opened for user root by jdb(uid=0)


pi_C$ ns
     -$ netstat -an | grep -i listen | egrep ^tcp | sort
    tcp        0      0 0.0.0.0:2224            0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN
    tcp6       0      0 ::1:25                  :::*                    LISTEN
    tcp6       0      0 ::1:631                 :::*                    LISTEN
    tcp6       0      0 :::2224                 :::*                    LISTEN
    tcp6       0      0 :::443                  :::*                    LISTEN
    tcp6       0      0 :::80                   :::*                    LISTEN


# ... reboot ...

pi_C$ ns
     -$ netstat -an | grep -i listen | egrep ^tcp | sort
    tcp        0      0 0.0.0.0:2224            0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN
    tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN
    tcp6       0      0 ::1:25                  :::*                    LISTEN
    tcp6       0      0 ::1:631                 :::*                    LISTEN
    tcp6       0      0 :::2224                 :::*                    LISTEN
    tcp6       0      0 :::443                  :::*                    LISTEN
    tcp6       0      0 :::80                   :::*                    LISTEN
```
<br>


## License
Copyright &copy; 2019, Mountain Informatik GmbH<br>
All rights reserved.<br>
Permission to share is granted under the terms of the **Server Side Public License**<br>
https://www.mongodb.com/licensing/server-side-public-license
<br>
