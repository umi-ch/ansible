# Minecraft Deployment

Here's a summary how I deploy Minecraft server on **John's Raspberry PI ecosystem**:<br>
Raspberry-PI 4, **power setup** : 8 Gb RAM, large USB SSD disk(s).
<br>


## Components

- Standard UMI Ansible deployment.<br/>
- Application owner: The ```ubuntu``` user account.
- Directory &nbsp;  **/usr/local/minecraft/**  &nbsp; is relocated into  &nbsp; **/zfs/**<br/>
- Distribution source: a backup tar file after (simple) manual installation
- Systemd scripts

Initial (manual) installation of Minecraft:

- https://www.minecraft.net/en-us/download/server
- https://minecraft.fandom.com/wiki/Tutorials/Setting_up_a_server
<br>


## Security

In my setup, only port **2223** (sshd) is exposed to the Internet.<br>
**John's Agile Raspberry PI** ecosystem has a strong security setup, described elsewhere.<br>

At this time, Minecraft port **25565** is exposed only on my home network, with WiFi use.<br>
I'm investigating wider authentication mechanisms for **Internet use**, eg:<br>

- https://wiki.vg/Authentication
- https://github.com/Mc-Auth-com

The Minecraft server (Java) is assumed to be secure, and I keep it up-to-date.<br>
In case of Hacker/Malware breaches due to vendor vulnerabilities, the PI server<br>
is of limited value; it stores no credentials or confidential material.<br>

I can fully reinstall it at anytime, with **Ansible** and offline backups.<br>
So **ransomeware outages** are of little concern.<br>

The main risk is **hijacked use** as part of a **bot net**.<br>
To mitigate this risk, I have external monitoring software, described elsewhere.<br>
<br>


## Quick Info

From a Bash shell:

```
pi_A$ i_minecraft

    #############
    # Minecraft #
    #############
    - v_bashrc_211: 6.2.1

Software:
    https://www.minecraft.net/en-us/download

To install the systemd controller with Ansible:
    pi_A$ chown -R ubuntu:ubuntu /usr/local/minecraft
    mac$  ape 04_u apple-pi

Operations:
    - Minecraft auto-starts on reboot.
    - Shutdown & maintenance needed only for upgrades.

    pi_A$ sudo -u ubuntu screen -ls

    pi_A$ (cd /usr/local/minecraft/; ls -lhF minecraft_server.jar)
        ubuntu ubuntu 25 Nov 13 21:09 minecraft_server.jar -> minecraft_server_1.19.jar

    pi_A$ systemctl status minecraft
    pi_A$ ps -ef | egrep 'java|jdk|minecraft|screen' | egrep -v 'man|grep'

    pi_A$ ls -lhF /usr/local/minecraft/logs/
    pi_A$ tail /usr/local/minecraft/logs/latest.log

    pi_A$ ns | egrep '25565|netstat'
        -$ netstat -an | grep -i listen | egrep ^tcp | sort
        tcp6   0   0 :::25565   :::*   LISTEN

Server version:
    pi_A$ grep -i version  /usr/local/minecraft/logs/latest.log
    [16:24:10] [Server thread/INFO]: Starting minecraft server version 1.19
    [11:38:56] [Server thread/INFO]: Starting minecraft server version 1.19

Server control:
    https://github.com/cwpearson/systemd-minecraft
    - to reattach:  $ sudo -u ubuntu screen -rS mc
    - to detatch:   ctrl-a d

pi_A$ i_minecraft
    #############
    # Minecraft #
    #############
    - v_bashrc_240: 6.2.7

To install the systemd controller with Ansible:
    pi_A$ chown -R ubuntu:ubuntu /usr/local/minecraft
    mac$  ape 04_u apple-pi

Operations:
    - Minecraft auto-starts on reboot.
    - Shutdown & maintenance needed only for upgrades.

    pi_A$ sudo -u ubuntu screen -ls

    pi_A$ (cd /usr/local/minecraft/; ls -lhF minecraft_server.jar)
        ubuntu ubuntu 25 Nov 13 21:09 minecraft_server.jar -> minecraft_server_1.19.jar

    pi_A$ systemctl status minecraft
    pi_A$ ps -ef | egrep 'java|jdk|minecraft|screen' | egrep -v 'man|grep'

    pi_A$ ls -lhF /usr/local/minecraft/logs/
    pi_A$ tail /usr/local/minecraft/logs/latest.log

    pi_A$ ns | egrep '25565|netstat'
        -$ netstat -an | grep -i listen | egrep ^tcp | sort
        tcp6   0   0 :::25565   :::*   LISTEN

More info here:
    https://github.com/cwpearson/systemd-minecraft
    - to reattach:  $ sudo -u ubuntu screen -rS mc
    - to detatch:   ctrl-a d
```
<br>


## Deploy SystemD control with Ansible

- https://github.com/cwpearson/systemd-minecraft

```
# - FYI: 'ape' = "Ansible Playbooks, with Environment variables"
#   It's my Bashrc function wrapper for ansible-playbook with extra conviences,
#   such as nicer output formatting, robust operations, and agile use-cases.

mac$ ape 04_u apple-pi
    -# 04_u.install-minecraft.yml
    -$ export APE_HOSTS='apple-pi'        # - Playbook host list.
    -$ ansible-playbook   '/Users/jdb/zGit/Johns-ansible/playbooks/04_u.install-minecraft.yml'
    ...

PLAY [localhost] ***************************************************************
    TASK [Gathering Facts] *********************************************************
    TASK [01. Timestamp] ***********************************************************
    ok: [localhost] => {
        msg": "2019-07-05 22:02:34 CEST

PLAY [apple-pi] ****************************************************************
    TASK [Gathering Facts] *********************************************************
    TASK [10. Sanity check 1] ******************************************************
    TASK [10. Diagnostics: Big output] *********************************************
    TASK [10. Failed?] *************************************************************
    TASK [11. Sanity check 2a: /usr/local/minecraft exists] ************************
    TASK [debug] *******************************************************************
    TASK [debug] *******************************************************************
    TASK [11. Failed?] *************************************************************
    TASK [22. sudo install -m 644  ~/zGit/ansible/playbooks/files/minecraft/minecraft.service  /etc/systemd/system/] ***
    TASK [debug] *******************************************************************
    TASK [24. Restart syslogd] *****************************************************
    TASK [debug] *******************************************************************
    TASK [25. systemd reread configs] **********************************************
    TASK [debug] *******************************************************************
    TASK [26. Activate minecraft.service] ******************************************
    TASK [debug] *******************************************************************

    TASK [29. tail /usr/local/minecraft/logs/latest.log] ***************************
    TASK [debug] *******************************************************************
    ok: [apple-pi] => {
        "stdout_lines": [
         $ ps -ef | egrep 'java|jdk|minecraft|screen' | egrep -v 'man|grep'
            ubuntu     20720       1  0 22:03 ?        00:00:00 /usr/bin/SCREEN -dmS mc /usr/bin/java -Xmx4096M -jar minecraft_server.jar nogui
            ubuntu     20721   20720 99 22:03 pts/0    00:00:13 /usr/bin/java -Xmx4096M -jar minecraft_server.jar nogui

         $ systemctl status minecraft.service
            ● minecraft.service - Minecraft Server
                 Loaded: loaded (/etc/systemd/system/minecraft.service; enabled; vendor preset: enabled)
                 Active: active (running) since Sun 2019-07-05 22:03:44 CEST; 7s ago
               Main PID: 20720 (screen)
                  Tasks: 25 (limit: 9226)
                 Memory: 129.2M
                    CPU: 14.496s
                 CGroup: /system.slice/minecraft.service
                         ├─20720 /usr/bin/SCREEN -dmS mc /usr/bin/java -Xmx4096M -jar minecraft_server.jar nogui
                         └─20721 /usr/bin/java -Xmx4096M -jar minecraft_server.jar nogui

            Jul 05 22:03:44 apple-pi systemd[1]: Starting Minecraft Server...
            Jul 05 22:03:44 apple-pi systemd[1]: Started Minecraft Server.

         $ ls -lhF /usr/local/minecraft/logs/ | tail
            -rw-rw-r-- 1 ubuntu ubuntu  273 Jul  1 17:26 2019-06-14-1.log.gz
            -rw-r--r-- 1 ubuntu ubuntu  947 Jul  5 14:59 2019-07-01-1.log.gz
            -rw-r--r-- 1 ubuntu ubuntu  975 Jul  5 15:05 2019-07-05-1.log.gz
            -rw-r--r-- 1 ubuntu ubuntu  947 Jul  5 15:07 2019-07-05-2.log.gz
            -rw-r--r-- 1 ubuntu ubuntu  938 Jul  5 15:16 2019-07-05-3.log.gz
            -rw-r--r-- 1 ubuntu ubuntu 1009 Jul  5 15:25 2019-07-05-4.log.gz
            -rw-r--r-- 1 ubuntu ubuntu  975 Jul  5 19:01 2019-07-05-5.log.gz
            -rw-r--r-- 1 ubuntu ubuntu 4.7K Jul  5 22:02 2019-07-05-6.log.gz
            -rw-r--r-- 1 ubuntu ubuntu  858 Jul  5 22:03 2019-07-05-7.log.gz
            -rw-r--r-- 1 ubuntu ubuntu    0 Jul  5 22:03 latest.log

         $ tail /usr/local/minecraft/logs/latest.log
            -- done --

         $ sudo -u ubuntu screen -ls
            There is a screen on:
                    20720.mc        (07/05/19 22:03:43)     (Detached)
            1 Socket in /run/screen/S-ubuntu.

         $ # More info here:
            https://github.com/cwpearson/systemd-minecraft
            - to reattach:  $ sudo -u ubuntu screen -rS mc
            - to detatch:   ctrl-a d

PLAY RECAP *********************************************************************
    apple-pi                   : ok=9    changed=2    unreachable=0    failed=0    skipped=9    rescued=0    ignored=0
    localhost                  : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```
<br>


## License
Copyright &copy; 2019, Mountain Informatik GmbH<br>
All rights reserved.<br>
Permission to share is granted under the terms of the **Server Side Public License**<br>
https://www.mongodb.com/licensing/server-side-public-license
<br>
