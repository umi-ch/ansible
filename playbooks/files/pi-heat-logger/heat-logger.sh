#!/bin/bash

sym_ver='2.0.1'

# JDB, 11. Nov 2022
# - Remove sleep loop, use SystemD timers.
# - /usr/local/bin/heat-logger.sh :
#   Simple looper script, run at system start, writing PI temperatures to syslog.
#   Ubuntu server assumed.
#   https://www.pragmaticlinux.com/2020/06/check-the-raspberry-pi-cpu-temperature/
# - No script output expected. STDOUT & STDERR messages are probably lost.
# - Test:
#   pi$ logger -p local3.info -t PI "[heat] John says test"

sleep=60

facility=local3
level=info
tag='PI'
brackets='[heat]'


# - Use the Unicode symbol for "degrees Celsius"
#   https://www.fileformat.info/info/unicode/char/2103/index.htm
#
#   pi$  perl -e 'use utf8; binmode STDOUT, ":utf8";  print "24.2 \x{2103}\n"'
#   24.2 ℃
#   pi$ printf '24.2 \u2103\n'
#   24.2 ℃

function hot_f() {
    local  deg=$(cat -v /sys/class/thermal/thermal_zone0/temp)
    local deg2=$(echo 2k ${deg} 1000 /p | /usr/bin/dc)
    local  msg=$(printf "%s \u2103" ${deg2})
    logger  -p ${facility}.${level}  -t "${tag}"  "${brackets}"  "${msg}"
}


hot_f


