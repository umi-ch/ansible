# Raspberry PI Temperature Logger

Here's a simple system to record PI temperature via Syslog, at one-minute intervals.<br/>

**Assumptions**
- You have installed a recent version of Ubuntu server.<br/>
- Temperatures consistently > 50 degrees Celcius are cause for concern, in my opinion.<br/>

&nbsp;

## Components

- logger script
- rsyslog config
- systemd scripts

&nbsp;

## Quick Check

From a Bash shell:

```
pi$ alias hot='cat -v /sys/class/thermal/thermal_zone0/temp | perl -ne '\''printf "%.2f degrees Celsius\n", ($_/1000)'\'' '

pi$ hot
23.85 degrees Celsius
```

&nbsp;

## Ansible playbook

I have an **Ansible playbook** to deploy this script system.<br>
For clarity, I show the "unwrapped" manual deployment steps here.<br>

- https://gitlab.com/John_DB/Johns-ansible-public/-/blob/main/playbooks/04_j.install-heat-logger.yml


&nbsp;

## Prep

I prefer to run my systems in the local timezone, not UTC.<br/>
This ensures that syslog messages appear in local time.

```
# - Default:
pi$ cat /etc/timezone
Etc/UTC

# - Update:
pi$ echo 'Europe/Zurich' | sudo tee /etc/timezone
Europe/Zurich

pi$ sudo systemctl restart rsyslog
pi$ sudo dpkg-reconfigure tzdata
pi$ sudo reboot   # - For good measure

# - Prerequisites:
#   - /usr/bin/dc (calculator)
#
pi$ sudo apt install -y dc
```

&nbsp;

## Install scripts

```
pi$ sudo install -m 755  ./heat-logger.sh  /usr/local/bin/
pi$ sudo install -m 644  ./10-PI.conf  /etc/rsyslog.d/
```

&nbsp;
## Deploy syslog


```
# - Check config:
#
pi$ rsyslogd -N1
rsyslogd: version 8.2006.0, config validation run (level 1), master config /etc/rsyslog.conf
rsyslogd: End of config validation run. Bye.

# - If OK, restart rsyslog:
#
pi$ sudo systemctl status rsyslog | grep Active:
     Active: active (running) since Sat 2022-11-08 18:35:22 CET; 15h ago

pi$ sudo systemctl restart rsyslog;  sleep 2

pi$ sudo systemctl status rsyslog | grep Active:
     Active: active (running) since Sun 2022-11-09 09:37:37 CET; 4s ago
```

&nbsp;
## Confirm script & syslog
```
# - Run the script manually.

pi$ ./heat-logger.sh
pi$

pi$ tail /var/log/pi.log
    Jan 28 17:50:04 pecan PI: [heat] 26.28 ℃
```

&nbsp;
## Deploy SystemD

```
pi$ sudo install -m 644  ./jdb-pi-heat-logger.timer    /etc/systemd/system/
pi$ sudo install -m 644  ./jdb-pi-heat-logger.service  /etc/systemd/system/

# - Confirm correctness:
pi$ sudo systemd-analyze verify /etc/systemd/system/jdb-pi-heat-logger.*

# - In another login session: Tail the syslogd journal.
pi$ journalctl -f | grep jdb-pi-heat-logger
    ...
```

&nbsp;
## Start SystemD services

```
pi$ sudo systemctl daemon-reload    # - only if relaoding

pi$ sudo systemctl start  jdb-pi-heat-logger.timer
pi$ sudo systemctl enable jdb-pi-heat-logger.timer
pi$ sudo systemctl enable jdb-pi-heat-logger.service

# - You can run an extra report manually.
#   The systemctl command returns right away, but processing still occurs in the background.
#
pi$ sudo systemctl start  jdb-pi-heat-logger.service

# - Status:

pi$ sudo systemctl status jdb-pi-heat-logger.timer
    ...

pi$ sudo systemctl status jdb-pi-heat-logger.service
    ...
```

&nbsp;
## Confirm

```
pi$ tail -f /var/log/pi.log
    Mar 24 18:31:21 cherry-pi PI: [heat] 22.87 ℃
    Mar 24 18:32:21 cherry-pi PI: [heat] 24.33 ℃
    Mar 24 18:33:21 cherry-pi PI: [heat] 22.87 ℃
    Mar 24 18:34:21 cherry-pi PI: [heat] 22.87 ℃
    Mar 24 18:35:21 cherry-pi PI: [heat] 22.87 ℃
    Mar 24 18:36:21 cherry-pi PI: [heat] 22.87 ℃
    Mar 24 18:37:21 cherry-pi PI: [heat] 23.84 ℃
    Mar 24 18:38:21 cherry-pi PI: [heat] 23.36 ℃
    Mar 24 18:39:21 cherry-pi PI: [heat] 23.36 ℃
    Mar 24 18:40:21 cherry-pi PI: [heat] 23.36 ℃
    ...
```
<br>


## License
Copyright &copy; 2019, Mountain Informatik GmbH<br>
All rights reserved.<br>
Permission to share is granted under the terms of the **Server Side Public License**<br>
https://www.mongodb.com/licensing/server-side-public-license
<br>

