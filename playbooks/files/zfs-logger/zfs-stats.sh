#!/bin/bash

sym_ver='1.1.1'

# JDB, 11. Nov 2022
# - /usr/local/bin/zfs-stats.sh :
#   Simple looper script, to report ZFS stats.
#
# - Test:
#   pi$ logger -p local3.info -t ZFS "[iostat] John says test"


facility=local3
level=info
tag='ZFS'
brackets='[iostat]'


function f_test_data() {
    local msg_test

    # - FYi:
    #   pi_E$ zpool iostat 5 5

    msg_test=$(cat <<END
              capacity     operations     bandwidth
pool        alloc   free   read  write   read  write
----------  -----  -----  -----  -----  -----  -----
t5_pool      451G  1.37T      3     24   401K   242K
t5_pool      451G  1.37T      0     25  25.6K   100K
t5_pool      451G  1.37T      0     25      0   112K
t5_pool      451G  1.37T      0     31  25.6K   126K
t5_pool      451G  1.37T      0     27      0  98.7K
END
    )

    echo "${msg_test}"
    return 0
}



# - Gather stats.

function f_zfs_stats() {
    local i=0  cmd=''  out=''  msg=''  line=''
    local rc_1=0


    # - Get data:
    #
    cmd="zpool iostat 5 5"

    ## msg="$(f_test_data)"
    msg=$(${cmd}  2>&1)
    rc_1=$?

    if [[ $rc_1 -ne 0 ]]; then
        # - Only log this with error conditions.
        msg=$(printf "\n$ %s\n%s\n%s\n" "${cmd}" "${msg}" "rc: $rc_1")
    else
        msg=$(printf "\n$ %s\n%s\n" "${cmd}" "${msg}")
    fi


    # - Show data:
    #
    ## echo "msg: '${msg}'"

    i=0
    echo "${msg}" | while read line; do
        ## echo "line: '${line}'"
        if [[ "${line}" =~ "\$ " ]]; then
            true
        elif [[ "${line}" =~ _pool ]]; then
            (( i++ ))
            line=$(printf "%1d. %s" $i "${line}")
        else
            ## line=$(echo "${line}" | sed -e 's/^-/=/;')   # - logger will not accept input with '--'
            line="   ${line}"
        fi
        logger  -p ${facility}.${level}  -t "${tag}"  "${brackets}"  "${line}"
    done
}


# - Use SystemD timers instead of a sleep-loop here.

f_zfs_stats


