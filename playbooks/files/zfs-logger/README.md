# Crypto stats

Here's a simple system to record periodic ZFS stats via Syslog.<br/>

**Assumptions**
- ZFS is used -- already installed, configured, and working.<br/>

&nbsp;

## Components

- stat scripts
- rsyslog config
- systemd scripts

&nbsp;

## Quick Check

From a Bash shell:

```
pi_E$ zpool iostat 5 5
                    capacity     operations     bandwidth
      pool        alloc   free   read  write   read  write
      ----------  -----  -----  -----  -----  -----  -----
    1. t5_pool      451G  1.37T      3     24   401K   242K
    2. t5_pool      451G  1.37T      0     25  25.6K   100K
    3. t5_pool      451G  1.37T      0     25      0   112K
    4. t5_pool      451G  1.37T      0     31  25.6K   126K
    5. t5_pool      451G  1.37T      0     27      0  98.7K
```


&nbsp;

## Ansible playbook

I have an **Ansible playbook** to deploy this script system.<br>
For clarity, I show the "unwrapped" manual deployment steps here.<br>

- https://gitlab.com/John_DB/Johns-ansible-public/-/blob/main/playbooks/04_l.install-zfs-stats.yml


&nbsp;

## Prep

I prefer to run my systems in the local timezone, not UTC.<br/>
This ensures that syslog messages appear in local time.

```
# - Default:
pi_E$ cat /etc/timezone
    Etc/UTC

# - Update:
pi_E$ echo 'Europe/Zurich' | sudo tee /etc/timezone
    Europe/Zurich

pi_E$ sudo dpkg-reconfigure tzdata
pi_E$ sudo systemctl restart rsyslog
pi_E$ sudo reboot   # - For good measure
```

&nbsp;

## Install scripts

```
pi_E$ sudo install -m 755  ./zfs-stats.sh        /usr/local/bin/
pi_E$ sudo install -m 644  ./14-zfs-stats.conf   /etc/rsyslog.d/
```

&nbsp;
## Deploy syslog

```
# - Check config:
#
pi_E$ rsyslogd -N1
    rsyslogd: version 8.2006.0, config validation run (level 1), master config /etc/rsyslog.conf
    rsyslogd: End of config validation run. Bye.

# - If OK, restart rsyslog:
#
pi_E$ sudo systemctl status rsyslog | grep Active:
     Active: active (running) since Sat 2022-11-08 18:35:22 CET; 15h ago

pi_E$ sudo systemctl restart rsyslog;  sleep 2

pi_E$ sudo systemctl status rsyslog | grep Active:
     Active: active (running) since Sun 2022-11-09 09:37:37 CET; 4s ago
```

&nbsp;
## Confirm script & syslog

```
# - Run the script manually.

pi_E$ ./zfs-stats.sh
pi_E$

pi_E$ tail /var/log/zfs-stats.log
    Jan 27 15:42:57 pecan ZFS: [iostat] $ zpool iostat 5 5
    Jan 27 15:42:58 pecan ZFS: [iostat]    capacity     operations     bandwidth
    Jan 27 15:42:58 pecan ZFS: [iostat]    pool        alloc   free   read  write   read  write
    Jan 27 15:42:58 pecan ZFS: [iostat]    ----------  -----  -----  -----  -----  -----  -----
    Jan 27 15:42:58 pecan ZFS: [iostat] 1. t5_pool      451G  1.37T      3     24   399K   241K
    Jan 27 15:42:58 pecan ZFS: [iostat] 2. t5_pool      451G  1.37T      0     24  76.8K   105K
    Jan 27 15:42:58 pecan ZFS: [iostat] 3. t5_pool      451G  1.37T      0     30  25.6K   122K
    Jan 27 15:42:58 pecan ZFS: [iostat] 4. t5_pool      451G  1.37T      0     25      0   113K
    Jan 27 15:42:58 pecan ZFS: [iostat] 5. t5_pool      451G  1.37T      0     24   102K  97.5K
    ...
```


&nbsp;
## Deploy SystemD

- https://www.digitalocean.com/community/tutorials/understanding-systemd-units-and-unit-files
- https://linuxconfig.org/how-to-schedule-tasks-with-systemd-timers-in-linux
- https://www.putorius.net/using-systemd-timers.html#verify-the-correctness-of-your-unit-files
- https://unix.stackexchange.com/questions/396605/systemd-timer-every-2-hours-at-30-minutes-past-the-hour

```
pi_E$ sudo install -m 644  ./zfs-stats.timer   /etc/systemd/system/
pi_E$ sudo install -m 644  ./zfs-stats.service /etc/systemd/system/

# - Confirm correctness:
pi_E$ sudo systemd-analyze verify /etc/systemd/system/zfs-stats*

# - In another login session: Tail the syslogd journal.
pi_E$ sudo journalctl -f | grep zfs-stats
    Jan 27 15:21:21 pecan-pi sudo[2097396]:      jdb : TTY=pts/0 ; PWD=/home/jdb/zGit ; USER=root ; COMMAND=/usr/bin/systemctl start zfs-stats.service
    Jan 27 15:21:36 pecan-pi sudo[2097505]:      jdb : TTY=pts/0 ; PWD=/home/jdb/zGit ; USER=root ; COMMAND=/usr/bin/systemctl status zfs-stats.timer
    Jan 27 15:21:41 pecan-pi systemd[1]: zfs-stats.service: Succeeded.
    Jan 27 15:22:07 pecan-pi sudo[2097763]:      jdb : TTY=pts/0 ; PWD=/home/jdb/zGit ; USER=root ; COMMAND=/usr/bin/systemctl status zfs-stats.timer
    Jan 27 15:23:33 pecan-pi sudo[2098387]:      jdb : TTY=pts/0 ; PWD=/home/jdb/zGit ; USER=root ; COMMAND=/usr/bin/systemctl status zfs-stats.service
    Jan 27 15:30:33 pecan-pi sudo[2101185]:      jdb : TTY=pts/0 ; PWD=/home/jdb/zGit ; USER=root ; COMMAND=/usr/bin/systemctl status zfs-stats.timer
    Jan 27 15:30:51 pecan-pi sudo[2101285]:      jdb : TTY=pts/0 ; PWD=/home/jdb/zGit ; USER=root ; COMMAND=/usr/bin/systemctl status zfs-stats.service
```


&nbsp;
## Start SystemD services

```
pi_E$ sudo systemctl daemon-reload

pi_E$ sudo systemctl start  zfs-stats.timer
pi_E$ sudo systemctl enable zfs-stats.timer
pi_E$ sudo systemctl enable zfs-stats.service


# - You can run an extra report manually.
#   The systemctl command returns right away, but processing still occurs in the background.
#
pi_E$ sudo systemctl start  zfs-stats.service
pi_E$


# - Timer status:
#
pi_E$ sudo systemctl status zfs-stats.timer
    o zfs-stats.timer - JDB ZFS stats
         Loaded: loaded (/etc/systemd/system/zfs-stats.timer; enabled; vendor preset: enabled)
         Active: active (waiting) since Thu 2022-11-27 15:20:22 CET; 1min 45s ago
        Trigger: Thu 2022-11-27 15:33:56 CET; 11min left
       Triggers: o zfs-stats.service

    Jan 27 15:20:22 pecan-pi systemd[1]: Started JDB ZFS stats.


# - Service status: The service itself is only active when an instance is spawned by timer (or manually).
#
pi_E$ sudo systemctl status zfs-stats.service
    o zfs-stats.service - JDB ZFS stats
         Loaded: loaded (/etc/systemd/system/zfs-stats.service; enabled; vendor preset: enabled)
         Active: inactive (dead) since Thu 2022-11-27 15:21:41 CET; 1min 52s ago
    TriggeredBy: o zfs-stats.timer
        Process: 2097399 ExecStart=/usr/local/bin/zfs-stats.sh (code=exited, status=0/SUCCESS)
       Main PID: 2097399 (code=exited, status=0/SUCCESS)

    Jan 27 15:21:21 pecan-pi systemd[1]: Started JDB ZFS stats.
    Jan 27 15:21:41 pecan-pi ZFS[2097562]: [iostat] pool        alloc   free   read  write   read  write
    Jan 27 15:21:41 pecan-pi systemd[1]: zfs-stats.service: Succeeded.
```

&nbsp;


## Confirm

```
pi_E$ tail -f /var/log/zfs-stats.log
    Jan 27 12:51:53 pecan ZFS: [iostat] John says test
    Jan 27 15:18:06 pecan ZFS: [iostat]
    Jan 27 15:18:06 pecan ZFS: [iostat] $ zpool iostat 5 5
    Jan 27 15:18:06 pecan ZFS: [iostat]    capacity     operations     bandwidth
    Jan 27 15:18:06 pecan ZFS: [iostat]    pool        alloc   free   read  write   read  write
    Jan 27 15:18:06 pecan ZFS: [iostat]    ----------  -----  -----  -----  -----  -----  -----
    Jan 27 15:18:06 pecan ZFS: [iostat] 1. t5_pool      451G  1.37T      3     24   399K   241K
    Jan 27 15:18:06 pecan ZFS: [iostat] 2. t5_pool      451G  1.37T      0     25  76.8K   128K
    Jan 27 15:18:06 pecan ZFS: [iostat] 3. t5_pool      451G  1.37T      0     25   102K   117K
    Jan 27 15:18:06 pecan ZFS: [iostat] 4. t5_pool      451G  1.37T      0     27      0   139K
    Jan 27 15:18:06 pecan ZFS: [iostat] 5. t5_pool      451G  1.37T      0      0      0      0
    ...
```
<br>


## License
Copyright &copy; 2019, Mountain Informatik GmbH<br>
All rights reserved.<br>
Permission to share is granted under the terms of the **Server Side Public License**<br>
https://www.mongodb.com/licensing/server-side-public-license
<br>

