
# JDB, 06. Jan 2019
# - Nginx reverse proxy for REST API server.
# - All files managed by myself, not Certbot.


# - This server section shadows the Apache HTML file from an Nginx port: 18446
#   This is a sanity test to confirm Nginx.

server {
        root /var/www/html;
        index  index.html index.htm;

        server_name pumpkin.purplemicro.ch;

	# - Require client cert, to reduce "Internet noise" of ad hoc hacker connections.
	#
	ssl_client_certificate /etc/nginx/client_certs/ZZ-Client-Cert-CA;   ## jdcc-ca-cert.pem;
	ssl_verify_client optional;	# - Allow custom error processing.

	# - Tips:
	#   https://serverfault.com/questions/721572/nginx-verifying-client-certs-only-on-a-particular-location
	#   https://www.keycdn.com/support/nginx-location-directive

	location = /robots.txt {
		add_header Content-Type text/plain;
		return 200 "User-agent: *\nDisallow: /\n";
	}

        location / {
		if ($ssl_client_verify != SUCCESS) {
			# - JDB: Disable both, to allow access without a client cert.
			## return 403;	# - Forbidden
			return 444;	# - Drop:  https://httpstatusdogs.com/444-no-response
		}
                try_files $uri $uri/ =404;
        }

    listen [::]:18446 ssl ipv6only=on;
    listen 18446 ssl;

    ssl_certificate /etc/letsencrypt/wildcard/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/wildcard/privkey.pem;

    include /etc/letsencrypt/options-ssl-nginx.conf;  # - default from Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;    # - default from Certbot
}


# - This server section is a reverse proxy for GETH daemon:
#   http://localhost:8545/
#
# - Tips:
#   https://stackoverflow.com/questions/54491991/geth-websocket-over-nginx-reverse-proxy

server {
    listen [::]:18545 ssl ipv6only=on;
    listen 18545 ssl;

    server_name pumpkin.purplemicro.ch;

    # ssl stuff here
    ssl_certificate /etc/letsencrypt/wildcard/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/wildcard/privkey.pem;

    include /etc/letsencrypt/options-ssl-nginx.conf;  # - default from Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;    # - default from Certbot

	# - Require client cert, to reduce "Internet noise" of ad hoc hacker connections.
	#
	ssl_client_certificate /etc/nginx/client_certs/ZZ-Client-Cert-CA;   ## jdcc-ca-cert.pem;
	ssl_verify_client optional;	# - Allow custom error processing.

	# - Tips:
	#   https://serverfault.com/questions/721572/nginx-verifying-client-certs-only-on-a-particular-location
	#   https://www.keycdn.com/support/nginx-location-directive

	location = /robots.txt {
		add_header Content-Type text/plain;
		return 200 "User-agent: *\nDisallow: /\n";
	}

    location / {
		if ($ssl_client_verify != SUCCESS) {
			# - JDB: Disable both, to allow access without a client cert.
			## return 403;	# - Forbidden
			return 444;	# - Drop:  https://httpstatusdogs.com/444-no-response
		}
                try_files $uri $uri/ =404;

        if ($request_method = OPTIONS) {
            return 204;
        }

        auth_basic          off;
        proxy_pass          http://localhost:8545;
    }
}


