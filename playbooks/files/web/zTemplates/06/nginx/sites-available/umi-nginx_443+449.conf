
server {
        root /var/www/umi-443;          # - Multiplexed
        index  index.html index.htm;

        server_name ZZ-ServerName;

	# - Require client cert, to reduce "Internet noise" of ad hoc hacker connections.
	#
	ssl_client_certificate /etc/nginx/client_certs/ZZ-Client-Cert-CA;   ## jdcc-ca-cert.pem;
	ssl_verify_client optional;	# - Allow custom error processing.

	# - Tips:
	#   https://serverfault.com/questions/721572/nginx-verifying-client-certs-only-on-a-particular-location
	#   https://www.keycdn.com/support/nginx-location-directive
	#
	# - Use cases:
	#   $ curl -s https://ocean.umi.ch		        # - Fails, no client cert.
	#   $ curl -s https://ocean.umi.ch/		        # - Fails, no client cert.
	#   $ curl -s https://ocean.umi.ch/robots.txt	# - Works.
	#   $ curl -s https://ocean.umi.ch/blah		    # - Fails, no client cert.

	location = /robots.txt {
		add_header Content-Type text/plain;
		return 200 "User-agent: *\nDisallow: /\n";
	}

        location / {
		if ($ssl_client_verify != SUCCESS) {
			# - JDB: Disable both, to allow access without a client cert.
			## return 403;	# - Forbidden
			return 444;	# - Drop:  https://httpstatusdogs.com/444-no-response
		}
                try_files $uri $uri/ =404;
        }

    listen [::]:443 ssl ipv6only=on;
    listen 443 ssl;
    #
    listen [::]:449 ssl ipv6only=on;
    listen 449 ssl;

    # - UMI is a symlink to a directory of certs. Eg:
    #   $ cd /etc/letsencrypt/
    #   $ rm -f UMI;  ln -s ./bozo  UMI                         # - Default installation.
    #   $ rm -f UMI;  ln -s ./live/aws.umi.ch  UMI              # - Variant 1
    #   $ rm -f UMI;  ln -s ./aws.umi.ch/live/aws.umi.ch  UMI   # - Variant 2 (obsolete)
    #
    ssl_certificate     /etc/letsencrypt/UMI/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/UMI/privkey.pem;
    #
    include     /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
}



server {
        root /var/www/umi-446/;
        index index-cc.html

        server_name ZZ-ServerName;

    listen [::]:446 ssl ipv6only=on;
    listen 446 ssl;

    # - UMI is a symlink to a directory of certs. Eg:
    #   $ cd /etc/letsencrypt/
    #   $ rm -f UMI;  ln -s ./bozo  UMI                         # - Default installation.
    #   $ rm -f UMI;  ln -s ./live/aws.umi.ch  UMI              # - Variant 1
    #   $ rm -f UMI;  ln -s ./aws.umi.ch/live/aws.umi.ch  UMI   # - Variant 2 (obsolete)
    #
    ssl_certificate     /etc/letsencrypt/UMI/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/UMI/privkey.pem;
    #
    include     /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;


	# - Client cert:
	#   https://fardog.io/blog/2017/12/30/client-side-certificate-authentication-with-nginx/
	#
	# - If not provided, connection fails with: 400 Bad Request
	#
	ssl_client_certificate /etc/nginx/client_certs/ZZ-Client-Cert-CA;   ## jdcc-ca-cert.pem;
	ssl_verify_client on;

        location / {
		# - not reached with 'ssl_verify_client on' :
		# - if the client-side certificate failed to authenticate, show a 403
		#   message to the client
		if ($ssl_client_verify != SUCCESS) {
			return 403;
		}

                try_files $uri $uri/ =404;
        }
}



# - When this section is enabled, a listener is started on port 80.
#   This one-line config gives a NOOP service returning http_status 404.
#
## server {
##    # - JDB: Disable this line when using Let's Encrypt Certbot:
##    return 404;   # - Unconditionally drop all HTTP traffic on port 80, without redirect.
## }


