# Product: Argon Case

I recommend this product, for Raspberry PI 4.

- https://www.argon40.com/products/argon-one-v2-case-for-raspberry-pi-4
- https://www.reichelt.de/index.html?ACTION=446&LA=0&nbc=1&q=rpi%20case%20argon2

Here's some info on PI temperatures and cooling.<br>
I like to run my PI's cool, below 50 degrees (Celsius), even with fan noise.

- https://raspberrytips.com/raspberry-pi-temperature/
- https://raspberrypi.stackexchange.com/questions/114462/how-much-temperature-is-normal-temperature-for-raspberry-pi-4
<br>


## Example of temperature reduction

```
pi_C$ date; whoami; uname -n
    Sun Oct 23 11:54:12 CEST 2022
    jdb
    cherry-pi

pi_C$ alias hot='cat -v /sys/class/thermal/thermal_zone0/temp | perl -ne '\''printf "%.2f degrees Celsius\n", ($_/1000)'\'' '

pi_C$ alias hot2='sudo vcgencmd measure_temp'


pi_C$ hot           # - Measured from the SOC (CPU) chip
    39.92 degrees Celsius

pi_C$ hot2          # - Measured on the video chip.
    temp=37.9'C


pi_C$ cat /etc/argononed.conf
    # - The Argon case daemon uses this config to start the fan, based on PI temperature.
    #   "Temperature in Celsius" = "Percentage of fan speed"
    40=10
    45=30
    50=55
    55=100


# - Results of the fan config:

pi_C$ less /var/log/pi.log
    ...
    # - Before the fan software is enabled:
    #
    Oct  3 08:45:17 cherry-pi PI: [heat] 51.60 ℃
    Oct  3 09:00:08 cherry-pi PI: [heat] 53.55 ℃
    Oct  3 09:15:33 cherry-pi PI: [heat] 55.01 ℃

    ...
    # - After the fan software is enabled:
    #
    Oct 23 08:30:16 cherry-pi PI: [heat] 40.40 ℃
    Oct 23 08:45:15 cherry-pi PI: [heat] 39.92 ℃
    Oct 23 09:00:16 cherry-pi PI: [heat] 40.40 ℃
    Oct 23 09:15:35 cherry-pi PI: [heat] 40.89 ℃
    Oct 23 09:30:29 cherry-pi PI: [heat] 41.38 ℃

```
<br>


## Deploy the patched software with Ansible

The vendor's **engineering** and **Python** control scripts are well done,<br>
but the **Linux packaging** is mediocre -- eg, a Bash script writing other Bash scripts.<br>
A rework is needed.<br>

Meanwhile, I made small patches for Ubuntu-PI, with installation by **Ansible**.<br>

- https://gitlab.com/John_DB/Johns-ansible-public/-/blob/main/playbooks/03_q.packages-PI-argon.yml
- https://gitlab.com/John_DB/Johns-ansible-public/-/blob/main/playbooks/04_i.install-pi-argon-case-tools.yml


```
mac$ date; whoami; uname -n
    Sun Oct 23 18:13:02 CEST 2022
    jdb
    jdbs-mac-mini-3.home


# - FYI: Use verbose mode to see the package list.
mac$ ape 03_q cherry-pi
    -# 03_q.packages-PI-argon.yml
    -$ export APE_HOSTS='cherry-pi'         # - Playbook host list.
    -$ export APE_STEPS=''
    -$ ansible-playbook   '/Users/jdb/zGit/Johns-ansible/playbooks/03_q.packages-PI-argon.yml'

PLAY [localhost] ***************************************************************
    TASK [Gathering Facts] *********************************************************
    TASK [01. Timestamp] ***********************************************************
    ok: [localhost] => {
        msg": "2022-10-23 18:18:56 CEST

PLAY [cherry-pi] ***************************************************************
    TASK [Gathering Facts] *********************************************************
    TASK [30. Packages for Argon PI utilities] *************************************
    TASK [debug] *******************************************************************
    TASK [debug] *******************************************************************
    PLAY RECAP *********************************************************************
    localhost                  : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    cherry-pi                  : ok=2    changed=1    unreachable=0    failed=0    skipped=2    rescued=0    ignored=0


mac$ ape 04_i cherry-pi
    -# 04_i.install-pi-argon-case-tools.yml
    ...

PLAY [localhost] ***************************************************************
    TASK [Gathering Facts] *********************************************************
    TASK [01. Timestamp] ***********************************************************
    ok: [localhost] => {
        msg": "2022-10-22 20:18:23 CEST

PLAY [cherry-pi] ******************************************************************
    TASK [Gathering Facts] *********************************************************
    TASK [10. Sanity check 1] ******************************************************
    TASK [debug] *******************************************************************
    TASK [10. Failed?] *************************************************************
    TASK [20. install files here: /usr/local/bin/] *********************************
    TASK [debug] *******************************************************************
    TASK [21. install files here: /usr/share/pixmaps/] *****************************
    TASK [debug] *******************************************************************
    TASK [40. Run installer script] ************************************************
    TASK [debug] *******************************************************************
    ok: [cherry-pi] => {
            "stdout_lines": [
             $ /usr/local/bin//argon1.sh
             $ sudo mkdir /home/ubuntu/Desktop/
             $ sudo chown ubuntu:ubuntu /home/ubuntu/Desktop/
                Created symlink /etc/systemd/system/multi-user.target.wants/argononed.service → /lib/systemd/system/argononed.service.

                ***************************
                Argon One Setup Completed.
                ***************************
                You may need to reboot for changes to take effect

                To configure the fan:  $ /usr/local/bin/argonone-config.sh
                To uninstall:          $ /usr/local/bin/argonone-uninstall.sh
                Systemd:  $ systemctl status argononed.service

             $ ls -l /usr/local/bin/
                total 72
                -rwxr-xr-x 1 root root 24204 Oct 22 20:18 argon1.sh
                -rwxr-xr-x 1 root root  3480 Oct 22 20:20 argonone-config.sh
                -rwxr-xr-x 1 root root  9594 Oct 22 20:18 argonone-irconfig.sh
                -rwxr-xr-x 1 root root 11238 Oct 22 20:18 argonone-irdecoder.py
                -rwxr-xr-x 1 root root   874 Oct 22 20:20 argonone-uninstall.sh
                -rwxr-xr-x 1 root root  2464 Oct 22 20:20 argononed.py
                -rwxr-xr-x 1 root root  1082 Mar 14  2022 heat-logger.sh
                -rwxr-xr-x 1 root root  4472 Mar 14  2022 speedtest-stats.sh

             $ systemctl status argononed.service
                ● argononed.service - Argon One Fan and Button Service
                     Loaded: loaded (/lib/systemd/system/argononed.service; enabled; vendor preset: enabled)
                     Active: active (running) since Sat 2022-10-22 20:20:38 CEST; 96ms ago
                   Main PID: 3595 (python3)
                      Tasks: 1 (limit: 9226)
                     Memory: 3.2M
                        CPU: 85ms
                     CGroup: /system.slice/argononed.service
                             └─3595 /usr/bin/python3 /usr/local/bin/argononed.py
                Oct 22 20:20:38 cherry-pi systemd[1]: Started Argon One Fan and Button Service.

    TASK [Failed?] *****************************************************************

PLAY RECAP *********************************************************************
    cherry-pi                  : ok=6    changed=3    unreachable=0    failed=0    skipped=5    rescued=0    ignored=0
    localhost                  : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0



# - FYI: It's not necessary to run the configuration script manually.

pi_C$ /usr/local/bin/argonone-config.sh
    --------------------------------------
    Argon One Fan Speed Configuration Tool
    --------------------------------------
    WARNING: This will remove existing configuration.
    Press Y to continue:Y
    Thank you.

    Select fan mode:
      1. Always on
      2. Adjust to temperatures (55C, 60C, and 65C)
      3. Customize behavior
      4. Cancel
    NOTE: You can also edit /etc/argononed.conf directly
    Enter Number (1-4):^C


# - FYI: Here's my preferred fan speed configuration:
pi_C$ cat /etc/argononed.conf
    ...
    40=10
    45=30
    50=55
    55=100


# - I like to reboot the PI after this installation, to ensure proper service starting.

mac$ ape 05_e cherry-pi
    -# 05_e.reboot.yml
    -$ export APE_HOSTS='cherry-pi'        # - Playbook host list.
    -$ export APE_STEPS=''
    -$ ansible-playbook   '/Users/jdb/zGit/Johns-ansible/playbooks/05_e.reboot.yml'

PLAY [localhost] ***************************************************************
    TASK [Gathering Facts] *********************************************************
    TASK [01. Timestamp #1] ********************************************************
    ok: [localhost] => {
        msg": "2022-10-22 19:39:08 CEST

PLAY [cherry-pi] ***************************************************************
    TASK [Gathering Facts] *********************************************************
    TASK [Guardian] ****************************************************************
    TASK [08. Sanity test 1: Spurious APE_vars ?] **********************************
    TASK [09. Uptime before, and PI disk info] *************************************
    TASK [debug] *******************************************************************
    ok: [cherry-pi] => {
            "stdout_lines": [
             $ date; uptime; uname -r
                Sat Oct 22 19:39:13 CEST 2022
                 19:39:13 up  8:21,  2 users,  load average: 0.30, 0.22, 0.19
                5.15.0-1017-raspi

    TASK [10. PI disk info] ********************************************************
    TASK [debug] *******************************************************************
    TASK [11. Reboot the machine with all default options] *************************
    TASK [debug] *******************************************************************
    TASK [debug] *******************************************************************
    ok: [cherry-pi] => {
        tout.elapsed": "69

    TASK [12. Uptime after] ********************************************************
    TASK [debug] *******************************************************************
    ok: [cherry-pi] => {
            "stdout_lines": [
             $ date; uptime; uname -r
                Sat Oct 22 19:40:29 CEST 2022
                 19:40:29 up 0 min,  1 user,  load average: 2.77, 0.80, 0.28
                5.15.0-1017-raspi

    TASK [13. PI disk info] ********************************************************
    TASK [debug] *******************************************************************

PLAY [localhost] ***************************************************************
    TASK [Gathering Facts] *********************************************************
    TASK [02. Timestamp #2] ********************************************************
    ok: [localhost] => {
        msg": "2022-10-22 19:40:32 CEST

PLAY RECAP *********************************************************************
    cherry-pi                  : ok=9    changed=5    unreachable=0    failed=0    skipped=5    rescued=0    ignored=0
    localhost                  : ok=4    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```
<br>


## License
Copyright &copy; 2019, Mountain Informatik GmbH<br>
All rights reserved.<br>
Permission to share is granted under the terms of the **Server Side Public License**<br>
https://www.mongodb.com/licensing/server-side-public-license
<br>

