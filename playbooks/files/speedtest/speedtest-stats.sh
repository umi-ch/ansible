#!/bin/bash

sym_ver='2.0.2'

# JDB, 11. Nov 2022
# - /usr/local/bin/speedtest-stats.sh :
#   Simple looper script, to report speedtests.
#
# - Prerequisites: Install speedtest. Eg, on Ubuntu:
#   pi$ sudo apt install speedtest
#
# - Manual session:
#
#   pi_P$ which speedtest; speedtest-cli --version
#   /usr/bin/speedtest
#   speedtest-cli 2.1.2
#   Python 3.8.10 (default, Jun  2 2021, 10:49:15) [GCC 10.3.0]
#
#   pi_P$ time msg=$(speedtest-cli --simple); echo rc: $?
#   real    0m23.047s
#   user    0m5.678s
#   sys     0m11.062s
#   rc: 0
#
#   pi_P$ echo "$msg"
#   Ping: 19.795 ms
#   Download: 265.34 Mbit/s
#   Upload: 99.86 Mbit/s
#
# - Test:
#   pi$ logger -p local3.info -t SPEED "[test] John says test"


# - For longer sleep times, eg more than a few minutes, use SystemD timers instead.
#
sleep=21600     # - Four times per day = every six hours = sleep 21'600 seconds


facility=local3
level=info
tag='SPEED'
brackets='[test]'


# - Format: Width of speed numbers.
#   Not too long (extra blanks), but long enough for column alignment.
#
if [[ -n "${STATS_SPEEDTEST_WIDE}" ]]; then
    true    # - NOOP, use the external value
elif [[ "$(hostname)" =~ ocean ]]; then
    STATS_SPEEDTEST_WIDE=4
else
    STATS_SPEEDTEST_WIDE=3
fi
export STATS_SPEEDTEST_WIDE


## msg_test=$(cat <<END
## Ping: 3.5 ms
## Download: 565.10 Mbit/s
## Upload: 575.78 Mbit/s
## END
## )

## msg_test=$(cat <<END
## Ping: 20.80700 ms
## Download: 269.9200 Mbit/s
## Upload: 91.2700 Mbit/s
## END
## )


# - Gather stats.

function f_speedtest() {
    local out=''  msg=''  msg2=''  line=''
    local rc_1=0  rc_2=0


    # - Uptime load average (first entry):
    # - Beware of system dependencies in output format:  :-(
    #
    #   mac$ uptime
    #    8:23  up 8 days, 18:15, 4 users, load averages: 1.72 1.88 1.83
    #
    #   pi_A$ uptime
    #    08:23:59 up 2 days, 11:18,  2 users,  load average: 0.08, 0.07, 0.03
    #
    #
    ## msg_2="18:04  up 10 days,  3:57, 3 users, load averages: 2.31 2.39 2.15"
    ## msg_2=" 08:16:20 up 2 days, 11:10,  2 users,  load average: 0.126, 0.02, 0.03"

    msg_2=$(uptime)
    rc_2=$?
    if [[ $rc_2 -ne 0 ]]; then
        msg_2="${msg_2}, rc_2: $rc_2"      # - Only log this with error conditions.
    fi
    ## echo "-b: msg_2: '$msg_2'"

    msg_2=$(perl -le '
        $_ = $ARGV[0];
        if (/^.*?\saverages?:\s+([\d\.]+),?\s.*$/i) {
            $_ = sprintf ("%5.2f", $1);     # - Eg: 0.02
        }
        print "Load: $_ avg";               # - avg == average
    '  "${msg_2}")
    #
    ## echo "-c: msg_2: '${msg_2}'"


    # - Speedtest:
    #
    ## msg="${msg_test}"
    msg=$(speedtest-cli --simple 2>&1)

    rc_1=$?
    if [[ $rc_1 -ne 0 ]]; then
        msg=$(printf "%s\n%s\n" "${msg}" "rc_1: $rc_1")    # - Only log this with error conditions.
    fi

    # - Round the outputs to a consistent decimal place, space-padding the left to three places:
    #   Ping: 19.783 ms         ->  Ping:  19.8 ms
    #   Download: 80.76 Mbit/s  ->  Download:  80.8 Mbit/s
    #   Upload: 90.32 Mbit/s    ->  Upload:  90.3 Mbit/s
    #
    # - Also:
    #   Ping: 20.80700 ms,  Download: 269.9200 Mbit/s,  Upload: 91.2700 Mbit/s  ->
    #   Ping:  20.80 ms,  Download: 270 Mbit/s,  Upload:  91 Mbit/s

    msg=$(echo "${msg}" | perl -nle '
        BEGIN {
            our $line = "";
        }

        if (/
                (\S+?:\s+)          # "Ping: "
                (\d+\.\d+)          # "19.783"
                (\s+\S+)            # " ms"
            /x)
        {
            my $a = $1;
            my $b = $2;
            my $c = $3;

            my $b2;
            if ($a =~ /Ping:/i) {
                my $fmt = "%4.1f";
                $b2 = sprintf ($fmt, $b);                       # - Eg: "  4.1f"
            } else {
                my $fmt = "%". $ENV{STATS_SPEEDTEST_WIDE}. "d"; # - Eg: %4d
                my $r = int ($b + 0.5);                         # - Round
                $b2 = sprintf ($fmt, $r);                       # - Eg: " 235"
            }

            $_ = "${a}${b2}${c}";
            s/Ping:(\d)/Ping: $1/i;
        }
        $line .= $_. ",  ";

        END {
            print $line;
        }
    ')

    msg="${msg}${msg_2}"

    ## echo "msg: '${msg}'"
    logger  -p ${facility}.${level}  -t "${tag}"  "${brackets}"  "${msg}"
}


# - Use SystemD timers instead of a sleep-loop here.
#
## while (true); do
##   f_speedtest
##   sleep ${sleep}
## done


f_speedtest


