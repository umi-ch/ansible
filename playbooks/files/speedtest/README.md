# Crypto stats

Here's a simple system to record periodic Internet speed tests via Syslog.<br/>

**Assumptions**
- Speedtest is installed: &nbsp; ```$ sudo apt install speedtest```<br/>

&nbsp;

## Components

- stat scripts
- rsyslog config
- systemd scripts

&nbsp;

## Quick Check

From a Bash shell:

```
pi_P$ speedtest-cli --simple
    Ping: 19.795 ms
    Download: 265.34 Mbit/s
    Upload: 99.86 Mbit/s

pi_A$ speedtest-cli --simple
    Ping: 4.03 ms
    Download: 602.54 Mbit/s
    Upload: 519.58 Mbit/s

pi_C$ speedtest-cli --simple
    Ping: 46.429 ms
    Download: 22.60 Mbit/s
    Upload: 9.22 Mbit/s

d$ speedtest-cli --simple
    Ping: 7.411 ms
    Download: 9.61 Mbit/s
    Upload: 10.03 Mbit/s

o$ speedtest-cli --simple
    Ping: 1.193 ms
    Download: 2173.50 Mbit/s
    Upload: 1122.29 Mbit/s

aws$ speedtest-cli --simple
    Ping: 5.589 ms
    Download: 659.11 Mbit/s
    Upload: 394.75 Mbit/s

mac$ load_conda
mac$ speedtest-cli --simple
    Ping: 2.999 ms
    Download: 92.31 Mbit/s
    Upload: 94.36 Mbit/s
```

&nbsp;

## Ansible playbook

I have an **Ansible playbook** to deploy this script system.<br>
For clarity, I show the "unwrapped" manual deployment steps here.<br>

- https://gitlab.com/John_DB/Johns-ansible-public/-/blob/main/playbooks/04_k.install-speedtest.yml

&nbsp;

## Prep

I prefer to run my systems in the local timezone, not UTC.<br/>
This ensures that syslog messages appear in local time.

```
# - Default:
pi_A$ cat /etc/timezone
    Etc/UTC

# - Update:
pi_A$ echo 'Europe/Zurich' | sudo tee /etc/timezone
    Europe/Zurich

pi_A$ sudo dpkg-reconfigure tzdata
pi_A$ sudo systemctl restart rsyslog
pi_A$ sudo reboot   # - For good measure
```

&nbsp;

## Install scripts

```
pi_A$ sudo install -m 755  ./speedtest-stats.sh  /usr/local/bin/
pi_A$ sudo install -m 644  ./13-speedtest.conf   /etc/rsyslog.d/
```

&nbsp;
## Deploy syslog

```
# - Check config:
#
pi_A$ rsyslogd -N1
    rsyslogd: version 8.2006.0, config validation run (level 1), master config /etc/rsyslog.conf
    rsyslogd: End of config validation run. Bye.

# - If OK, restart rsyslog:
#
pi_A$ sudo systemctl status rsyslog | grep Active:
     Active: active (running) since Sat 2022-11-08 18:35:22 CET; 15h ago

pi_A$ sudo systemctl restart rsyslog;  sleep 2

pi_A$ sudo systemctl status rsyslog | grep Active:
     Active: active (running) since Sun 2022-11-09 09:37:37 CET; 4s ago
```

&nbsp;
## Confirm script & syslog

```
# - Run the script manually.

pi_A$ ./speedtest-stats.sh
pi_A$

pi_A$ ls -l /var/log/speedtest.log
    -rw-r--r-- 1 syslog adm 104 Jan  9 20:53 /var/log/speedtest.log

pi_A$ tail /var/log/speedtest.log
    Jan  9 19:53:43 apple-pi SPEED: [test] Ping: 4.545 ms,  Download: 607.54 Mbit/s,  Upload: 566.18 Mbit/s
```


&nbsp;
## Deploy SystemD

- https://www.digitalocean.com/community/tutorials/understanding-systemd-units-and-unit-files
- https://linuxconfig.org/how-to-schedule-tasks-with-systemd-timers-in-linux
- https://www.putorius.net/using-systemd-timers.html#verify-the-correctness-of-your-unit-files
- https://unix.stackexchange.com/questions/396605/systemd-timer-every-2-hours-at-30-minutes-past-the-hour

```
pi_A$ sudo install -m 644  ./speedtest-stats.timer   /etc/systemd/system/
pi_A$ sudo install -m 644  ./speedtest-stats.service /etc/systemd/system/

# - Confirm correctness:
pi_A$ sudo systemd-analyze verify /etc/systemd/system/speedtest*

# - In another login session: Tail the syslogd journal.
pi_A$ journalctl -f | grep speedtest
    ...
    ... Jan 09 22:26:59 apple-pi sudo[5398]: jdb : TTY=pts/0 ; PWD=/home/jdb/zGit ; USER=root ; COMMAND=/usr/bin/systemctl start speedtest-stats.service
    ... Jan 09 22:26:59 apple-pi systemd[1]: Started JDB speedtest stats.
    ... Jan 09 22:26:59 apple-pi audit[1]: SERVICE_START pid=1 uid=0 auid=4294967295 ses=4294967295 subj=unconfined msg='unit=speedtest-stats comm="systemd" exe="/usr/lib/systemd/systemd" hostname=? addr=? terminal=? res=success'
    ... Jan 09 22:27:09 apple-pi systemd[1]: speedtest-stats.service: Deactivated successfully.
    ... Jan 09 22:27:09 apple-pi audit[1]: SERVICE_STOP pid=1 uid=0 auid=4294967295 ses=4294967295 subj=unconfined msg='unit=speedtest-stats comm="systemd" exe="/usr/lib/systemd/systemd" hostname=? addr=? terminal=? res=success'
    ... Jan 09 22:27:09 apple-pi systemd[1]: speedtest-stats.service: Consumed 16.738s CPU time.
    ...
```


&nbsp;
## Start SystemD services

```
pi_A$ sudo systemctl daemon-reload

pi_A$ sudo systemctl start  speedtest-stats.timer
pi_A$ sudo systemctl enable speedtest-stats.timer
pi_A$ sudo systemctl enable speedtest-stats.service

# - You can run an extra report manually.
#   The systemctl command returns right away, but processing still occurs in the background.
#
pi_A$ sudo systemctl start  speedtest-stats.service


# - Timer status:
#
pi_A$ sudo systemctl status speedtest-stats.timer
    o speedtest-stats.timer - JDB speedtest stats
         Loaded: loaded (/etc/systemd/system/speedtest-stats.timer; enabled; vendor preset: enabled)
         Active: active (waiting) since Sun 2022-11-09 22:14:01 CET; 4min 50s ago
        Trigger: Mon 2022-11-10 01:24:29 CET; 3h 5min left
       Triggers: o speedtest-stats.service


# - Service status: The service itself is only active when an instance is spawned by timer (or manually).
#
pi_A$ sudo systemctl status speedtest-stats.service
    o speedtest-stats.service - JDB speedtest stats
         Loaded: loaded (/etc/systemd/system/speedtest-stats.service; enabled; vendor preset: enabled)
         Active: inactive (dead) since Sun 2022-11-09 22:14:56 CET; 5min ago
    TriggeredBy: o speedtest-stats.timer
        Process: 5095 ExecStart=/usr/local/bin/speedtest-stats.sh (code=exited, status=0/SUCCESS)
       Main PID: 5095 (code=exited, status=0/SUCCESS)
            CPU: 16.610s


# - Service status when running:
#
pi_A$ sudo systemctl status speedtest-stats.service
    o speedtest-stats.service - JDB speedtest stats
         Loaded: loaded (/etc/systemd/system/speedtest-stats.service; enabled; vendor preset: enabled)
         Active: active (running) since Sun 2022-11-09 22:35:24 CET; 10s ago
    TriggeredBy: o speedtest-stats.timer
       Main PID: 81197 (speedtest-stats)
          Tasks: 11 (limit: 9259)
         Memory: 13.1M
            CPU: 2.014s
         CGroup: /system.slice/speedtest-stats.service
                 +- 81197 /bin/bash /usr/local/bin/speedtest-stats.sh
                 +- 82100 /bin/bash /usr/local/bin/speedtest-stats.sh
                 +- 81201 /usr/bin/python3 /usr/bin/speedtest-cli --simple
```


&nbsp;
## Confirm

```
pi_A$ tail -f /var/log/speedtest.log
    Jan  9 19:53:43 apple-pi SPEED: [test] Ping: 4.545 ms,  Download: 607.54 Mbit/s,  Upload: 566.18 Mbit/s
    Jan  9 21:33:54 apple-pi SPEED: [test] Ping: 5.506 ms,  Download: 591.52 Mbit/s,  Upload: 515.92 Mbit/s
    Jan  9 21:38:49 apple-pi SPEED: [test] Ping: 4.752 ms,  Download: 581.18 Mbit/s,  Upload: 517.00 Mbit/s
    Jan  9 22:14:56 apple-pi SPEED: [test] Ping: 5.253 ms,  Download: 565.86 Mbit/s,  Upload: 562.18 Mbit/s
```
<br>


## License
Copyright &copy; 2019, Mountain Informatik GmbH<br>
All rights reserved.<br>
Permission to share is granted under the terms of the **Server Side Public License**<br>
https://www.mongodb.com/licensing/server-side-public-license
<br>

