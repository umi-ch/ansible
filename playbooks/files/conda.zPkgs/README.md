# Miniconda for x86 and Raspberry PI (Arm) platforms

I use the popular **Miniconda** system to install Python environments<br>
and other software tools into Linux and Macintosh environments.

- https://docs.conda.io/en/latest/miniconda.html

<br/>


## Mambaforge

I had technical difficulties getting a stable edition of **Miniconda** running<br>
on Raspberry PI / Ubuntu 21.10 (**Impish Indri**).<br>

I found a reliable working alternative with **Mambaforge**:

- https://mamba.readthedocs.io/en/latest/index.html
- https://github.com/conda-forge/miniforge#mambaforge
- https://github.com/conda-forge/miniforge/releases

<br/>


## Ansible playbook

I have an **Ansible playbook** to deploy this script system.

- https://gitlab.com/John_DB/Johns-ansible-public/-/blob/main/playbooks/04_r.install.miniconda.yml

<br/>


## Downloads

**Miniconda** and **Mambaforge** software is **Open Source**, but with redistribution restrictions.<br/>
Therefore I don't provide it here, instead showing you quick commands to download it<br/>
&nbsp; into your own Linux directory: &nbsp; `~/conda.Pkgs/`<br/>

The files in this repo are Linux symlinks into your directory.<br/>
Your files should exist when you run the **Ansible Playbooks**.

**Caution**<br/>
These instructions download the latest software versions as of this writing,<br/>
&nbsp; which are not necessarily the same versions that you download.

**Tip**<br/>
If **Curl** download returns http_code **302** (MAX_CONCURRENT_STREAMS issues),<br/>
&nbsp; try a newer version of **Curl**, or make downloads with a web browser.

**Security**<br/>
Be sure to compare the **checksum hashes** of the downloaded files, with the published editions.<br/>
This is **manual inspection** by yourself.

<br/>

```
mac$ mkdir -p ~/conda.Pkgs/
mac$ cd       ~/conda.Pkgs/

mac$ which curl; curl --version
    /usr/local/opt//curl/bin/curl
    curl 7.85.0 (x86_64-apple-darwin19.6.0)


# - Download Mambaforge

mac$ mambaforge_url='https://github.com/conda-forge/miniforge/releases/latest/download/Mambaforge-Linux-aarch64.sh'
mac$ file='Mambaforge-Linux-aarch64.sh'

mac$ curl -s -L  -w "http_code: %{http_code}\n"  -o ${file}  ${mambaforge_url}
    http_code: 200

mac$ head Mambaforge-Linux-aarch64.sh
    #!/bin/sh
    #
    # NAME:  Mambaforge
    # VER:   22.9.0-2
    # PLAT:  linux-aarch64
    # MD5:   66909f7749af469f9505d6227073719e


# - Download Miniconda

mac$ miniconda_url='https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh'
mac$ file='Miniconda3-Linux-x86_64.sh'

mac$ curl -s -L  -w "http_code: %{http_code}\n"  -o ${file}  ${miniconda_url}
    http_code: 200

mac$ head Miniconda3-Linux-x86_64.sh
    #!/bin/sh
    #
    # NAME:  Miniconda3
    # VER:   py39_4.12.0
    # PLAT:  linux-64
    # LINES: 595
    # MD5:   0c82c9dd531785b1ca2af7f6b1c29523


# - File info, and checksum hashes

mac$ ls -lhF M*
    -rw-r--r--  1 jdb  staff  77M Nov 24 16:52   Mambaforge-Linux-aarch64.sh
    -rw-r--r--  1 jdb  staff  73M Nov 24 17:00   Miniconda3-Linux-x86_64.sh

mac$ shasum -a 256 M*
    26cf4a5cd3a3b9085f75911b459b969d6ff8ab426ef9a8e7ce3b47cc683ead86  Mambaforge-Linux-aarch64.sh
    78f39f9bae971ec1ae7969f0516017f2413f17796670f7040725dd83fcff5689  Miniconda3-Linux-x86_64.sh

mac$ md5 M*
    MD5 (Mambaforge-Linux-aarch64.sh) = e6b25bb0ddd2b5234f653bc531a05e31
    MD5 (Miniconda3-Linux-x86_64.sh) = 7843dd7d0a2c53b0df37ca8189672992
```


<br/>

## License
Copyright &copy; 2019, Mountain Informatik GmbH<br>
All rights reserved.<br>
Permission to share is granted under the terms of the **Server Side Public License**<br>
https://www.mongodb.com/licensing/server-side-public-license
<br>



