# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

# - symver.org version
# version='9.0.0'


# - Install Ansible vault, with working demos.
#
# - FYI:
#   -> Ansible "Vault" is misleading, it barely manages a "password vault" at all!
#      Eg, reading and writing encrypted content (strings, variables) to & from a vault file.
#
#   -> I recommend Hashicorp Vault for more serious use.
#       https://developer.hashicorp.com/vault
#       https://en.wikipedia.org/wiki/HashiCorp#Open-source_tools
#       https://elatov.github.io/2022/01/using-hashicorp-vault-with-ansible/
#
#   -> Nevertheless, Ansible Vault is adequate for secure use in basic use-cases.
#       https://docs.ansible.com/ansible/latest/user_guide/vault.html
#
# -> Red Hat says:
#    https://www.redhat.com/sysadmin/ansible-vault-secure-playbooks



# - Working demo: encrypted Ansible variables
#   -> Create Ansible vault (yml), encrypt/decrypt VARIABLES used in a Playbook.
#   -> FYI: The "vault password file" has only one password!
#
#       location of vaults:     ~/.vaults/
#
#       vault password file:
#           - file:             ~/.vaults/password-1.txt
#             ... ansible.cfg:  DEFAULT_VAULT_PASSWORD_FILE
#           - vault id:         demoVaultId
#           - master password:  demoMasterPassword
#
#       vault:
#           - file:             ~/.vaults/vault-1.yml
#           - key:              demoKey
#           - password:         demoPassword


# - Strategy Q&A:
#   https://docs.ansible.com/ansible/latest/user_guide/vault.html
#
#   Q: Do you want to encrypt all your content with the same password,
#      or use different passwords for different needs?
#
#   A: To get started, with "demo" and "kickoff / low-security" use-cases,
#       I want to encrypt all content with the same password.
#
#   Q: Where do you want to store your password or passwords?
#
#   A: To get started, with "demo" and "kickoff / low-security" use-cases,
#       I store these in a file:  ~/.vaults/vault_1.yml
#       For demo purposes, a copy of this vault is published in GitLab.


# - FYI:
#   https://docs.ansible.com/ansible/latest/user_guide/vault.html#managing-vault-passwords
#   https://docs.ansible.com/ansible/latest/user_guide/vault.html#using-encrypted-variables-and-files
#   https://docs.ansible.com/ansible/latest/reference_appendices/config.html#default-vault-password-file
#
#   https://www.redhat.com/sysadmin/ansible-playbooks-secrets
#   http://www.freekb.net/Article?id=2426
#
#   https://stackoverflow.com/questions/55432965/how-to-encrypt-variables-using-ansible-vault
#   https://stackoverflow.com/questions/49743856/using-ansible-vault-in-interactive-mode-via-bash-script/49744154#49744154
#
#   https://pypi.org/project/keyring/
#   https://github.com/ansible-community/contrib-scripts/tree/main/vault
#   https://github.com/ansible-community/contrib-scripts/blob/main/vault/vault-keyring.py
#   https://github.com/ansible-community/contrib-scripts/blob/main/vault/vault-keyring-client.py



mac$ date; whoami; uname -n
    Thu Nov 17 08:24:51 CET 2022
    jdb
    jdbs-mac-mini-3.home

mac$ sw_vers -productVersion    # - MacOS Catalina
    10.15.7

mac$ pwd
    /Users/jdb/



    #######
    # FYI #
    #######

mac$ echo $ANSIBLE_CONFIG
    /Users/jdb/zGit/ansible/etc/ansible.jdb.cfg


mac$ which yq; yq --version
    /usr/local/bin/yq
    yq (https://github.com/mikefarah/yq/) version 4.27.3


mac$ which python python3
    /Users/jdb/miniconda3/bin/python
    /Users/jdb/miniconda3/bin/python3

mac$ python --version; python3 --version
    Python 3.9.7
    Python 3.9.7


mac$ which ansible ansible-playbook ansible-vault
    /usr/local/bin/ansible
    /usr/local/bin/ansible-playbook
    /usr/local/bin/ansible-vault

mac$ ansible --version
    ansible [core 2.13.3]
      config file = /Users/jdb/zGit/ansible/etc/ansible.jdb.cfg
      configured module search path = ['/Users/jdb/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
      ansible python module location = /usr/local/Cellar/ansible/6.3.0/libexec/lib/python3.10/site-packages/ansible
      ansible collection location = /Users/jdb/.ansible/collections:/usr/share/ansible/collections
      executable location = /usr/local/bin/ansible
      python version = 3.10.6 (main, Aug 30 2022, 05:09:33) [Clang 12.0.0 (clang-1200.0.32.29)]
      jinja version = 3.1.2
      libyaml = True


mac$ man ansible-vault | head
    ANSIBLE-VAULT(1)        System administration commands        ANSIBLE-VAULT(1)
    ...


mac$ ansible-vault --help
    usage: ansible-vault [-h] [--version] [-v] {create,decrypt,edit,view,encrypt,encrypt_string,rekey} ...
    encryption/decryption utility for Ansible data files

    positional arguments:
      {create,decrypt,edit,view,encrypt,encrypt_string,rekey}
        create              Create new vault encrypted file
        decrypt             Decrypt vault encrypted file
        edit                Edit vault encrypted file
        view                View vault encrypted file
        encrypt             Encrypt YAML file
        encrypt_string      Encrypt a string
        rekey               Re-key a vault encrypted file

    options:
      --version             show program's version number, config file location, configured module search path, module location, executable location and exit
      -h, --help            show this help message and exit
      -v, --verbose         Causes Ansible to print more debug messages. Adding multiple -v will increase the verbosity, the builtin plugins currently
                            evaluate up to -vvvvvv. A reasonable level to start is -vvv, connection debugging might require -vvvv.

    See 'ansible-vault <command> --help' for more information on a specific command.



    #######################
    # Set up conveniences #
    #######################

# - Set these environment variables in Bashrc, or ~/.zPass.cr, or elsewhere.

mac$ echo ${ANSIBLE_CONFIG}
    /Users/jdb/zGit/ansible/etc/ansible.jdb.cfg


# - Bashrc "info function" shows real examples:

mac$ i_ansible_vault

    #########################
    # Ansible Vault summary #
    #########################
    - v_bashrc_204: 9.0.0

# - https://gitlab.com/umi-ch/ansible
# - https://gitlab.com/umi-ch/bashrc
#
# - With "John's APE convention" (Ansible Playbook via Environment-variables),
#       only this ANSIBLE_ environment variable is needed:  ANSIBLE_VAULT_IDENTITY_LIST
#   It's set to a list of vaults and password files.

$ ansv_show
    - v_bashrc_204: 9.0.0

    $ env | egrep 'ANSV_|ANSIBLE_VAULT|APE_VAULT' | sort
        ANSIBLE_VAULT_IDENTITY_LIST=
        ..  demoVaultId@~/.vaults/password-1.txt
        APE_VAULT_FILES=~/zGit/ansible/inventories/group_vars/all/
        APE_VAULT_PASSWORDS=~/.vaults/

    $ ( cd /Users/jdb/zGit/ansible/inventories//group_vars/all/ ; ls -lhF v*.yml )
        lrwxr-xr-x@ 1 jdb  staff    30B Nov 17  2022 vault-1.yml@ -> ~/.vaults/vault-1.yml


    # - Vault keys:
    
        ~/zGit/ansible/inventories/group_vars/all/vault-1.yml
            ---
            demoKey: !vault |
            ansv_demoKey_1: !vault |
            ansv_inbound_authorized_keys_default: !vault |
            ansv_ubuntu_password: !vault |


    # - Vault password files:
    
        ~/.vaults/password-1.txt


    ###########
    # ansible #
    ###########

# - 'ansible' command works, implicitly using $ANSIBLE_VAULT_IDENTITY_LIST

$ ansible localhost  -m ansible.builtin.debug  -a var="ansv_demoKey_1"
    localhost | SUCCESS => {
        "ansv_demoKey_1": "demoPassword-1-Default"
    }


    #################
    # ansible-vault #
    #################

# - Ironically, 'ansible-vault' command doesn't support management of "vault files"
#       with multiple secrets.
#   It only works as a "Unix filter", encrypting and decrypting entire strings or files,
#       not "selections" from a vault file.
# - Use 'yq' to extract keys.
#       https://stackoverflow.com/questions/43467180/how-to-decrypt-string-with-ansible-vault-2-3-0

$ yq '.ansv_demoKey_1' "${APE_VAULT_FILES}/vault-1.yml"
    $ANSIBLE_VAULT;1.2;AES256;demoVaultId
    336...37
    653...38
    306...32
    363...64
    356...65

$ yq '.ansv_demoKey_1' "${APE_VAULT_FILES}/vault-1.yml" | ansible-vault decrypt --vault-password-file "${APE_VAULT_PASSWORDS}/password-1.txt"; echo
    Decryption successful
    demoPassword-1-Default


    ################################
    # ansible-playbook             #
    # - using John's APE mechanism #
    ################################

$ alias ape
    alias ape='f_ape'

$ ape 00_e localhost
    -# 00_e.show-vaulted.yml
    ...
    -# vault_id=''
    -# ANSIBLE_VAULT_IDENTITY_LIST='demoVaultId@~/.vaults/password-1.txt'
    -# ANSIBLE_VAULT_ID_MATCH='True'   
    -$ ansible-playbook   '/Users/jdb/zGit/ansible/playbooks/00_e.show-vaulted.yml'
    PLAY [localhost] ***************************************************************
    ...
    TASK [20. Encrypted variables: ansv_demoKey_*] *********************************
    ok: [localhost] => {
        "msg": [
            ansv_demoKey_0    :  undefined
            ansv_demoKey_1    : demoPassword-1-Default
            ...
            ansv_demoKey_bozo :  undefined
    
    PLAY RECAP *********************************************************************
    localhost                  : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0


    ###########################
    # create a vault identity #
    ###########################

$ mkdir -p  ~/.vaults/
$ chmod 700 ~/.vaults/

# - Use a local variable here, instead of exporting ANSIBLE_IDENTITY:
$ my_ANSIBLE_IDENTITY='demoVaultId'

# - As new vaults (and corresponding passwords) are created, update this list in ~/.zrc/bashrc_...
$ export ANSIBLE_VAULT_IDENTITY_LIST="${ANSIBLE_VAULT_IDENTITY_LIST}, ..."


    #####################
    # create a password #
    #####################
    #
    # - FYI: Ansible's scripting mechanism can be used as well, but not shown here.

# - Create a password for this vault:
$ echo 'demoMasterPassword' > "${APE_VAULT_PASSWORDS}/password-1.txt"

# - More secure alternative:
$ openssl rand -hex 20 | tee "${APE_VAULT_PASSWORDS}/password-1.txt"
    f1f...72

$ chmod 600 "${APE_VAULT_PASSWORDS}/password-1.txt"


    ##########################
    # new Ansible Vault file #
    ##########################

# - Create a new Ansible Vault file:
$ touch "${APE_VAULT_PASSWORDS/vault-1.yml}"

# - Sym-linked the new vault into Ansible space:
$ deslashed=$(echo "${APE_VAULT_PASSWORDS}/vault-1.yml" | f_deslash)
$ ( cd "${APE_VAULT_FILES}" ; ln -s "${deslashed}" . )

# - Populate the new vault:
$ printf "# Ansible Vault YAML\n\n---\n\n"  > "${APE_VAULT_FILES}/vault-1.yml"

# - FYI: The  --name  must be unique across all vault files, eg: ansv_demoKey_1
#   Otherwise, explicitly set ANSIBLE_VAULT_ID before running f_ape.

$ (ansible-vault encrypt_string  'demoPassword-1-Default'  --name 'ansv_demoKey_1'  --encrypt-vault-id "${my_ANSIBLE_IDENTITY}"; echo; echo) | tee -a "${APE_VAULT_FILES}/vault-1.yml"
    ansv_demoKey_1: !vault |
          $ANSIBLE_VAULT;1.2;AES256;demoVaultId
          336...37
            ...
          356...65


        #######
        # FYI #
        #######

# - FYI: Glitch:
#   Parameters
#       --vault-id "${ANSV_VAULT_ID}"   --vault-password-file "${ANSV_PASSWORD_FILE}"
#   are not the same as:
#       --vault-id "${ANSV_VAULT_ID}"@"${ANSV_PASSWORD_FILE}"
#
# - in the first format, "demoVaultId" is assumed to be a file!



    ###############################################
    # Playbook : ./playbooks/04_t.deploy-webs.yml #
    ###############################################
    #
    # - This playbook deploys web server config files, including a client-cert CA
    #   which is confidential, and should not be stored (unencrypted) in a Git Repo.
    #
    # - Instead, make vault-protected edition of the file, which can be published
    #   in a repository and decrypted by Ansible during installation
    #   on the target web server.
    #
    # - FYI:
    #   https://docs.ansible.com/ansible/latest/vault_guide/vault_encrypting_content.html#encrypting-files-with-ansible-vault

    # - Raw, unencrypted files to be encrypted:
    #       ~/zGit/ansible/playbooks/files/web/client-certs/demo-ca-cert.pem
    #
    # - Encrypted files in the Git Repo:
    #       ~/zGit/ansible/playbooks/files/web/zTemplates/00/apache2/client_certs/demo-ca-cert.pem.enc
    #       ~/zGit/ansible/playbooks/files/web/zTemplates/00/nginx/client_certs/demo-ca-cert.pem.enc
    #
    # - Files decrypted on the target web server:
    #       /ext/apache2/client_certs/demo-ca-cert.pem
    #       /etc/nginx/client_certs/demo-ca-cert.pem


mac$ cd ~/zGit/ansible/playbooks/files/web/client-certs/
mac$

mac$ ls -lhF
    total 56
    -rw-r--r--  1 jdb  staff   1.5K Nov 25 15:14 demo-ca-cert.pem
    -rw-------  1 jdb  staff   1.7K Nov 25 15:14 demo-ca-key.pem
    -rw-------  1 jdb  staff   2.7K Nov 25 15:25 demo-client-cert.p12
    -rw-r--r--  1 jdb  staff   1.3K Nov 25 15:21 demo-client-cert.pem
    -rw-------  1 jdb  staff   1.7K Nov 25 15:17 demo-client-key.pem
    -rw-r--r--  1 jdb  staff   1.0K Nov 25 15:19 demo-client-req.pem
    -rw-------  1 jdb  staff   1.1K Nov 25 15:40 demo-openssl.cnf

mac$ md5 demo-ca-cert.pem
    MD5 (demo-ca-cert.pem) = 304a755d2ffae781cbf9b2dc6365695b


mac$ ansible-vault encrypt  demo-ca-cert.pem        \
        --vault-id "${ANSIBLE_VAULT_IDENTITY}"      \
        --encrypt-vault-id "${ANSV_VAULT_ID}"       \
        --output  demo-ca-cert.pem.enc

    Encryption successful

mac$ cat demo-ca-cert.pem.enc
    $ANSIBLE_VAULT;1.2;AES256;demoVaultId
    35336532326237313165663832333635333561626637633335333138346635613432653239633761
    3139373964633739326339666335386637633363656539390a626635333335323730336134303830
    32383834396632326533376538376633336566633839316665366466396661393264353532346161
    3962373834323739360a636463373964613339613964643037343530363535653136393336343139
    32663164373231346139353337366664643539616635626264373830666563326263323264656333
    66636238653832393663663538663737623734633665376533656433316136303161376237653432
    66323635356235643836306433333063616166323231626338616538346561616266383930643130
    65623437663763326234613866653362333934633839303330363130396366383031623361356435
    65326635646566343833613462386231643663623465303433363536353532376163633732376162
    66383639616262643962306331613163353337363562323065373836326266313038643034333064
    35333732396365343465386237613936643635643061636337626663646235613564626363613830
    61396239376661366164653937656165626639646632326461386561323137646365396135613765
    31663638333664633337353834383334633838626665356534316163613765623364626334373731
    34623066373934643566666438326134636332333436323637616634616637623738303232356433
    31373063623832653537326338316238393837363330393430353431333564323233663631656334
    64343261333736336330366364373431316636393030653365626462336630623366313939366661
    65323362316236333165313335376637616466643564383163663930623364653264303637663161
    32363466343039393833343766636162303463663861373363336433306265633730346433303764
    64353032613161633333643837363561633333303864616139373762313763356430373237396133
    32623562313531393630363062336334313631346633326137323564613531313930316230316438
    31653466613439366164323963336533633630373737366638613437663332653732343162643830
    34343936653433633933363131383964363435366434343239376232323130353933313664373465
    38656130376665616137316461346635666265616563633362643036353565666636363035396630
    33626465323161353932393039643939363962353733376265666232323538363233336337363365
    31396339323764316430666164326130616535626236343733366339373633353963343966396162
    35663662323138633431383565653932386134633262373030613631393233316635333365306465
    31646161353135323332343635653433333135623832366536633439396434623164326436666266
    32393730306562383065336136613164313437356633376332626430326330623732376639656438
    36393638343133393465333139353464663935353131313566643732353238653761643235646637
    62393934393236323434343462343436636338383464376563333464393966343466636265356337
    33653534623366353136373764353238303133633538333764393462613934643236656332306664
    30646564613965343439653463646437633434353131323838646162356561653330323965616466
    32663561633534343036333966373131343636643935656266323365346531393732306131616466
    31663666626537616639366662393465386662313533383361663835353431353834353835343261
    35353731623136613531333461656230393039323563646236363865623761616439333866633639
    34313064656631623833663436636263633364613432653436363530663438613532366165393432
    39636664323337303264663635386434323163653261323961613430666163356331346237366563
    61656165333735373531363437653963393933316439306365336164623362333763653465313064
    31396438656137343565663435623432316236616334356636356237353832353032356336303037
    37633935643264623131343661323934633535363135663232326164623936343038346132363266
    63646537303164333130306231663333656630643961316661303938646336356531396439313662
    35666430346633343931383734353933616133663536663137613239343562336535386562326566
    63353163396637666133653864306435356164373239363632663637353564633664316164353633
    30633533376630643937366239346336643939366135613363323035356330396234353537333930
    66653334363365663536626261323831633963623331616563636432663635663461396663393732
    36666439633933633339343533653861633166323564303531313665663832616635333362376664
    66353965613562333935623963623832353131356536393432333165356138376631353766343338
    64336138633038656430343130353864623031353631633037643437663336316532623333323637
    31333932323338313965356331633936323365386337623431353735313537343030633837366538
    61306434636138343934633764313032323735326132623433363536623331656263303530653861
    31393232623630336633373738323964373531663932643236313762396166643332663465613935
    32613461613836616365373364393761653062356533326661383334386362306538663133616337
    36663662313763353533343563353263313365323933623964373530353835383235346437643838
    36353762353933636566303061623834356364663336646331326465386165663639393636316532
    30653962643238616633623063663634626636306662323362626464656437356439333037353335
    39626661633965643937643464646238623735333462343562376636666233343739613262636163
    63383561343839373863333064326133663731663264303239626539316533393936396536383664
    35623533313538303137386232313032366235663135336464363965303739336637643330313035
    66633666643739613533303166303462346238343963373566356238656566396666663931313064
    65613033363336656665366633633565626366376264386263616265363165356335646564313935
    65313937633064616261393937383634386537346662623566363262326535393562653439616461
    63336130353533373033376561666336633831323632393635366261303965636666663433303065
    31373132636565643837343266333161386565613937633337633366343739366564316566373838
    66326135363061373132623631383632326336663166643264346134643537313731653838336563
    66316231383162303661643233653036343232356135386331313136626637393263396432633666
    39346635653363313261333034616533393931306435613462386462336433666336353638393138
    33353633646635646366616337376234663831386638646161653739646633616333643466373163
    31646261613266613561373230303639633534363363656466366561643866616363316137383365
    31306236366261643431323661613662313530383566376232333131633663303433613139343334
    36613165306139386238393932373865366239346536366637396163633630303034633534336439
    37393466316133666163363037343263353934663037633531663065363735333235363738306435
    32616461663363656561626339633263613264336637646337336232326566663966653137646663
    32353632303937343230386433646363343933313065613061306339626537373135626136313662
    35653662373835666562623635666464626636316434343930353134623730303062613161373739
    37623037366563396135333062393135336137646562393266393333353363303935623365613333
    33383265613738313937303363396330313564306139396331636639656636636335643931316461
    30353531353535303238356530626239626336393562326337643764366130323061326665386439
    39393230313735346334336266303931323433333134373634663166333734313663656335356532
    34383637366330343237326232613330383062316334633538333434356263393032666162373161
    37623834373361363831


# - Manual check:
#   -> Looks good.

mac$ ansible-vault view  demo-ca-cert.pem.enc  --vault-id "${ANSIBLE_VAULT_IDENTITY}" | head -2
    -----BEGIN CERTIFICATE-----
    MIIELjCCAxagAwIBAgIUSLOBG+9YCyUaV7spyJnmSG6G0+AwDQYJKoZIhvcNAQEL


# - Copy the encrypted files into the "file source" used by Ansible Playbooks.
#   These files will be copied to the target server with  ansible.builtin.copy  which
#   will automatically detect and decrypt Vault data, when the parameters are supplied.
#
# - These Vault parameters are already handled in  Johns Agile Bashrc / f_ape()  wrapper.

mac$ cp -p demo-ca-cert.pem.enc  ~/zGit/ansible/playbooks/files/web/zTemplates/00/apache2/client_certs/demo-ca-cert.pem
mac$

mac$ head -3 ~/zGit/ansible/playbooks/files/web/zTemplates/00/apache2/client_certs/demo-ca-cert.pem
    $ANSIBLE_VAULT;1.2;AES256;demoVaultId
    35336532326237313165663832333635333561626637633335333138346635613432653239633761
    3139373964633739326339666335386637633363656539390a626635333335323730336134303830


mac$ cp -p demo-ca-cert.pem.enc  ~/zGit/ansible/playbooks/files/web/zTemplates/00/nginx/client_certs/demo-ca-cert.pem
mac$

mac$ head -3 ~/zGit/ansible/playbooks/files/web/zTemplates/00/nginx/client_certs/demo-ca-cert.pem
    $ANSIBLE_VAULT;1.2;AES256;demoVaultId
    35336532326237313165663832333635333561626637633335333138346635613432653239633761
    3139373964633739326339666335386637633363656539390a626635333335323730336134303830


# - Set the symlinks
#
# - My Nginx config files use a "substitute variable" for the client cert,
#   which the Ansible Playbook translated into a file reference.
# - For more flexibility when testing, this file reference is a symlink
#   which can be override, per tenant.

mac$ cd ~/zGit/ansible/playbooks/files/web/zTemplates/
mac$

mac$ find .  -name '*ca-cert*'
    ./00/apache2/client_certs/demo-ca-cert.pem
    ./00/nginx/client_certs/demo-ca-cert.pem


mac$ (cd ./00/apache2/client_certs/; rm -f ca-cert; ln -s demo-ca-cert.pem ca-cert)
mac$ (cd ./00/nginx/client_certs/;   rm -f ca-cert; ln -s demo-ca-cert.pem ca-cert)


mac$ find .  -name '*ca-cert*'
    ./00/apache2/client_certs/ca-cert
    ./00/apache2/client_certs/jdcc-ca-cert.pem
    ./00/apache2/client_certs/demo-ca-cert.pem
    ./00/nginx/client_certs/ca-cert
    ./00/nginx/client_certs/jdcc-ca-cert.pem
    ./00/nginx/client_certs/demo-ca-cert.pem

mac$ find .  -name '*ca-cert*' -exec ls -lhF {} \;
    lrwxr-xr-x  1 jdb  staff    16B Nov 25 18:15 ./00/apache2/client_certs/ca-cert@ -> demo-ca-cert.pem
    -rw-------  1 jdb  staff   6.3K Nov 25 16:22 ./00/apache2/client_certs/demo-ca-cert.pem
    lrwxr-xr-x  1 jdb  staff    16B Nov 25 18:15 ./00/nginx/client_certs/ca-cert@ -> demo-ca-cert.pem
    -rw-------  1 jdb  staff   6.3K Nov 25 16:22 ./00/nginx/client_certs/demo-ca-cert.pem


# - Update Ansible Playbook
#   -> ansible.builtin.copy  can automatically identify and decrypt Ansible Vault files.
#
#   https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html#parameter-decrypt
#   https://ahelpme.com/software/ansible/ansible-using-ansible-vault-with-copy-module-to-decrypt-on-the-fly-files/

mac$ grep -A5 ansible.builtin.copy  ~/zGit/ansible/playbooks/04_t.deploy-webs.yml
      ansible.builtin.copy:
        src:            "{{ my_web_repo_dir }}/{{ item[0] }}/{{ item[1] }}"
        dest:           "/etc/{{ item[1] }}"
        decrypt:        True
        follow:         True
        local_follow:   False
    --
      ansible.builtin.copy:
        src:            "{{ my_web_repo_dir }}/{{ item[0] }}/{{ item[1] }}"
        dest:           "/var/{{ item[1] }}"
        decrypt:        True
        follow:         True
        local_follow:   False



    ###########
    # Confirm #
    ###########

# - Deploy these updates to  ocean.umi.ch  and confirm that the results are seen there.

mac$ time ape 04_t ocean
    -# 04_t.deploy-webs.yml
    ...

    TASK [20. Download config files] ***********************************************
    ok: [ocean] => (item=['00', 'apache2/ports.conf'])
    ok: [ocean] => (item=['00', 'apache2/apache2.conf'])
    ok: [ocean] => (item=['00', 'apache2/sites-available/umi-apache2.conf'])
    ok: [ocean] => (item=['00', 'apache2/conf-available/fqdn.conf'])
    ok: [ocean] => (item=['00', 'apache2/client_certs/demo-ca-cert.pem'])
    ok: [ocean] => (item=['00', 'apache2/client_certs/ca-cert'])
    ok: [ocean] => (item=['00', 'nginx/nginx.conf'])
    ok: [ocean] => (item=['00', 'nginx/sites-available/umi-nginx.conf'])
    ok: [ocean] => (item=['00', 'nginx/client_certs/demo-ca-cert.pem'])
    ...

    TASK [27. Make substitutes: Do it.] ********************************************
    ok: [aws] => (item=['/etc/apache2/conf-available/fqdn.conf', ['ZZ-Client-Cert-CA', ' ...
    ok: [aws] => (item=['/etc/apache2/sites-available/umi-apache2.conf', ['ZZ-Client-Cer ...
    ok: [aws] => (item=['/etc/apache2/sites-available/umi-apache2.conf', ['ZZ-NGINX-Cl-C ...
    ok: [aws] => (item=['/etc/nginx/sites-available/umi-nginx.conf', ['ZZ-NGINX-Cl-Cert' ...
    ok: [aws] => (item=['/var/www/s3-aws/private/index.html', ['ZZ-Client-Cert-CA', 'ca- ...
    ok: [aws] => (item=['/var/www/s3-aws/private/index.html', ['ZZ-NGINX-Cl-Cert', '= NO ...
    ok: [aws] => (item=['/var/www/s3-aws/public/index.html', ['ZZ-Client-Cert-CA', 'ca-c ...
    ok: [aws] => (item=['/var/www/s3-aws/public/index.html', ['ZZ-NGINX-Cl-Cert', '= NOO ...
    ok: [aws] => (item=['/var/www/s2-aws/index.html', ['ZZ-Client-Cert-CA', 'ca-cert']])
    ok: [aws] => (item=['/var/www/s2-aws/index.html', ['ZZ-NGINX-Cl-Cert', '= NOOP']])
    ok: [aws] => (item=['/var/www/umi-446/index-cc.html', ['ZZ-Client-Cert-CA', 'ca-cert ...
    ok: [aws] => (item=['/var/www/umi-446/index-cc.html', ['ZZ-NGINX-Cl-Cert', '= NOOP'] ...
    ...

    TASK [35. Apache2 config check] ************************************************
    TASK [debug] *******************************************************************
    ok: [ocean] => {
            "stdout_lines": [
             $ sudo apachectl configtest
                Syntax OK
    ...

    TASK [36. Nginx config check] **************************************************
    TASK [debug] *******************************************************************
    ok: [ocean] => {
            "stdout_lines": [
             $ sudo nginx -t -c /etc/nginx/nginx.conf
                nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
                nginx: configuration file /etc/nginx/nginx.conf test is successful
    ...


# - Restart Nginx on the target web server

mac$ ape 14_j ocean
    -# 14_j.nginx-restart.yml
    -$ export APE_HOSTS='ocean'        # - Playbook host list.
    -$ export APE_STEPS=''
    -# vault_id=''
    -# ANSIBLE_VAULT_IDENTITY_LIST='demoVaultId@~/.vaults/password-1.txt'
    -# ANSIBLE_VAULT_ID_MATCH='True'
    -# ANSIBLE_VAULT_PASSWORD_FILE='/Users/jdb/.vaults/password-1.txt'

    -$ ansible-playbook   '/Users/jdb/zGit/ansible/playbooks/14_j.nginx-restart.yml'
    ...

    TASK [12. Nginx status] ********************************************************
    TASK [debug] *******************************************************************
    ok: [ocean] => {
            "stdout_lines": [
             $ systemctl status nginx
                ● nginx.service - A high performance web server and a reverse proxy server
                     Loaded: loaded (/lib/systemd/system/nginx.service; enabled; vendor preset: enabled)
                     Active: active (running) since Fri 2022-11-25 20:38:49 CET; 1s ago
                       Docs: man:nginx(8)
                    Process: 95764 ExecStartPre=/usr/sbin/nginx -t -q -g daemon on; master_process on; (code=exited, status=0/SUCCESS)
                    Process: 95766 ExecStart=/usr/sbin/nginx -g daemon on; master_process on; (code=exited, status=0/SUCCESS)
                   Main PID: 95767 (nginx)
                      Tasks: 4 (limit: 1030)
                     Memory: 5.1M
                        CPU: 60ms
                     CGroup: /system.slice/nginx.service
                             ├─95767 "nginx: master process /usr/sbin/nginx -g daemon on; master_process on;"
                             ├─95768 "nginx: worker process" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" ""
                             ├─95769 "nginx: cache manager process" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" ""
                             └─95770 "nginx: cache loader process" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" ""

                Nov 25 20:38:49 ocean systemd[1]: nginx.service: Deactivated successfully.
                Nov 25 20:38:49 ocean systemd[1]: Stopped A high performance web server and a reverse proxy server.
                Nov 25 20:38:49 ocean systemd[1]: Starting A high performance web server and a reverse proxy server...
                Nov 25 20:38:49 ocean systemd[1]: Started A high performance web server and a reverse proxy server.


# - HTTPS tests

mac$ cd ~/zGit/ansible/playbooks/files/web/client-certs/


# - Main page, port 443, no client cert needed:
#
mac$ curl -s -w '%{stderr}http_code: %{http_code}\n'   https://ocean.umi.ch  |  grep '<title>'
    <title>ocean.umi.ch</title>
    http_code: 200


# - Special page, port 446, test WITHOUT client cert:
#
mac$ curl -s -w '%{stderr}http_code: %{http_code}\n'   https://ocean.umi.ch:446
    <html>
    <head><title>400 No required SSL certificate was sent</title></head>
    <body>
    <center><h1>400 Bad Request</h1></center>
    <center>No required SSL certificate was sent</center>
    <hr><center>nginx</center>
    </body>
    </html>
    http_code: 400


# - Special page, port 443, test WITH client cert:
#
mac$ curl -s -w '%{stderr}http_code: %{http_code}\n'    \
        --cert demo-client-cert.pem                     \
        --key  demo-client-key.pem                      \
        https://ocean.umi.ch:446  |  grep '<title>'

    <title>ocean.umi.ch with Client Cert</title>
    http_code: 200


